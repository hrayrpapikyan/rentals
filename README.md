# Apartment rental platform

A platform to allow clients and realtors rent and sail apartments on monthly basis.

* User (clients and realtors) may register and log in.
* Clients would be able to browse rentable apartments in a list and on a map.
* Realtors would be able to browse rentable and already rented apartments in a list and on a map.
* Realtors would be able to CRUD apartments and set their state rentable/rented.
* Admins would be able CRUD all apartments, realtors, and clients.
* All users should be able to filter the displayed apartments by size, price, and the number of rooms. When an apartment is added, each entry has a floor area size, price per month, number of rooms, geolocation coordinate, date added and an associated realtor.

### Installation

The project consists of two parts: server and client.
For installation guides and other information like running unit tests about each one please browse to 'server/api.rentals.com' and 'client/rentals.com' directories.

## Built With

* [Apigility](https://apigility.org/) - [Zend Framework 3](https://framework.zend.com/) based RESTful API creating tool
* [Doctrine 2](http://www.doctrine-project.org/) - ORM
* [OAUTH 2](https://oauth.net/2/) - Authorization standard
* [MySql](https://www.mysql.com/) - Database
* [phinx](https://phinx.org/) - Database migration tool
* [PHPUnit](https://phpunit.de/) - PHP Testing framework
* [React](https://reactjs.org/) - JavaScript library
* [Redux](https://redux.js.org/) - State management framework
* [ApiDoc](http://apidocjs.com/) - Documentation creating tool for RESTful APIs
* [Monolog](https://www.loggly.com/docs/php-monolog/) - Monolog library