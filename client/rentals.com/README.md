# RESTful API for Apartment rentals application

### Prerequisites

You need to install on your machine
* [npm](https://www.npmjs.com/)

### Installing

* Rename src/config.json.dist to config.json
* Replace __BASE64(API_CLIENT:API_SECRET)__ with base64 hash of client_id and client_secret combination, e.g.: d3d3LnJlbnRhbHMuY29tOnJlbnRhbHMud2Vic2l0ZQ
* Replace __API_URL__ with api url e.g.: http://api.rentals.com/

```
cd /path/to/rentals.com
npm install
```

Development mode will start the application on localhost:3000

```
npm start
```
Build production application
```
 npm run-script build
 ```
 
After creating a production build, please don't forget to 
add it's url into backEnd's config/autoload/zfr_cors.global.php, 
so that the API will not reject HTTP requests