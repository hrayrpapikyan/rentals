import {APARTMENT_ACTION_TYPES} from "../Constants/ActionTypes";
import apiWrapper from '../ApiWrapper';
import * as AppActions from './AppActions';
import {push} from "react-router-redux";
import createUUID from "uuid";

export const API_SET_REALTORS = realtors => ({
    type: APARTMENT_ACTION_TYPES.API_SET_REALTORS,
    payload: realtors
});

export const TOGGLE_APARTMENT_LOADING = isLoading => ({
    type: APARTMENT_ACTION_TYPES.TOGGLE_APARTMENT_LOADING,
    payload: isLoading
});

export const API_GET_REALTORS = () => async dispatch => {
    try {
        const realtors = await apiWrapper.get('users', { only_realtors: 1 });
        dispatch(API_SET_REALTORS(realtors));
    } catch (err) {
        dispatch(AppActions.SET_API_ERROR(err.response.data.detail));
    }

};

export const SET_APARTMENT_VALIDATION_ERRORS = errors => ({
    type: APARTMENT_ACTION_TYPES.SET_APARTMENT_VALIDATION_ERRORS,
    payload: errors
});

export const API_SET_APARTMENT = apartment => ({
    type: APARTMENT_ACTION_TYPES.API_SET_APARTMENT,
    payload: apartment
});

export const API_GET_APARTMENT = (id) => async dispatch => {
    try {
        const apartment = await apiWrapper.get(`apartments/${id}`);
        dispatch(API_SET_APARTMENT(apartment));
    } catch (err) {
        if (err.response && err.response.data) {
            switch (err.response.data.status) {
                case 404:
                    dispatch(push('/not-found'));
                    break;
                case 403:
                    dispatch(push('/forbidden'));
                    break;
                default:
                    dispatch(AppActions.SET_API_ERROR(err.response.data.detail));
            }
        } else {
            dispatch(AppActions.SET_API_ERROR('Unknown error'));

        }

    }

};


export const SAVE_APARTMENT = (data, id = null) => dispatch => {
    dispatch(TOGGLE_APARTMENT_LOADING(true));

    let res = id
        ? apiWrapper.patch(`apartments/${id}`, data)
        : apiWrapper.post('apartments', { ...data, uuid: createUUID()});
    res
        .then(() => {
            dispatch(push('/apartment-search'));
            dispatch(SET_APARTMENT_VALIDATION_ERRORS({}));

            dispatch(TOGGLE_APARTMENT_LOADING(false));
            if (id) {
                dispatch(AppActions.SET_API_SUCCESS('Successfully Updated'));

            } else {
                dispatch(AppActions.SET_API_SUCCESS('Successfully Created'));

            }

        }).catch(err => {
        dispatch(TOGGLE_APARTMENT_LOADING(false));

        if (err.response && err.response.data.status === 422) {
            let errors = {};
            for (let key in err.response.data.validation_messages) {
                let firstKey = Object.keys(err.response.data.validation_messages[key])[0];
                errors[key] = err.response.data.validation_messages[key][firstKey];
            }
            dispatch(SET_APARTMENT_VALIDATION_ERRORS(errors));

        } else {
            dispatch(AppActions.SET_API_ERROR(err.response.data.detail));
        }
    })

};

export const DELETE_APARTMENT = (id) => async dispatch => {
    try {
        await apiWrapper.delete(`apartments/${id}`);
        dispatch(AppActions.SET_API_SUCCESS('Successfully deleted'));
        dispatch(push('/apartment-search'));
    } catch (err) {
        dispatch(AppActions.SET_API_ERROR(err.response.data.detail));
    }

};
