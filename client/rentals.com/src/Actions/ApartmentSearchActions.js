import { APARTMENT_SEARCH_ACTION_TYPES } from "../Constants/ActionTypes";
import apiWrapper from '../ApiWrapper';
import * as AppActions from './AppActions';

export const API_SET_APARTMENTS = apartments => ({
    type: APARTMENT_SEARCH_ACTION_TYPES.API_SET_APARTMENTS,
    payload: apartments
});

export const API_GET_APARTMENTS = (data) => async dispatch => {
    try {
        const apartments = await apiWrapper.get('apartments', data);
        dispatch(API_SET_APARTMENTS(apartments));
    } catch (err) {
        if (err.response.data && err.response.data.detail) {
            dispatch(AppActions.SET_API_ERROR(err.response.data.detail));
        } else {
            dispatch(AppActions.SET_API_ERROR('Unknown error'));
        }

    }

};
