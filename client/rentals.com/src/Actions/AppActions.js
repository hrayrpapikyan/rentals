import { APP_ACTION_TYPES } from "../Constants/ActionTypes";

export const SET_API_ERROR = errText => dispatch => {
    dispatch({
        type: APP_ACTION_TYPES.SET_API_ERROR,
        payload: errText
    });

    setTimeout(() => {
        dispatch({
            type: APP_ACTION_TYPES.SET_API_ERROR,
            payload: null
        })
    }, 2000)
};

export const SET_API_SUCCESS = successText => dispatch => {
    dispatch({
        type: APP_ACTION_TYPES.SET_API_SUCCESS,
        payload: successText
    });

    setTimeout(() => {
        dispatch({
            type: APP_ACTION_TYPES.SET_API_SUCCESS,
            payload: null
        })
    }, 2000)
};
