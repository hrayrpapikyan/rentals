import createUUID from 'uuid';
import { push } from 'react-router-redux';
import { AUTH_ACTION_TYPES } from "../Constants/ActionTypes";
import apiWrapper from '../ApiWrapper';
import * as AppActions from './AppActions';

export const TOGGLE_REGISTER_LOADING = isLoading => ({
    type: AUTH_ACTION_TYPES.TOGGLE_REGISTER_LOADING,
    payload: isLoading
});

export const TOGGLE_LOGIN_LOADING = isLoading => ({
    type: AUTH_ACTION_TYPES.TOGGLE_LOGIN_LOADING,
    payload: isLoading
});

export const SET_REGISTER_VALIDATION_ERRORS = errors => ({
    type: AUTH_ACTION_TYPES.SET_REGISTER_VALIDATION_ERRORS,
    payload: errors
});


export const SET_USER = user => ({
    type: AUTH_ACTION_TYPES.SET_USER,
    payload: user
});

export const LOGOUT_USER = () => dispatch => {
    apiWrapper.logout();
    dispatch(SET_USER(null));
    dispatch(push('/login'));
};

export const REGISTER = data => dispatch => {
    dispatch(TOGGLE_REGISTER_LOADING(true));
    apiWrapper
        .register({ ...data, uuid: createUUID()})
        .then(() => {
            dispatch(push('/login'));
            dispatch(SET_REGISTER_VALIDATION_ERRORS({}));
            dispatch(TOGGLE_REGISTER_LOADING(false));
            dispatch(AppActions.SET_API_SUCCESS('Successfully registered'));
        })
        .catch(err => {
            if (err.response && err.response.data.status === 422) {
                let errors = {};
                for (let key in err.response.data.validation_messages) {
                    let firstKey = Object.keys(err.response.data.validation_messages[key])[0];
                    errors[key] = err.response.data.validation_messages[key][firstKey];
                }
                dispatch(SET_REGISTER_VALIDATION_ERRORS(errors));
            } else {
                dispatch(AppActions.SET_API_ERROR(err.response.data.detail));
            }
            dispatch(TOGGLE_REGISTER_LOADING(false));
        })
}

export const LOGIN = data => dispatch => {
    dispatch(TOGGLE_LOGIN_LOADING(true));
    apiWrapper
        .authorizeWithCredentials(data)
        .then(() => apiWrapper.get('users/me'))
        .then(user => {
            dispatch(SET_USER(user));
            dispatch(TOGGLE_LOGIN_LOADING(false));
            dispatch(push('/apartment-search'));
        })
        .catch(err => {
            dispatch(AppActions.SET_API_ERROR(err.response.data.detail));
            dispatch(TOGGLE_LOGIN_LOADING(false));
        })
}
