import { EDIT_PROFILE_ACTION_TYPES } from "../Constants/ActionTypes";
import apiWrapper from '../ApiWrapper';
import * as AppActions from './AppActions';
import * as AuthActions from './AuthActions';
import { push } from 'react-router-redux';

export const API_SET_PROFILE = profile => ({
    type: EDIT_PROFILE_ACTION_TYPES.API_SET_PROFILE,
    payload: profile
});

export const API_GET_PROFILE = (id = 'me') => async dispatch => {
    try {
        const profile = await apiWrapper.get(`users/${id}`);
        dispatch(API_SET_PROFILE(profile));
    } catch (err) {

        if (err.response) {
            if (err.response.data.status === 404) {
                dispatch(push('/not-found'));
            } else {
                dispatch(AppActions.SET_API_ERROR(err.response.data.detail));
            }

        } else {
            dispatch(AppActions.SET_API_ERROR('Unknown error'));

        }

    }

};

export const SET_EDIT_PROFILE_VALIDATION_ERRORS = errors => ({
    type: EDIT_PROFILE_ACTION_TYPES.SET_EDIT_PROFILE_VALIDATION_ERRORS,
    payload: errors
});

export const API_UPDATE_PROFILE = (data, id = 'me') => async dispatch => {
    try {
        const newProfile = await apiWrapper.patch(`users/${id}`, data);
        if(id === 'me') {
            dispatch(AuthActions.SET_USER(newProfile));
            dispatch(SET_EDIT_PROFILE_VALIDATION_ERRORS({}));

            dispatch(push('/'));
        } else {
            dispatch(push('/users'));
        }
        dispatch(AppActions.SET_API_SUCCESS('Successfully Updated'));


    } catch (err) {

        if (err.response && err.response.data) {
            if (err.response.data.status === 422) {
                let errors = {};
                for (let key in err.response.data.validation_messages) {
                    let firstKey = Object.keys(err.response.data.validation_messages[key])[0];
                    errors[key] = err.response.data.validation_messages[key][firstKey];
                }
                dispatch(SET_EDIT_PROFILE_VALIDATION_ERRORS(errors));

            } else {
                dispatch(AppActions.SET_API_ERROR(err.response.data.detail));
            }
        } else {
            dispatch(AppActions.SET_API_ERROR('Unknown Error'));

        }


    }

};

export const API_DELETE_PROFILE = (id) => async dispatch => {
    try {
        await apiWrapper.delete(`users/${id}`);
        dispatch(push('/users'));
    } catch (err) {
        dispatch(AppActions.SET_API_ERROR(err.response.data.detail));
    }
    dispatch(AppActions.SET_API_SUCCESS('Successfully deleted'));

};
