import { USERS_ACTION_TYPES } from "../Constants/ActionTypes";
import apiWrapper from '../ApiWrapper';
import * as AppActions from './AppActions';

export const API_SET_USERS = users => ({
    type: USERS_ACTION_TYPES.API_SET_USERS,
    payload: users
});

export const API_GET_USERS = (page = 1) => async dispatch => {
    try {
        const users = await apiWrapper.get('users', { page });
        dispatch(API_SET_USERS(users));
    } catch (err) {
        dispatch(AppActions.SET_API_ERROR(err.response.data.detail));
    }

};
