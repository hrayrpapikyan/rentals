
import Api from './HttpClient';
import Config from './config';

const apiWrapper = Api({
    api_client_token: Config.api_client_token,
    base_url: Config.base_url
});

export default apiWrapper;