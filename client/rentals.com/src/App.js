import React, {Component} from 'react';
import {HashRouter, Route, Redirect, Switch} from 'react-router-dom';
import {ConnectedRouter} from 'react-router-redux';
import {Provider} from 'react-redux';
import history from './history';
import store from './store';
import './styles/bootstrap.css';
import './styles/hover.css';
import './styles/style.css';
import './styles/media.css';
import Register from "./Screens/Register";
import Login from "./Screens/Login";
import Users from "./Screens/Users";
import EditProfile from "./Screens/EditProfile";
import ApartmentEdit from "./Screens/ApartmentEdit";
import ApartmentSearch from "./Screens/ApartmentSearch";
import NotFound from "./Screens/Error/NotFound";
import Forbidden from "./Screens/Error/Forbidden";
import checkInitialAuth from './checkInitialAuth';
import * as AuthActions from './Actions/AuthActions';


class App extends Component {

    constructor(props) {
        super(props);
        this.state = { authChecked: false };
    }

    async componentWillMount() {
        const user = await checkInitialAuth();
        store.dispatch(AuthActions.SET_USER(user));
        this.setState({ authChecked: true });
    }

    render() {
        if (!this.state.authChecked) return null;
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <HashRouter>
                        <Switch>
                            <Route exact path={'/'} component={() => <Redirect from="/" to="apartment-search" />} />
                            <Route exact path={"/register"} component={Register}/>
                            <Route exact path={"/login"} component={Login}/>
                            <Route exact path={"/users"} component={Users}/>
                            <Route exact path={"/edit-profile"} component={EditProfile}/>
                            <Route exact path={"/edit-profile/:id"} component={EditProfile}/>
                            <Route exact path={"/apartment-edit"} component={ApartmentEdit}/>
                            <Route exact path={"/apartment-edit/:id"} component={ApartmentEdit}/>
                            <Route exact path={"/apartment-search"} component={ApartmentSearch}/>
                            <Route exact path={"/not-found"} component={NotFound}/>
                            <Route exact path={"/forbidden"} component={Forbidden}/>
                            <Route  component={NotFound}/>
                        </Switch>
                    </HashRouter>
                </ConnectedRouter>
            </Provider>
        );
    }
}

export default App;
