import React from 'react';

class AppError extends React.Component {

    render() {
        if(!this.props.error) return null;
        return (
            <div className="alert-custom alert alert-danger" role="alert">
                {this.props.error}
            </div>
        )
    }
}

export default AppError;