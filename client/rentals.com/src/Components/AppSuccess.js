import React from 'react';

class AppSuccess extends React.Component {

    render() {
        if(!this.props.success) return null;
        return (
            <div className="alert-custom alert alert-success" role="alert">
                {this.props.success}
            </div>
        )
    }
}

export default AppSuccess;