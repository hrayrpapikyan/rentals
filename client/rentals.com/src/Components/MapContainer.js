import React from 'react';
import {Map, Marker, GoogleApiWrapper} from 'google-maps-react';
import _ from 'lodash';

export class MapContainer extends React.Component {
    countMapInitialCentre = () => {
        const {apartments} = this.props;
        if (!apartments || !apartments.length) {
            return {
                lat: 0,
                lng: 0
            }
        }

        let latSum = 0, longSum = 0;
        _.map(apartments, apartment => {
            latSum += parseFloat(apartment.latitude);
            longSum += parseFloat(apartment.longitude);
        });

        return {
            lat: latSum / apartments.length,
            lng: longSum / apartments.length,
        }
    };

    getMarkersForApartments = () => {
        const {apartments} = this.props;
        if (!apartments || !apartments.length) {
            return [];
        }
        let markers = [];
        _.map(apartments, (apartment, index) => {
            markers.push(<Marker
                key={index}
                title={`${apartment.size} SQm, ${apartment.price} USD, ${apartment.room_count} rooms`}
                position={{lat: apartment.latitude, lng: apartment.longitude }} />);

        });
        return markers;
    };

    render() {
        const style = {
            width: '100%',
            height: '100%'
        };

        return (

            <Map
                google={this.props.google}
                zoom={14}
                style={style}
                initialCenter={this.countMapInitialCentre()}
            >
                {this.getMarkersForApartments()}

            </Map>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: ('AIzaSyAB_6lYOGnpT0F82gyqWhXlk5R0um7sRkI')
})(MapContainer)