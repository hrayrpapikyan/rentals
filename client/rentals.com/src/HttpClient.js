import axios from 'axios';


let saveAccess;
let getAccess;

export default initConfig => {
    if (!initConfig.api_client_token) {
        throw new Error('client_id must be specified');
    }
    if (!initConfig.base_url) {
        throw new Error('base_url must be specified');
    }

    saveAccess = createAccessSetter(initConfig.accessKeyName);
    getAccess = createAccessGetter(initConfig.accessKeyName);

    const conf = { ...initConfig, base_url: initConfig.base_url.replace(/\/$/g, '')};
    return {
        get: (url, params) => _get(conf, url, params),
        post: (url, data) => _post(conf, url, data),
        patch: (url, data) => _patch(conf, url, data),
        delete: (url) => _delete(conf, url),
        authorizeWithCredentials: credentials => _authorizeWithCredentials(conf, credentials),
        register: (data) => _register(conf, data),
        authCheck: () => _authCheck(),
        logout: () => _logout(),
    }
};

const _get = (initConfig, url, params) => abstractRequest(initConfig, {
    method: 'get',
    url,
    params,
    headers: {},
});

const _post = (initConfig, url, data) => abstractRequest(initConfig, {
    method: 'post',
    url,
    data,
    headers: {},
    params: {},
});

const _patch = (initConfig, url, data) => abstractRequest(initConfig, {
    method: 'patch',
    url,
    data,
    headers: {},
    params: {},
});

const _delete = (initConfig, url) => abstractRequest(initConfig, {
    method: 'delete',
    url,
    data: {},
    headers: {},
    params: {},
});

const _authorizeWithCredentials = (initConfig, credentials) => {
    const authPayload ={ ...credentials, grant_type: 'password'};
    return authorize(initConfig, authPayload);
}

const _authCheck = () => {
    const access = getAccess();
    return (!!access && typeof access === 'object');
}

const _logout = async () => {
    await saveAccess(null);
}

const _register = (initConfig, payload) => new Promise((resolve, reject) => {
    axios.post(`${initConfig.base_url}/users`, payload, { headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        }})
        .then( response => resolve(response.data))
        .catch(e => reject(e));
})

const authorize = (initConfig, payload) => new Promise((resolve, reject) => {
    axios.post(`${initConfig.base_url}/oauth`, payload, { headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": `Basic ${initConfig.api_client_token}`
        }})
        .then( response => {
             saveAccess(response.data);
            return resolve();
        })
        .catch(e => reject(e));
});



const abstractRequest = async (initConfig, requestConfig, res, rej) => {
    return new Promise(async (resolve, reject) => {
        if (res) resolve = res;
        if (rej) reject = rej;
        const access = getAccess();
        if(!!access && typeof access === 'object') {
            const { access_token } = access;
            const payload = {
                ...requestConfig,
                url: `${initConfig.base_url}/${requestConfig.url}`,
                headers: {
                    ...requestConfig.headers,
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${access_token}`
                },
            };
            axios.request(payload)
                .then(response => resolve(response.data))
                .catch(err => {
                    if (err.response.status === 401) {
                        return refreshToken(initConfig)
                            .then(() => abstractRequest(initConfig, requestConfig, resolve, reject))
                            .catch(err => reject(err));
                    } else {
                       return reject(err);
                    }


                });
        }


    })
}

const refreshToken = (initConfig) => new Promise(async (resolve, reject) => {
    const access = getAccess();
    axios.post(`${initConfig.base_url}/oauth`, {
        grant_type: 'refresh_token',
        refresh_token: access.refresh_token,
    }, { headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": `Basic ${initConfig.api_client_token}`
        }}).then(response => {
         saveAccess(response.data);
        return resolve(response.data);
    }).catch(err => {
        return reject(err);
    });

});

const createAccessSetter = (accessKeyName='API_ACCESS') => access => localStorage.setItem(accessKeyName, JSON.stringify(access));
const createAccessGetter = (accessKeyName='API_ACCESS') => () => JSON.parse(localStorage.getItem(accessKeyName));