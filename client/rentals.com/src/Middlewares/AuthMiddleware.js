import { push } from 'react-router-redux';
import _ from 'lodash';

const guestRoutes = [
    '/login',
    '/register',
];

const middleWare = store => next => action => {
    next(action);
    if (action.type === '@@router/LOCATION_CHANGE') {
        if (!!store.getState().auth.user && _.includes(guestRoutes, action.payload.pathname)) {
            store.dispatch(push('/apartment-search'));
        } else if ((!store.getState().auth.user && !_.includes(guestRoutes, action.payload.pathname))) {
            store.dispatch(push('/login'));
        }
    }
};

export default middleWare;