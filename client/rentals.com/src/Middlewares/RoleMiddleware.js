import { push } from 'react-router-redux';



const middleWare = store => next => action => {
    next(action);
    if (action.type === '@@router/LOCATION_CHANGE') {
            if (!!store.getState().auth.user) {
                if (store.getState().auth.user.type === 'client' && isForbiddenForClient(action.payload.pathname)) {
                    store.dispatch(push('/forbidden'));
                } else if (store.getState().auth.user.type === 'realtor' && isForbiddenForRealtor(action.payload.pathname)) {
                    store.dispatch(push('/forbidden'));
                }
            }
        }
};

const isForbiddenForClient = route => {
    if (route === '/users') {
        return true;
    }

    if (route.includes('/apartment-edit')) {
        return true;
    }

    if (route.includes('/edit-profile/')) {
        return true;
    }
};

const isForbiddenForRealtor = route => {
    if (route === '/users') {
        return true;
    }

    if (route.includes('/edit-profile/')) {
        return true;
    }
};


export default middleWare;