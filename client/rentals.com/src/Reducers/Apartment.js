
import {APARTMENT_ACTION_TYPES} from '../Constants/ActionTypes';

const initialState = () => ({
    realtors: null,
    apartmentIsLoading: false,
    validationErrors: {},
    apartment: null
});

const API_SET_REALTORS = (state, action) => ({ ...state, realtors: action.payload });
const TOGGLE_APARTMENT_LOADING = (state, action) => ({ ...state, apartmentIsLoading: action.payload });
const SET_APARTMENT_VALIDATION_ERRORS = (state, action) => ({ ...state, validationErrors: action.payload });
const API_SET_APARTMENT = (state, action) => ({ ...state, apartment: action.payload });


export default function reducer(state = initialState(), action) {
    switch (action.type) {
        case APARTMENT_ACTION_TYPES.API_SET_REALTORS: return API_SET_REALTORS(state, action);
        case APARTMENT_ACTION_TYPES.TOGGLE_APARTMENT_LOADING: return TOGGLE_APARTMENT_LOADING(state, action);
        case APARTMENT_ACTION_TYPES.SET_APARTMENT_VALIDATION_ERRORS: return SET_APARTMENT_VALIDATION_ERRORS(state, action);
        case APARTMENT_ACTION_TYPES.API_SET_APARTMENT: return API_SET_APARTMENT(state, action);
        default: return state;
    }
}