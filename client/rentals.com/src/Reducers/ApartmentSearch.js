
import { APARTMENT_SEARCH_ACTION_TYPES } from '../Constants/ActionTypes';

const initialState = () => ({
    apartments: null,
});

const API_SET_APARTMENTS = (state, action) => ({ ...state, apartments: action.payload });

export default function reducer(state = initialState(), action) {
    switch (action.type) {
        case APARTMENT_SEARCH_ACTION_TYPES.API_SET_APARTMENTS: return API_SET_APARTMENTS(state, action);
        default: return state;
    }
}