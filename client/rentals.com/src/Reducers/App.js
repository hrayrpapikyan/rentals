import { APP_ACTION_TYPES } from '../Constants/ActionTypes';

const initialState = () => ({
    apiError: null,
});

const SET_API_ERROR = (state, action) => ({ ...state, apiError: action.payload });
const SET_API_SUCCESS = (state, action) => ({ ...state, apiSuccess: action.payload });

export default function reducer(state = initialState(), action) {
    switch (action.type) {
        case APP_ACTION_TYPES.SET_API_ERROR: return SET_API_ERROR(state, action);
        case APP_ACTION_TYPES.SET_API_SUCCESS: return SET_API_SUCCESS(state, action);
        default: return state;
    }
}