
import { AUTH_ACTION_TYPES } from '../Constants/ActionTypes';

const initialState = () => ({
    registerIsLoading: false,
    loginIsLoading: false,
    validationErrors: {},
    error: null,
    user: null,
});

const TOGGLE_REGISTER_LOADING = (state, action) => ({ ...state, registerIsLoading: action.payload });

const TOGGLE_LOGIN_LOADING = (state, action) => ({ ...state, loginIsLoading: action.payload });

const SET_REGISTER_VALIDATION_ERRORS = (state, action) => ({ ...state, validationErrors: action.payload });

const SET_USER = (state, action) => ({ ...state, user: action.payload });

export default function reducer(state = initialState(), action) {
    switch (action.type) {
        case AUTH_ACTION_TYPES.TOGGLE_REGISTER_LOADING: return TOGGLE_REGISTER_LOADING(state, action);
        case AUTH_ACTION_TYPES.TOGGLE_LOGIN_LOADING: return TOGGLE_LOGIN_LOADING(state, action);
        case AUTH_ACTION_TYPES.SET_REGISTER_VALIDATION_ERRORS: return SET_REGISTER_VALIDATION_ERRORS(state, action);
        case AUTH_ACTION_TYPES.SET_USER: return SET_USER(state, action);
        default: return state;
    }
}