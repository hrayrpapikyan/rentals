
import {EDIT_PROFILE_ACTION_TYPES} from '../Constants/ActionTypes';

const initialState = () => ({
    data: null,
    validationErrors: {},

});

const API_SET_PROFILE = (state, action) => ({ ...state, data: action.payload });
const SET_EDIT_PROFILE_VALIDATION_ERRORS = (state, action) => ({ ...state, validationErrors: action.payload });

export default function reducer(state = initialState(), action) {
    switch (action.type) {
        case EDIT_PROFILE_ACTION_TYPES.API_SET_PROFILE: return API_SET_PROFILE(state, action);
        case EDIT_PROFILE_ACTION_TYPES.SET_EDIT_PROFILE_VALIDATION_ERRORS: return SET_EDIT_PROFILE_VALIDATION_ERRORS(state, action);

        default: return state;
    }
}