
import { USERS_ACTION_TYPES } from '../Constants/ActionTypes';

const initialState = () => ({
    data: null,
});

const API_SET_USERS = (state, action) => ({ ...state, data: action.payload });

export default function reducer(state = initialState(), action) {
    switch (action.type) {
        case USERS_ACTION_TYPES.API_SET_USERS: return API_SET_USERS(state, action);
        default: return state;
    }
}