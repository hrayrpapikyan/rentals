
import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';


import app from './App';
import auth from './Auth';
import profile from './Profile';
import users from './Users';
import apartment from './Apartment';
import apartments from './ApartmentSearch';


export default combineReducers({
    app,
    auth,
    profile,
    users,
    apartment,
    apartments,
    router: routerReducer,
});