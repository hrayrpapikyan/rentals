import React from 'react';
import Layout from "../Layout";
import {bindActionCreators} from "redux";
import * as ApartmentActions from "../../Actions/ApartmentActions";
import {connect} from "react-redux";
import _ from 'lodash';
import classNames from "classnames";

class ApartmentEdit extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            price: '',
            room_count: '',
            longitude: '',
            latitude: '',
            size: '',
            realtor_id: '',
            status: ''
        }
    }

    setChangedVal = (key, event) => {
        this.setState({
            [key]: event.target.value
        })
    };

    componentWillMount() {
        if (!this.props.auth.user) return;

        if (this.props.match.params.id) {
            this.props.actions.API_GET_APARTMENT(this.props.match.params.id);
        }

        if (this.props.auth.user.type === 'admin') {
            this.props.actions.API_GET_REALTORS();
        }
    }

    componentWillReceiveProps(props) {
        if (!this.props.auth.user) return;

        if (!_.isEqual(this.props.apartment.apartment, props.apartment.apartment) && !!props.apartment.apartment) {
            let data = {
                price: props.apartment.apartment.price,
                room_count: props.apartment.apartment.room_count,
                longitude: props.apartment.apartment.longitude,
                latitude: props.apartment.apartment.latitude,
                size: props.apartment.apartment.size,
            };
            if (this.props.auth.user.type === 'admin') {
                data.realtor_id = props.apartment.apartment.realtor_id;
            }

            if (this.props.match.params.id) {
                data.status = props.apartment.apartment.status;
            }

            this.setState(data);
        }
    }

    componentWillUnmount() {
        if (!this.props.auth.user) return;

        if (this.props.auth.user.type === 'admin') {
            this.props.actions.API_SET_REALTORS(null);
        }
        this.props.actions.API_SET_APARTMENT(null);
        this.props.actions.SET_APARTMENT_VALIDATION_ERRORS({});
    }

    getRealtorFullName = realtor => {
        return `${realtor.first_name} ${realtor.last_name}`
    };

    onSave = () => {
        if (!this.props.auth.user) return;

        let data = {
            price: this.state.price,
            size: this.state.size,
            room_count: this.state.room_count,
            longitude: this.state.longitude,
            latitude: this.state.latitude,
        };
        if (this.props.auth.user.type === 'admin') {
            data.realtor_id = this.state.realtor_id;
        }

        if (this.props.match.params.id) {
            data.status = this.state.status;
        }

        let id = this.props.match.params.id || null;
        this.props.actions.SAVE_APARTMENT(data, id);
    };

    onDelete = () => {
        if (!this.props.match.params.id) {
            return;
        }

        this.props.actions.DELETE_APARTMENT(this.props.match.params.id);

    }

    render() {
        const { realtors, apartmentIsLoading, validationErrors } = this.props.apartment;
        return (
            <Layout>
                <form className="form-block">
                    <h2 className="text-uppercase text-center title">Apartment Details</h2>

                    <div className={classNames('form-group', { 'form-error': validationErrors.price})}>
                        <input
                            onChange={this.setChangedVal.bind(this, 'price')}
                            value={this.state.price}
                            type="text"
                            className="form-control"
                            placeholder="price"
                        />
                        <p className="form-info">USD</p>

                        {!!validationErrors.price && (
                            <p className="error-txt ">{validationErrors.price}</p>
                        )}
                    </div>

                    <div className={classNames('form-group', { 'form-error': validationErrors.room_count})}>
                        <select
                            className="custom-select"
                            onChange={this.setChangedVal.bind(this, 'room_count')}
                            value={this.state.room_count}
                        >
                            <option>Room Count</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>

                        {!!validationErrors.room_count && (
                            <p className="error-txt ">{validationErrors.room_count}</p>
                        )}
                    </div>

                    <div className={classNames('form-group', { 'form-error': validationErrors.longitude})}>
                        <input
                            onChange={this.setChangedVal.bind(this, 'longitude')}
                            className="form-control"
                            placeholder="Longitude"
                            value={this.state.longitude}
                        />

                        {!!validationErrors.longitude && (
                            <p className="error-txt ">{validationErrors.longitude}</p>
                        )}
                    </div>

                    <div className={classNames('form-group', { 'form-error': validationErrors.latitude})}>
                        <input
                            onChange={this.setChangedVal.bind(this, 'latitude')}
                            className="form-control"
                            placeholder="Latitude"
                            value={this.state.latitude}

                        />

                        {!!validationErrors.latitude && (
                            <p className="error-txt ">{validationErrors.latitude}</p>
                        )}
                    </div>

                    <div className={classNames('form-group', { 'form-error': validationErrors.size})}>
                        <input
                            onChange={this.setChangedVal.bind(this, 'size')}
                            className="form-control"
                            placeholder="Size"
                            value={this.state.size}

                        />
                        <p className="form-info">SqM</p>

                        {!!validationErrors.size && (
                            <p className="error-txt ">{validationErrors.size}</p>
                        )}
                    </div>

                    {this.props.auth.user && this.props.auth.user.type === 'admin' && (
                        <div className={classNames('form-group', { 'form-error': validationErrors.realtor_id})}>
                            <select
                                onChange={this.setChangedVal.bind(this, 'realtor_id')}
                                className="custom-select"
                                title=""
                                value={this.state.realtor_id}

                            >
                                <option>Realtor</option>
                                {
                                    _.map(realtors, (realtor) => <option key={realtor.id} value={realtor.id}>{this.getRealtorFullName(realtor)}</option>)
                                }

                            </select>

                            {!!validationErrors.realtor_id && (
                                <p className="error-txt ">{validationErrors.realtor_id}</p>
                            )}
                        </div>
                    )}

                    {this.props.match.params.id && (
                        <div className={classNames('form-group', { 'form-error': validationErrors.status})}>
                            <select
                                onChange={this.setChangedVal.bind(this, 'status')}
                                className="custom-select"
                                title=""
                                value={this.state.status}

                            >
                                <option value="rented">Rented</option>
                                <option value="rentable">Rentable</option>

                            </select>

                            {!!validationErrors.status && (
                                <p className="error-txt ">{validationErrors.status}</p>
                            )}
                        </div>
                    )}


                    <div className="form-group">
                        <button
                            disabled={apartmentIsLoading}
                            type="button"
                            className="btn btn-default"
                            onClick={this.onSave}
                        >
                            Save
                        </button>
                    </div>

                    {this.props.match.params.id && (
                        <div className="form-group">
                            <button
                                type="button"
                                className="btn btn-default"
                                onClick={this.onDelete}
                            >
                                Delete
                            </button>
                        </div>
                    )}

                </form>
            </Layout>
            
        );
    }
}

const mapDispatchToProps = dispatch => ({ actions: bindActionCreators(ApartmentActions, dispatch) });
const mapStateToProps = state => ({ apartment: state.apartment , auth: state.auth});

export default connect(mapStateToProps, mapDispatchToProps)(ApartmentEdit);