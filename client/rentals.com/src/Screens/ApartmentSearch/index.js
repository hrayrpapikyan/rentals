import React from 'react';
import Layout from "../Layout";
import MapContainer from '../../Components/MapContainer';
import {bindActionCreators} from "redux";
import * as ApartmentSearchActions from "../../Actions/ApartmentSearchActions";
import {connect} from "react-redux";
import { Link } from 'react-router-dom';
import _ from 'lodash';
import classNames from "classnames";


class ApartmentSearch extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            size_from: '',
            size_to: '',
            price_from: '',
            price_to: '',
            room_count_from: '',
            room_count_to: '',
            page: 1,
        }
    }

    setChangedVal = (key, event) => {
        this.setState({
            [key]: event.target.value
        })
    };

    onSearch = (page) => {

        this.setState({
            page
        })

        let searchParams = {
            page
        };

        if (this.state.size_from) {
            searchParams.size_from = this.state.size_from;
        }

        if (this.state.size_to) {
            searchParams.size_to = this.state.size_to;
        }

        if (this.state.price_from) {
            searchParams.price_from = this.state.price_from;
        }

        if (this.state.price_to) {
            searchParams.price_to = this.state.price_to;
        }

        if (this.state.room_count_from) {
            searchParams.room_count_from = this.state.room_count_from;
        }

        if (this.state.room_count_to) {
            searchParams.room_count_to = this.state.room_count_to;
        }

        this.props.actions.API_GET_APARTMENTS(searchParams)
    };

    componentWillUnmount() {
        this.props.actions.API_SET_APARTMENTS(null);
    }


    getActionButton = apartment => {
        if (!this.props.auth.user) return;
        if (this.props.auth.user.type === 'admin' || parseInt(apartment.realtor_id, 10) === parseInt(this.props.auth.user.id, 10)) {
            return (
                    <Link to={`/apartment-edit/${apartment.id}`}>
                        <button type="button" className="btn btn-default">update/delete</button>
                    </Link>
                )
        }
        return '';
    };


    render() {
        const apartmentData = this.props.apartments.apartments;
        return (
            <Layout>
                <div className="container">

                    <div className="row">

                        <div className="col-lg-7 col-12">

                            <form action="#" method="post" className="form-block full-width">
                                <h2 className="text-uppercase text-center title">Search Apartment</h2>

                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="form-group form-info-out">
                                            <input
                                                onChange={this.setChangedVal.bind(this, 'size_from')}
                                                className="form-control"
                                                placeholder="Size From"
                                                value={this.state.size_from}
                                            />
                                            <p className="form-info">SqM</p>
                                        </div>
                                    </div>

                                    <div className="col-md-6">
                                        <div className="form-group form-info-out">
                                            <input
                                                onChange={this.setChangedVal.bind(this, 'size_to')}
                                                className="form-control"
                                                placeholder="Size To"
                                                value={this.state.size_to}

                                            />
                                            <p className="form-info">SqM</p>
                                        </div>
                                    </div>

                                    <div className="col-md-6">
                                        <div className="form-group form-info-out">
                                            <input
                                                onChange={this.setChangedVal.bind(this, 'price_from')}
                                                className="form-control"
                                                placeholder="Price From"
                                                value={this.state.price_from}

                                            />
                                            <p className="form-info">USD</p>
                                        </div>
                                    </div>

                                    <div className="col-md-6">
                                        <div className="form-group form-info-out">
                                            <input
                                                onChange={this.setChangedVal.bind(this, 'price_to')}
                                                className="form-control"
                                                placeholder="Price To"
                                                value={this.state.price_to}

                                            />
                                            <p className="form-info">USD</p>
                                        </div>
                                    </div>

                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <input
                                                onChange={this.setChangedVal.bind(this, 'room_count_from')}
                                                className="form-control"
                                                placeholder="Number of rooms from"
                                                value={this.state.room_count_from}

                                            />
                                        </div>
                                    </div>

                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <input
                                                onChange={this.setChangedVal.bind(this, 'room_count_to')}
                                                className="form-control"
                                                placeholder="Number of rooms to"
                                                value={this.state.room_count_to}

                                            />
                                        </div>
                                    </div>

                                    <div className="col-md-12">
                                        <div className="form-group">
                                            <button
                                                type="button"
                                                className="btn btn-default"
                                                onClick={this.onSearch.bind(this, 1)}
                                            >
                                                Search
                                            </button>

                                            {this.props.auth.user && this.props.auth.user.type !== 'client' && (
                                                <Link to={'/apartment-edit'} className="btn btn-default">Add Apartment</Link>

                                            )}
                                        </div>
                                    </div>



                                </div>

                            </form>
                        </div>
                        <div className="col-lg-5 col-12 map-cont">
                            {apartmentData && apartmentData.apartments && apartmentData.apartments.length && (
                                <MapContainer apartments={apartmentData.apartments}/>
                            )}
                        </div>
                    </div>


                    <div className="table-out full-width search-table">
                        <div className="table-responsive">
                            <table className="table default-table">
                                <thead>
                                <tr>
                                    <th>Status</th>
                                    <th>Size</th>
                                    <th>Price per month</th>
                                    <th>Number of rooms</th>
                                    <th>Realtor</th>
                                    {this.props.auth.user && this.props.auth.user.type !== 'client' && (
                                        <th>actions</th>
                                    )}

                                </tr>
                                </thead>
                                <tbody>
                                    {apartmentData && apartmentData.apartments && _.map(apartmentData.apartments, apartment => (
                                        <tr key={apartment.id}>
                                            <td>{apartment.status}</td>
                                            <td>{apartment.size} SQm</td>
                                            <td>{apartment.price} USD</td>
                                            <td>{apartment.room_count}</td>
                                            <td>{apartment.realtor_name}</td>
                                            <td>
                                                {this.props.auth.user && this.props.auth.user.type !== 'client' && this.getActionButton(apartment)}

                                            </td>
                                        </tr>
                                    ))}



                                </tbody>
                            </table>
                        </div>


                    </div>
                    {apartmentData && (
                        <nav aria-label="Page navigation" className="default-pagination">
                            <ul className="pagination justify-content-end">
                                <li className="page-item" onClick={this.onSearch.bind(this, Math.max(1, this.state.page - 1))}>
                                        <span className="page-link"  aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                            <span className="sr-only">Previous</span>
                                        </span>
                                </li>

                                {_.range(1, apartmentData.page_count + 1).map((page, index) => (
                                    <li key={index} onClick={this.onSearch.bind(this, page)} className={classNames('page-item', { 'active': this.state.page === page})}><span className="page-link">{page}</span></li>

                                ))}

                                <li className="page-item" onClick={this.onSearch.bind(this, Math.min(apartmentData.page_count, parseInt(this.state.page, 10) + 1))}>
                                        <span className="page-link" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                            <span className="sr-only">Next</span>
                                        </span>
                                </li>

                            </ul>
                        </nav>
                    )}


                </div>


            </Layout>

        );
    }
}

const mapDispatchToProps = dispatch => ({ actions: bindActionCreators(ApartmentSearchActions, dispatch) });
const mapStateToProps = state => ({ apartments: state.apartments, auth: state.auth});

export default connect(mapStateToProps, mapDispatchToProps)(ApartmentSearch);