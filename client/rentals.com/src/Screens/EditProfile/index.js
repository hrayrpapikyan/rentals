import React from 'react';
import Layout from "../Layout";
import * as EditProfileActions from "../../Actions/EditProfileActions";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import _ from 'lodash';
import classNames from "classnames";

class EditProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            first_name: '',
            last_name: '',
            change_password: false,
            current_password: '',
            password: '',
            password_confirm: ''
        }
    }

    componentWillMount() {
        const id = this.props.match.params.id || 'me';
        this.props.actions.API_GET_PROFILE(id);
    }

    componentWillUnmount() {
        this.props.actions.API_SET_PROFILE(null);
        this.props.actions.SET_EDIT_PROFILE_VALIDATION_ERRORS({});
    }

    componentWillReceiveProps(props) {
        if (!_.isEqual(this.props.profile, props.profile) && !!props.profile.data) {
            this.setState({
                first_name: props.profile.data.first_name,
                last_name: props.profile.data.last_name,
            })
        }
    }

    setChangedVal = (key, event) => {
        this.setState({
            [key]: event.target.value
        })
    };

    setChangePassword = (event) => {
        this.setState({
            change_password: event.target.checked
        })
    };

    updateProfile = () => {

        const id = this.props.match.params.id || 'me';

        let data = {
            first_name: this.state.first_name,
            last_name: this.state.last_name,
        };
        if (this.state.change_password) {
            data.current_password = this.state.current_password;
            data.password = this.state.password;
            data.password_confirm = this.state.password_confirm;
        }
        this.props.actions.API_UPDATE_PROFILE(data, id)
    };

    deleteProfile = () => {
        const id = this.props.match.params.id;
        this.props.actions.API_DELETE_PROFILE(id)
    };


    render() {
        const { profile } = this.props;
        const { validationErrors } = profile;
        const { first_name, last_name, current_password, password, password_confirm} = this.state;
        const id = this.props.match.params.id || 'me';
        return (
            <Layout>
                {!!profile.data && (
                    <form action="#" method="post" className="form-block">
                        <h2 className="text-uppercase text-center title">Update Profile</h2>

                        <div className={classNames('form-group', { 'form-error': validationErrors.first_name})}>
                            <input
                                onChange={this.setChangedVal.bind(this, 'first_name')}
                                value={first_name}
                                type="text"
                                className="form-control"
                                placeholder="First Name"
                            />
                            {!!validationErrors.first_name && (
                                <p className="error-txt ">{validationErrors.first_name}</p>
                            )}
                        </div>

                        <div className={classNames('form-group', { 'form-error': validationErrors.last_name})}>
                            <input
                                onChange={this.setChangedVal.bind(this, 'last_name')}
                                value={last_name}
                                type="text"
                                className="form-control"
                                placeholder="Last Name"
                            />

                            {!!validationErrors.last_name && (
                                <p className="error-txt ">{validationErrors.last_name}</p>
                            )}

                        </div>

                        {id === 'me' && (
                            <div className='form-group'>

                                <label>
                                    <input
                                        onChange={this.setChangePassword.bind(this)}
                                        type="checkbox"
                                    />
                                    &nbsp;<span className="change-password-span">Change password</span>
                                </label>


                            </div>
                        )}



                        {this.state.change_password && (
                            <div>
                                <div className={classNames('form-group', { 'form-error': validationErrors.current_password})}>
                                    <input
                                        onChange={this.setChangedVal.bind(this, 'current_password')}
                                        value={current_password}
                                        type="password"
                                        className="form-control"
                                        placeholder="Current Password"
                                    />
                                    {!!validationErrors.current_password && (
                                        <p className="error-txt ">{validationErrors.current_password}</p>
                                    )}
                                </div>

                                <div className={classNames('form-group', { 'form-error': validationErrors.password})}>
                                    <input
                                        onChange={this.setChangedVal.bind(this, 'password')}
                                        value={password}
                                        type="password"
                                        className="form-control"
                                        placeholder="New Password"
                                    />
                                    {!!validationErrors.password && (
                                        <p className="error-txt ">{validationErrors.password}</p>
                                    )}
                                </div>

                                <div className={classNames('form-group', { 'form-error': validationErrors.password_confirm})}>
                                    <input
                                        onChange={this.setChangedVal.bind(this, 'password_confirm')}
                                        value={password_confirm}
                                        type="password"
                                        className="form-control"
                                        placeholder="New Password Confirmation"
                                    />
                                    {!!validationErrors.password_confirm && (
                                        <p className="error-txt ">{validationErrors.password_confirm}</p>
                                    )}
                                </div>
                            </div>

                        )}


                        <div className="form-group">
                            <button
                                onClick={this.updateProfile}
                                type="button"
                                className="btn btn-default"
                            >
                                Update
                            </button>
                        </div>

                        {profile.data.type !== 'admin' && this.props.match.params.id && (
                        <div className="form-group">
                            <button
                                onClick={this.deleteProfile}
                                type="button"
                                className="btn btn-default"
                            >
                                Delete
                            </button>
                        </div>
                        )}
                    </form>
                )}

            </Layout>
            
        );
    }
}

const mapDispatchToProps = dispatch => ({ actions: bindActionCreators(EditProfileActions, dispatch) });
const mapStateToProps = state => ({ profile: state.profile });

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);