import React from 'react';
import Layout from "../Layout";

class NotFound extends React.Component {



    render() {

        return (
            <Layout>
                <div className="row">
                    <div className="col-sm-12">
                        <h1 className="text-danger text-center">Page Not Found</h1>
                    </div>
                </div>

            </Layout>
            
        );
    }
}

export default NotFound;