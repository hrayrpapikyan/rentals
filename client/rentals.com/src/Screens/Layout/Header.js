import React from 'react';
import { Link } from 'react-router-dom';

class Header extends React.Component {

    getFullname = user => `${user.first_name} ${user.last_name}`;

    render() {
        const { user } = this.props;
        if(!user) return null;
        return (
            <header>
                <nav className="navbar navbar-expand-lg navbar-light bg-light">

                    <div className="container">
                        <div className="navbar-collapse" id="navbarText">
                            <ul className="navbar-nav mr-auto">
                                {user.type === 'admin' && (
                                    <li className="nav-item">
                                        <Link to={'/users'}>
                                            <span className="nav-link">Users</span>
                                        </Link>

                                    </li>
                                )}
                                <li className="nav-item">
                                    <Link to={'/apartment-search'}>
                                        <span className="nav-link">Apartments</span>
                                    </Link>
                                </li>
                            </ul>
                            <Link to={'/edit-profile'}>
                                <span className="navbar-text">hi, <span className="link-txt">{this.getFullname(user)}</span></span>
                            </Link>
                            &nbsp;
                            <span className="navbar-text"><a className="link-txt" onClick={this.props.onLogout}>logout</a></span>
                        </div>
                    </div>
                </nav>
            </header>
        );
    }
}

export default Header;