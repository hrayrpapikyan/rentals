import React from 'react';
import Wrapper from './Wrapper';
import Header from './Header';
import * as AuthActions from "../../Actions/AuthActions";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import AppError from "../../Components/AppError";
import AppSuccess from "../../Components/AppSuccess";

class Layout extends React.Component {

    onLogout = () => {
        this.props.actions.LOGOUT_USER();
    }

    render() {
        return (
            <div>
                <Header
                    user={this.props.auth.user}
                    onLogout={this.onLogout}
                />
                <AppError error={this.props.app.apiError} />
                <AppSuccess success={this.props.app.apiSuccess} />
                <Wrapper>
                    {this.props.children}
                </Wrapper>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({ actions: bindActionCreators(AuthActions, dispatch) });
const mapStateToProps = state => ({ auth: state.auth, app: state.app });

export default connect(mapStateToProps, mapDispatchToProps)(Layout);