import React from 'react';
import { Link } from 'react-router-dom';
import Wrapper from "../Layout/Wrapper";
import AppError from "../../Components/AppError";
import classNames from "classnames";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as AuthActions from "../../Actions/AuthActions";
import AppSuccess from "../../Components/AppSuccess";

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            validationErrors: {}
        }
    }

    setChangedVal = (key, event) => {
        this.setState({
            [key]: event.target.value
        })
    };

    onLogin = () => {
        const validationErrors = {}
        this.setState({
            validationErrors: {}
        });
        if(!this.state.email) {
            validationErrors.email = 'Value is required and can\'t be empty';
        }
        if(!this.state.password) {
            validationErrors.password = 'Value is required and can\'t be empty';
        }
        this.setState({ validationErrors});
        if(!this.state.email || !this.state.password) return;
        this.props.actions.LOGIN({
            username: this.state.email,
            password: this.state.password,
        })
    }
    render() {
        const { validationErrors } = this.state;
        const { loginIsLoading } = this.props.auth;
        return (
            <Wrapper>
                <AppError error={this.props.app.apiError} />
                <AppSuccess success={this.props.app.apiSuccess} />

                <form action="#" method="post" className="form-block">
                    <h2 className="text-uppercase text-center title">Login</h2>

                    <div className={classNames('form-group', { 'form-error': validationErrors.email})}>
                        <input
                            onChange={this.setChangedVal.bind(this, 'email')}
                            type="text"
                            className="form-control"
                            placeholder="Email"
                            value={this.state.email}
                        />

                        {!!validationErrors.email && (
                            <p className="error-txt ">{validationErrors.email}</p>
                        )}
                    </div>

                    <div className={classNames('form-group', { 'form-error': validationErrors.password})}>
                        <input
                            onChange={this.setChangedVal.bind(this, 'password')}
                            type="password"
                            className="form-control"
                            placeholder="Password"
                            value={this.state.password}
                        />

                        {!!validationErrors.password && (
                            <p className="error-txt ">{validationErrors.password}</p>
                        )}
                    </div>

                    <button
                        disabled={loginIsLoading}
                        onClick={this.onLogin}
                        type="button"
                        className="btn btn-default"
                    >
                        Login
                    </button>

                    <p className="text-center register-txt">
                        Not registered? <Link to={'/register'} className="text-center">Create an account</Link>
                    </p>


                </form>
            </Wrapper>
        );
    }
}

const mapDispatchToProps = dispatch => ({ actions: bindActionCreators(AuthActions, dispatch) });
const mapStateToProps = state => ({ auth: state.auth, app: state.app });

export default connect(mapStateToProps, mapDispatchToProps)(Login);