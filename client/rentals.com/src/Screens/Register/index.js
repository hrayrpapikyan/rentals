import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import classNames from "classnames";
import Wrapper from "../Layout/Wrapper";
import AppError from "../../Components/AppError";
import AppSuccess from "../../Components/AppSuccess";

import * as AuthActions from '../../Actions/AuthActions';

class Register extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            first_name: '',
            last_name: '',
            password: '',
            password_confirm: '',
            type: '',
        }
    }

    setChangedVal = (key, event) => {
        this.setState({
            [key]: event.target.value
        })
    }

    onRegister = () => {
        this.props.actions.REGISTER({
            email: this.state.email,
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            password: this.state.password,
            password_confirm: this.state.password_confirm,
            type: this.state.type,
        })
    }

    componentWillUnmount() {
        this.props.actions.SET_REGISTER_VALIDATION_ERRORS({});
    }

    render() {
        const { registerIsLoading, validationErrors } = this.props.auth;
        return (
            <Wrapper>
                <AppError error={this.props.app.apiError} />
                <AppSuccess success={this.props.app.apiSuccess} />
                <form className="form-block">
                    <h2 className="text-uppercase text-center title">Register New Account</h2>

                    <div className={classNames('form-group', { 'form-error': validationErrors.first_name})}>
                        <input
                            onChange={this.setChangedVal.bind(this, 'first_name')}
                            type="text"
                            className="form-control"
                            placeholder="First Name"
                            value={this.state.first_name}
                        />
                        {!!validationErrors.first_name && (
                            <p className="error-txt ">{validationErrors.first_name}</p>
                        )}

                    </div>

                    <div className={classNames('form-group', { 'form-error': validationErrors.last_name})}>
                        <input
                            onChange={this.setChangedVal.bind(this, 'last_name')}
                            type="text"
                            className="form-control"
                            placeholder="Last Name"
                            value={this.state.last_name}
                        />

                        {!!validationErrors.last_name && (
                            <p className="error-txt ">{validationErrors.last_name}</p>
                        )}
                    </div>

                    <div className={classNames('form-group', { 'form-error': validationErrors.email})}>
                        <input
                            onChange={this.setChangedVal.bind(this, 'email')}
                            type="text"
                            className="form-control"
                            placeholder="Email"
                            value={this.state.email}
                        />

                        {!!validationErrors.email && (
                            <p className="error-txt ">{validationErrors.email}</p>
                        )}
                    </div>

                    <div className={classNames('form-group', { 'form-error': validationErrors.password})}>
                        <input
                            onChange={this.setChangedVal.bind(this, 'password')}
                            type="password"
                            className="form-control"
                            placeholder="Password"
                            value={this.state.password}
                        />

                        {!!validationErrors.password && (
                            <p className="error-txt ">{validationErrors.password}</p>
                        )}
                    </div>

                    <div className={classNames('form-group', { 'form-error': validationErrors.password_confirm})}>
                        <input
                            onChange={this.setChangedVal.bind(this, 'password_confirm')}
                            type="password"
                            className="form-control"
                            placeholder="Confirm Password"
                            value={this.state.password_confirm}
                        />

                        {!!validationErrors.password_confirm && (
                            <p className="error-txt ">{validationErrors.password_confirm}</p>
                        )}
                    </div>

                    <div className={classNames('form-group', { 'form-error': validationErrors.type})}>
                        <select
                            onChange={this.setChangedVal.bind(this, 'type')}
                            className="custom-select"
                            value={this.state.type}
                        >
                            <option>Type</option>
                            <option value="client">Client</option>
                            <option value="realtor">Realtor</option>
                        </select>

                    {!!validationErrors.type && (
                        <p className="error-txt ">{validationErrors.type}</p>
                    )}
                    </div>

                    <div className="form-group">
                        <button
                            disabled={registerIsLoading}
                            onClick={this.onRegister}
                            type="button"
                            className="btn btn-default"
                        >
                            Register
                        </button>
                    </div>

                </form>
            </Wrapper>
        );
    }
}

const mapDispatchToProps = dispatch => ({ actions: bindActionCreators(AuthActions, dispatch) });
const mapStateToProps = state => ({ auth: state.auth, app: state.app });

export default connect(mapStateToProps, mapDispatchToProps)(Register);