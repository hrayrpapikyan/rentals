import React from 'react';
import Layout from "../Layout";
import {bindActionCreators} from "redux";
import * as UsersAction from "../../Actions/UsersActions";
import {connect} from "react-redux";
import { Link } from 'react-router-dom';
import _ from 'lodash';
import queryString from 'query-string';
import classNames from 'classnames';

class Users extends React.Component {

    componentWillMount() {
        this.getUsers(this.props.location.search);
    }

    componentWillReceiveProps(props) {
        if(this.props.location.search !== props.location.search) {
            this.getUsers(props.location.search);
        }
    }

    componentWillUnmount() {
        this.props.actions.API_SET_USERS(null);
    }

    getUserId = user => {
        if (!this.props.auth.user) return;
        if(user.id === this.props.auth.user.id) return 'me';
        return user.id;
    }

    getUsers = search => {
        const { page=1 } = queryString.parse(search);
        this.props.actions.API_GET_USERS(page);
    }

    isActive = currPage => {
        const { page=1 } = queryString.parse(this.props.location.search);
        return currPage === parseInt(page, 10);
    }

    getPreviousPageNumber = () => {
        const { page=1 } = queryString.parse(this.props.location.search);
        return (page > 1) ? page - 1 : 1;
    }

    getNextPageNumber = totalPages => {
        const { page=1 } = queryString.parse(this.props.location.search);
        return Math.min(totalPages, parseInt(page, 10) + 1);
    }

    getActionName = user => {
        return user.type === 'admin' ? 'update' : 'update/delete';
    }

    render() {
        const usersData = this.props.users.data;
        return (
            <Layout>
                {!!usersData && (
                    <div className="container">
                        <div className="table-responsive">
                            <table className="table default-table">
                                <thead>
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Type</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                {_.map(usersData.users, user => (
                                    <tr key={user.id}>
                                        <td>{user.first_name}</td>
                                        <td>{user.last_name}</td>
                                        <td>{user.email}</td>
                                        <td>{user.type}</td>
                                        <td>
                                            <Link to={`/edit-profile/${this.getUserId(user)}`}>
                                                <button type="button" className="btn btn-default">{this.getActionName(user)}</button>
                                            </Link>

                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                            </table>
                        </div>

                        <nav aria-label="Page navigation" className="default-pagination">
                            <ul className="pagination justify-content-end">
                                <Link to={`/users/?page=${this.getPreviousPageNumber()}`}>
                                    <li className="page-item">

                                        <span className="page-link"  aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                            <span className="sr-only">Previous</span>
                                        </span>
                                    </li>
                                </Link>

                                {_.range(1, usersData.page_count + 1).map((page, index) => (
                                    <Link key={index} to={`/users/?page=${page}`}>
                                        <li className={classNames('page-item', { 'active': this.isActive(page)})}><span className="page-link">{page}</span></li>
                                    </Link>

                                ))}

                                <Link to={`/users/?page=${this.getNextPageNumber(usersData.page_count)}`}>
                                    <li className="page-item">
                                        <span className="page-link" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                            <span className="sr-only">Next</span>
                                        </span>
                                    </li>
                                </Link>

                            </ul>
                        </nav>
                    </div>
                )}

            </Layout>

        );
    }
}

const mapDispatchToProps = dispatch => ({ actions: bindActionCreators(UsersAction, dispatch) });
const mapStateToProps = state => ({ users: state.users, auth: state.auth });

export default connect(mapStateToProps, mapDispatchToProps)(Users);