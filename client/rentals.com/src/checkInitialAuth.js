import apiWrapper from './ApiWrapper';


export default () => new Promise((resolve, reject) => {
    const isLoggedIn = apiWrapper.authCheck();
    if (isLoggedIn) {
        apiWrapper
            .get('users/me')
            .then(user => resolve(user));
    } else {
        apiWrapper.logout();
        resolve(null);

    }
})