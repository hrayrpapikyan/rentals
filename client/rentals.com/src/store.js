
import { applyMiddleware, createStore } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import reducer from './Reducers/index';
import history from './history';
import authMiddleware from './Middlewares/AuthMiddleware';
import roleMiddleware from './Middlewares/RoleMiddleware';

let middleware = [
    thunk,
    routerMiddleware(history),
    authMiddleware,
    roleMiddleware,
];

const store = createStore(reducer, applyMiddleware(...middleware));

export default store;