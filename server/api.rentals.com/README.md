# RESTful API for Apartment rentals application

### Prerequisites

You need to install on your machine
* PHP >= 7.1
* [composer](https://getcomposer.org/)
* [mySql](https://www.mysql.com/)
* [APACHE](https://www.apache.org/) (recommended) or [nginx](https://nginx.org/en/) or other web server

### Installing

[Create APACHE virtual](https://www.digitalocean.com/community/tutorials/how-to-set-up-apache-virtual-hosts-on-ubuntu-14-04-lts) host for http://api.rentals.com (or other domain, we only take this as an example)
with configurations like in the example bellow:
```
<VirtualHost *:80>
    <Directory /path/to/api.rentals.com/public>
        Options FollowSymLinks Indexes
        AllowOverride All
        Order deny,allow
        allow from All
    </Directory>
    ServerAdmin admin@rentals.com
    ServerName api.rentals.com
    ServerAlias www.api.rentals.com
    DocumentRoot /path/to/api.rentals.com/public
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

Install dependencies and create autoload files

```
cd /path/to/api.rentals.com
composer install
composer update
composer dumpautoload -o
```

Database
* Create a mysql database 
* Edit phinx.yml , replace __PRODUCTION_DB_NAME__, __PRODUCTION_DB_USERNAME__, __PRODUCTION_DB_PASSWORD__ for production
and __LOCAL_DB_NAME__, __LOCAL_DB_USERNAME__, __LOCAL_DB_PASSWORD__ for development environments
* Edit config/doctrine.global.php file and replace placeholders with your live db credentials
* Rename config/doctrine.global.php.dist to doctrine.global.php and edit with your development db credentials

Run [phinx](https://phinx.org/) migrations to create DB tables and the admin user:
```
cd /path/to/api.rentals.com
./phinx migrate
```

Admin user credentials:
* username: admin@rentals.com
* password: john.doe

Application Oauth credentials
* client_id: www.rentals.com
* client_secret: rentals.website


CORS
* Edit config/zfr_cord.global.php and replace __CLIENT_BASE_URL__ with your production client application url
* Rename config/zfr_cord.local.php.dist to config/zfr_cord.local.php and add other trusted local hosts if needed (localhost:3000 is there by default)

Enable development mode for using Apigility user interface:
```
cd /path/to/api.rentals.com
composer development-enable
```
then just open your browser on {api_url}/apigility/ui#/

## Run unit tests

```
cd /path/to/api.rentals.com
./vendor/bin/phpunit
```
## API documentation

Documentation is already created and located in server/api.rentals.com/public/doc directory.
To view documentation please open your browser on {api_url}/doc/

To regenerate documentation please [install](https://www.npmjs.com/package/apidoc) [ApiDoc](https://www.npmjs.com/package/apidoc)
and

```
cd /path/to/api.rentals.com
apidoc -i ./module -o ./public/doc
```

## Error and exception logging

All unknown errors/exceptions are logged in /path/to/api.rentals.com/app.log file
Which will automatically be created on first error

