<?php

use Doctrine\DBAL\Driver\PDOMySql\Driver as PDOMySqlDriver;

return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driverClass' => PDOMySqlDriver::class,
                'params' => [
                    'host'     => 'localhost',
                    'user'     => '__PRODUCTION_DB_USERNAME__',
                    'password' => '__PRODUCTION_DB_PASSWORD__',
                    'dbname'   => '__PRODUCTION_DB_NAME_',
                ]
            ],
        ],
    ],
];