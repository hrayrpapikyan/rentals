<?php
return [
    'router' => [
        'routes' => [
            'oauth' => [
                'options' => [
                    'spec' => '%oauth%',
                    'regex' => '(?P<oauth>(/oauth))',
                ],
                'type' => 'regex',
            ],
        ],
    ],
    'zf-mvc-auth' => [
        'authentication' => [
            'map' => [
                'Rentals\\V1' => 'oauth2',
            ],
            'adapters' => [
                'oauth2' => [
                    'adapter' => \ZF\MvcAuth\Authentication\OAuth2Adapter::class,
                    'storage' => [
                        'storage' => \Application\OAuth2Adapter::class,
                        'route' => '/oauth',
                    ],
                ],
            ],
        ],
    ],
];
