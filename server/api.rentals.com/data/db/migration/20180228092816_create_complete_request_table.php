<?php


use Phinx\Migration\AbstractMigration;

class CreateCompleteRequestTable extends AbstractMigration
{

    public function up()
    {
        $this->execute("create table complete_request
        (
            uuid varchar(36) not null
                primary key,
            created_date datetime not null
        )
        ;");
    }

    public function down()
    {
        $this->execute("drop table complete_request");
    }
}
