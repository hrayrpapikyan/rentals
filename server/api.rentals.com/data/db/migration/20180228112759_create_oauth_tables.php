<?php


use Phinx\Migration\AbstractMigration;

class CreateOauthTables extends AbstractMigration
{
    public function up()
    {
        $this->execute(
    "CREATE TABLE oauth_clients (
        client_id VARCHAR(80) NOT NULL,
        client_secret VARCHAR(80) NOT NULL,
        redirect_uri VARCHAR(2000) NOT NULL,
        grant_types VARCHAR(80),
        scope VARCHAR(2000),
        user_id VARCHAR(255),
        CONSTRAINT clients_client_id_pk PRIMARY KEY (client_id)
        );");

        $this->execute(
    "CREATE TABLE oauth_access_tokens (
        access_token VARCHAR(40) NOT NULL,
        client_id VARCHAR(80) NOT NULL,
        user_id VARCHAR(255),
        expires TIMESTAMP NOT NULL,
        scope VARCHAR(2000),
        CONSTRAINT access_token_pk PRIMARY KEY (access_token)
        );");

        $this->execute(
    "CREATE TABLE oauth_authorization_codes (
        authorization_code VARCHAR(40) NOT NULL,
        client_id VARCHAR(80) NOT NULL,
        user_id VARCHAR(255),
        redirect_uri VARCHAR(2000),
        expires TIMESTAMP NOT NULL,
        scope VARCHAR(2000),
        id_token VARCHAR(2000),
        CONSTRAINT auth_code_pk PRIMARY KEY (authorization_code)
        );");

        $this->execute("CREATE TABLE oauth_refresh_tokens (
        refresh_token VARCHAR(40) NOT NULL,
        client_id VARCHAR(80) NOT NULL,
        user_id VARCHAR(255),
        expires TIMESTAMP NOT NULL,
        scope VARCHAR(2000),
        CONSTRAINT refresh_token_pk PRIMARY KEY (refresh_token)
        );");

        $this->execute("CREATE TABLE oauth_scopes (
        type VARCHAR(255) NOT NULL DEFAULT \"supported\",
        scope VARCHAR(2000),
        client_id VARCHAR (80),
        is_default SMALLINT DEFAULT NULL
        );");

        $this->execute("CREATE TABLE oauth_jwt (
        client_id VARCHAR(80) NOT NULL,
        subject VARCHAR(80),
        public_key VARCHAR(2000),
        CONSTRAINT jwt_client_id_pk PRIMARY KEY (client_id)
        );");

        $this->execute("INSERT INTO rentals.oauth_clients (client_id, client_secret, redirect_uri, grant_types, scope, user_id) VALUES ('www.rentals.com', '$2y$10\$X9fr8a9IwJaqeHCJnzwKB.Tm0jt9CD1rqkWBIzlSlho3tlpmKxo7e', '''''', 'password refresh_token', null, null);");

    }

    public function down()
    {
        $this->execute("DROP TABLE oauth_clients;");
        $this->execute("DROP TABLE oauth_access_tokens;");
        $this->execute("DROP TABLE oauth_authorization_codes;");
        $this->execute("DROP TABLE oauth_refresh_tokens;");
        $this->execute("DROP TABLE oauth_scopes;");
        $this->execute("DROP TABLE oauth_jwt;");
    }
}
