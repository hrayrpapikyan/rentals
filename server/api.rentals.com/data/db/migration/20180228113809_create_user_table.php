<?php


use Phinx\Migration\AbstractMigration;

class CreateUserTable extends AbstractMigration
{

    public function up()
    {
        $this->execute("
            create table user
            (
                id int unsigned not null auto_increment
                    primary key,
                password varchar(500) not null,
                first_name varchar(255) not null,
                last_name varchar(255) not null,
                email varchar(255) not null,
                type enum ('admin', 'realtor', 'client') not null,
                constraint user_email
                    unique (email)
            );"
        );

        $this->execute("alter table `user` add index user_email_password (email, password)");
        $this->execute("alter table `user` add index user_type (`type`)");
    }

    public function down()
    {
        $this->execute("DROP TABLE user");
    }
}
