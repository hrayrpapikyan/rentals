<?php


use Phinx\Migration\AbstractMigration;

class CreateAdminUser extends AbstractMigration
{

    public function up()
    {
        $this->execute("
        INSERT INTO user (password, first_name, last_name, email, type) VALUES ('$2y$10\$B.SNxylhRAhEy7fmpRfYxO.QB8yZjQdvYPlzhZZjkFE2QsmauoSNi', 'John', 'Doe', 'admin@rentals.com', 'admin');
        ");
    }

    public function down()
    {
        $this->execute("
        DELETE FROM user WHERE type = 'admin';
        ");
    }
}
