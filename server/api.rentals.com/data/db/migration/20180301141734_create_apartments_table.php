<?php


use Phinx\Migration\AbstractMigration;

class CreateApartmentsTable extends AbstractMigration
{

    public function up()
    {
        $this->execute("
        create table apartment
        (
          id int unsigned not null auto_increment
            primary key,
          created_at datetime not null,
          realtor_id int unsigned not null,
          status enum ('rentable', 'rented') not null,
          size decimal(5, 2) unsigned not null,
          price decimal(13, 2) unsigned not null,
          room_count tinyint unsigned not null,
          longitude varchar(50),
          latitude varchar(50),
          constraint fk_apartment_realtor_id foreign key (realtor_id) references user(id) on delete cascade
        );
        ");

        $this->execute("alter table apartment add index apartment_status (status)");
        $this->execute("alter table apartment add index apartment_size (`size`)");
        $this->execute("alter table apartment add index apartment_price (price)");
        $this->execute("alter table apartment add index apartment_room_count (room_count)");
        $this->execute("alter table apartment add index apartment_status_size (status, `size`)");
        $this->execute("alter table apartment add index apartment_status_price (status, price)");
        $this->execute("alter table apartment add index apartment_status_room_count (status, room_count)");

    }

    public function down()
    {
        $this->execute("drop table apartment");
    }
}
