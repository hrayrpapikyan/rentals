<?php
namespace Application;

use Application\CompleteRequest\CompleteRequestService;
use Rentals\V1\Rest\User\Exception\UserIsNotAdminException;
use Rentals\V1\Rest\User\Exception\UserIsNotAdminOrRealtorException;
use Rentals\V1\Rest\User\UserEntity;
use Rentals\V1\Rest\User\UserService;
use Zend\Log\LoggerInterface;
use ZF\Rest\AbstractResourceListener;

class BaseResource extends AbstractResourceListener
{
    /**
     * @var CompleteRequestService
     */
    protected $completeRequestService;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var UserEntity|null
     */
    protected $loggedInUser = null;


    public function __construct(
        UserService $userService,
        CompleteRequestService $completeRequestService,
        LoggerInterface $logger
    ) {
        $this->completeRequestService = $completeRequestService;
        $this->logger = $logger;
        $this->userService = $userService;

    }


    protected function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    protected function getCompleteRequestService(): CompleteRequestService
    {
        return $this->completeRequestService;
    }

    protected function getLoggedInUserId(): int
    {
        return $this->getIdentity()->getAuthenticationIdentity()['user_id'];
    }

    protected function getLoggedInUser(): UserEntity
    {
        if ($this->loggedInUser == null) {
            $this->loggedInUser = $this->getUserService()->getUser($this->getLoggedInUserId());
        }
        return $this->loggedInUser;
    }


    protected function checkIsAdmin(): void
    {
        if (!$this->getLoggedInUser()->isAdmin()) {
            throw new UserIsNotAdminException();
        }
    }

    protected function checkIsNotClient(): void
    {
        if ($this->getLoggedInUser()->isClient()) {
            throw new UserIsNotAdminOrRealtorException();
        }
    }

    protected function getUserService(): UserService
    {
        return $this->userService;
    }

}
