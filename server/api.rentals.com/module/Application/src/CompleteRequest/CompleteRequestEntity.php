<?php

namespace Application\CompleteRequest;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="complete_request")
 */
class CompleteRequestEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(name="uuid")
     */
    protected $uuid;

    /**
     * @ORM\Column(name="created_date")
     */
    protected $created_date;

    public function __construct(string $uuid, string $createdDate)
    {
        $this->uuid = $uuid;
        $this->created_date = $createdDate;
    }
}