<?php

namespace Application\CompleteRequest;

use Application\Constant\Constant;
use Application\Exception\DuplicateRequestException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class CompleteRequestService
{
    protected $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function checkRequest(string $uuid): void
    {
        $completeRequest = $this->getEntityManager()->getRepository(CompleteRequestEntity::class)->findOneBy([
            'uuid' => $uuid
        ]);

        if ($completeRequest) {
            throw new DuplicateRequestException();
        }

    }

    public function complete(string $uuid): void
    {
        $createdDate = date(Constant::DATABASE_DATE_TIME_FORMAT);
        $completeRequest = new CompleteRequestEntity($uuid, $createdDate);
        $this->getEntityManager()->persist($completeRequest);

        $this->getEntityManager()->flush();

    }

    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }


}