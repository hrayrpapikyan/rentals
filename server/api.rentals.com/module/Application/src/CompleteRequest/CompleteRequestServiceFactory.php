<?php

namespace Application\CompleteRequest;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class CompleteRequestServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new CompleteRequestService(
            $container->get('doctrine.entitymanager.orm_default')
        );
    }
}