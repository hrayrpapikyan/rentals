<?php

namespace Application\Constant;


class Constant
{
    const DATABASE_DATE_TIME_FORMAT = 'Y-m-d h:i:s';
    const GLOBAL_DATE_TIME_FORMAT = 'M j, Y H:i:s';
}