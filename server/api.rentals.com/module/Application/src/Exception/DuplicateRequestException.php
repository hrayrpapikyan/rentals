<?php

namespace Application\Exception;

class DuplicateRequestException extends \DomainException
{
    protected $message = "Duplicate request";
}