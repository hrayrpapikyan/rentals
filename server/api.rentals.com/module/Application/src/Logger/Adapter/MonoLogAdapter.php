<?php

namespace Application\Logger\Adapter;

use Zend\Log\LoggerInterface;

class MonoLogAdapter implements LoggerInterface
{
    /**
     * @var \Monolog\Logger
     */
    protected $monoLogger;

    public function __construct(\Monolog\Logger $logger)
    {
        $this->monoLogger = $logger;
    }

    /**
     * @param string $message
     * @param array|\Traversable $extra
     * @return LoggerInterface
     */
    public function emerg($message, $extra = [])
    {
        $this->monoLogger->emerg($message, $extra);
    }

    /**
     * @param string $message
     * @param array|\Traversable $extra
     * @return LoggerInterface
     */
    public function alert($message, $extra = [])
    {
        $this->monoLogger->alert($message, $extra);
    }

    /**
     * @param string $message
     * @param array|\Traversable $extra
     * @return LoggerInterface
     */
    public function crit($message, $extra = [])
    {
        $this->monoLogger->crit($message, $extra);
    }

    /**
     * @param string $message
     * @param array|\Traversable $extra
     * @return LoggerInterface
     */
    public function err($message, $extra = [])
    {
        $this->monoLogger->err($message, $extra);
    }

    /**
     * @param string $message
     * @param array|\Traversable $extra
     * @return LoggerInterface
     */
    public function warn($message, $extra = [])
    {
        $this->monoLogger->warn($message, $extra);
    }

    /**
     * @param string $message
     * @param array|\Traversable $extra
     * @return LoggerInterface
     */
    public function notice($message, $extra = [])
    {
        $this->monoLogger->notice($message, $extra);
    }

    /**
     * @param string $message
     * @param array|\Traversable $extra
     * @return LoggerInterface
     */
    public function info($message, $extra = [])
    {
        $this->monoLogger->info($message, $extra);
    }

    /**
     * @param string $message
     * @param array|\Traversable $extra
     * @return LoggerInterface
     */
    public function debug($message, $extra = [])
    {
        $this->monoLogger->debug($message, $extra);
    }
}