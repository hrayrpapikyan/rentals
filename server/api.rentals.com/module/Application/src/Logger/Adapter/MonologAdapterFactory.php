<?php

namespace Application\Logger\Adapter;


use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class MonoLogAdapterFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $monoLogger = new \Monolog\Logger('Rentals Logger');
        $monoLogger->pushHandler(new \Monolog\Handler\StreamHandler('app.log', \Monolog\Logger::WARNING));
        return new MonoLogAdapter(
            $monoLogger
        );
    }
}