<?php

namespace Application\Logger;


use Application\Logger\Adapter\MonoLogAdapter;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class LoggerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $loggerAdapter = new \Zend\Log\PsrLoggerAdapter(
            $container->get(MonoLogAdapter::class)
        );
        return $loggerAdapter->getLogger();
    }
}