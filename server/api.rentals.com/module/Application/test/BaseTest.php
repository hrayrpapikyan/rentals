<?php


namespace ApplicationTest;
use PHPUnit\Framework\TestCase;


class BaseTest extends TestCase
{

    protected function callProtectedMethod($obj, string $name, array $args = [])
    {
        $reflectionClass = new \ReflectionClass($obj);
        $method = $reflectionClass->getMethod($name);
        $method->setAccessible(true);
        return $method->invokeArgs($obj, $args);
    }


}
