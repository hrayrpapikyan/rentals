<?php

namespace ApplicationTest;

use Application\CompleteRequest\CompleteRequestEntity;
use Application\Constant\Constant;
use Application\Exception\DuplicateRequestException;
use Application\CompleteRequest\CompleteRequestService;


class CompleteRequestServiceTest extends ServiceBaseTest
{

    public function testComplete()
    {
        $completeRequestServiceMock = $this
            ->getMockBuilder(CompleteRequestService::class)
            ->setMethods(['getEntityManager'])
            ->disableOriginalConstructor()
            ->getMock();

        $entityManagerStub = $this->getEntityManagerStub();
        $completeRequestServiceMock->method('getEntityManager')->willReturn($entityManagerStub);

        $uuid = '624d5b7a-3e98-4344-bbb1-e7bebcbff66a';
        $completeRequestServiceMock->expects($this->exactly(2))->method('getEntityManager');
        $entityManagerStub->expects($this->once())->method('persist')->with(new CompleteRequestEntity($uuid, date(Constant::DATABASE_DATE_TIME_FORMAT)));
        $entityManagerStub->expects($this->once())->method('flush');
        $completeRequestServiceMock->complete($uuid);

    }

    public function testCheckRequestThrowsDuplicateRequestException()
    {
        $completeRequestServiceMock = $this
            ->getMockBuilder(CompleteRequestService::class)
            ->setMethods(['getEntityManager'])
            ->disableOriginalConstructor()
            ->getMock();

        $entityManagerStub = $this->getEntityManagerStub();
        $completeRequestServiceMock->method('getEntityManager')->willReturn($entityManagerStub);

        $repositoryStub = $this->getRepositoryStub();
        $entityManagerStub->method('getRepository')->willReturn($repositoryStub);

        $uuid = '624d5b7a-3e98-4344-bbb1-e7bebcbff66a';
        $repositoryStub->method('findOneBy')->willReturn(new CompleteRequestEntity($uuid, date(Constant::DATABASE_DATE_TIME_FORMAT)));

        $this->expectException(DuplicateRequestException::class);
        $completeRequestServiceMock->expects($this->once())->method('getEntityManager');
        $entityManagerStub->expects($this->once())->method('getRepository');
        $repositoryStub->expects($this->once())->method('findOneBy');


        $completeRequestServiceMock->checkRequest($uuid);

    }

    public function testCheckRequestSuccess()
    {
        $completeRequestServiceMock = $this
            ->getMockBuilder(CompleteRequestService::class)
            ->setMethods(['getEntityManager'])
            ->disableOriginalConstructor()
            ->getMock();

        $entityManagerStub = $this->getEntityManagerStub();
        $completeRequestServiceMock->method('getEntityManager')->willReturn($entityManagerStub);

        $repositoryStub = $this->getRepositoryStub();
        $entityManagerStub->method('getRepository')->willReturn($repositoryStub);

        $uuid = '624d5b7a-3e98-4344-bbb1-e7bebcbff66a';
        $repositoryStub->method('findOneBy')->willReturn(null);

        $completeRequestServiceMock->expects($this->once())->method('getEntityManager');
        $entityManagerStub->expects($this->once())->method('getRepository');
        $repositoryStub->expects($this->once())->method('findOneBy');

        $completeRequestServiceMock->checkRequest($uuid);
    }
}
