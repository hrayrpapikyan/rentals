<?php


namespace ApplicationTest;


use Application\CompleteRequest\CompleteRequestService;
use Application\Logger\Adapter\MonoLogAdapter;

class ResourceBaseTest extends BaseTest
{
    protected function getCompleteRequestServiceStub()
    {
        $competeRequestServiceStub = $this
            ->getMockBuilder(CompleteRequestService::class)
            ->setMethods([
                'checkRequest',
                'complete',
            ])
            ->disableOriginalConstructor()
            ->getMock();
        $competeRequestServiceStub->method('complete')->willReturn(null);
        return $competeRequestServiceStub;
    }

    protected function getLoggerStub()
    {

        $loggerStub = $this
            ->getMockBuilder(MonoLogAdapter::class)
            ->setMethods([
                'emerg',
                'alert',
                'crit',
                'err',
                'warn',
                'notice',
                'info',
                'debug',
            ])
            ->disableOriginalConstructor()
            ->getMock();
        $loggerStub->method('emerg')->willReturn(null);
        $loggerStub->method('alert')->willReturn(null);
        $loggerStub->method('crit')->willReturn(null);
        $loggerStub->method('err')->willReturn(null);
        $loggerStub->method('notice')->willReturn(null);
        $loggerStub->method('info')->willReturn(null);
        $loggerStub->method('debug')->willReturn(null);

        return $loggerStub;

    }


}
