<?php


namespace ApplicationTest;


class ServiceBaseTest extends BaseTest
{
    protected function getEntityManagerStub()
    {
        $entityManagerStub =
            $this->getMockBuilder(\Doctrine\ORM\EntityManager::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getRepository',
                'flush',
                'persist',
                'remove'
            ])->getMock();
        $entityManagerStub->method('persist')->willReturn(null);
        $entityManagerStub->method('flush')->willReturn(null);
        $entityManagerStub->method('remove')->willReturn(null);

        return $entityManagerStub;
    }

    protected function getRepositoryStub()
    {
        $repositoryStub =
            $this->getMockBuilder(\Doctrine\ORM\EntityManager::class)
                ->disableOriginalConstructor()
                ->setMethods([
                    'findOneBy',
                    'findBy',
                ])->getMock();

        return $repositoryStub;
    }


}
