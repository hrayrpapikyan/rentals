<?php
/**
 * @api {post} /oauth Get Access Token By password
 * @apiName GetAccessTokenByPassword
 * @apiGroup Oauth
 * @apiVersion 1.0.0
 *
 * @apiParam {String} grant_type Grand Type, shall be "password"
 * @apiParam {String} username User email.
 * @apiParam {String} password User password.
 *
 * @apiHeaderExample {json} Header
 *     {
 *       "Accept": "application/json",
 *       "Content-Type": "application/json",
 *       "Authorization": "Basic d3d3LnJlbnRhbHMuY29tOnJlbnRhbHMud2Vic2l0ZQ=="
 *     }
 *
 * @apiSuccessExample {json} Success-Response
 *     HTTP/1.1 200 OK
 *    {
 *    "access_token": "e2051ad8fea7c201b76b262ad2f9fb748e522ce5",
 *    "expires_in": 3600,
 *    "token_type": "Bearer",
 *    "scope": null,
 *    "refresh_token": "d5d3ec288188d6832c5f1ef3083227fa62ee66f4"*
 *    }
 *
 * @apiErrorExample {json} Error-Response Invalid grand
 *      HTTP/1.1 401 Invalid grand
 *      {
 *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
 *       "title": "invalid_grant",
 *       "status": 401,
 *       "detail": "Invalid username and password combination"
 *      }
 *
 */

/**
 * @api {post} /oauth Get Access Token By Refresh token
 * @apiName GetAccessTokenByRefreshToken
 * @apiGroup Oauth
 * @apiVersion 1.0.0
 *
 * @apiParam {String} grant_type Grand Type, shall be "refresh_token"
 * @apiParam {String} refresh_token Refresh token.
 *
 * @apiHeaderExample {json} Header
 *     {
 *       "Accept": "application/json",
 *       "Content-Type": "application/json",
 *       "Authorization": "Basic d3d3LnJlbnRhbHMuY29tOnJlbnRhbHMud2Vic2l0ZQ=="
 *     }
 *
 * @apiSuccessExample {json} Success-Response
 *     HTTP/1.1 200 OK
 *    {
 *    "access_token": "e2051ad8fea7c201b76b262ad2f9fb748e522ce5",
 *    "expires_in": 3600,
 *    "token_type": "Bearer",
 *    "scope": null
 *    }
 *
 * * @apiErrorExample {json} Error-Response Invalid grand
 *      HTTP/1.1 400 Invalid grand
 *      {
 *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
 *       "title": "invalid_grant",
 *       "status": 400,
 *       "detail": "Invalid refresh token"
 *      }
 */