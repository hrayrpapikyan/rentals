<?php
return [
    'doctrine' => [
        'driver' => [
            'Rentals_driver' => [
                'class' => \Doctrine\ORM\Mapping\Driver\AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [
                    0 => __DIR__ . '/../src/V1/Rest/User',
                ],
            ],
            'orm_default' => [
                'drivers' => [
                    'Rentals\\V1\\Rest\\User' => 'Rentals_driver',
                    'Rentals\\V1\\Rest\\Apartment' => 'Rentals_driver',
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            \Rentals\V1\Rest\User\UserResource::class => \Rentals\V1\Rest\User\UserResourceFactory::class,
            \Rentals\V1\Rest\User\UserService::class => \Rentals\V1\Rest\User\UserServiceFactory::class,
            \Rentals\V1\Rest\Apartment\ApartmentResource::class => \Rentals\V1\Rest\Apartment\ApartmentResourceFactory::class,
            \Rentals\V1\Rest\Apartment\ApartmentService::class => \Rentals\V1\Rest\Apartment\ApartmentServiceFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'rentals.rest.user' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/users[/:user_id]',
                    'defaults' => [
                        'controller' => 'Rentals\\V1\\Rest\\User\\Controller',
                    ],
                ],
            ],
            'rentals.rest.apartment' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/apartments[/:apartment_id]',
                    'defaults' => [
                        'controller' => 'Rentals\\V1\\Rest\\Apartment\\Controller',
                    ],
                ],
            ],
        ],
    ],
    'zf-versioning' => [
        'uri' => [
            0 => 'rentals.rest.user',
            1 => 'rentals.rest.apartment',
        ],
    ],
    'zf-rest' => [
        'Rentals\\V1\\Rest\\User\\Controller' => [
            'listener' => \Rentals\V1\Rest\User\UserResource::class,
            'route_name' => 'rentals.rest.user',
            'route_identifier_name' => 'user_id',
            'collection_name' => 'user',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'DELETE',
                2 => 'PATCH',
            ],
            'collection_http_methods' => [
                0 => 'POST',
                1 => 'GET',
            ],
            'collection_query_whitelist' => [
                0 => 'only_realtors',
                1 => 'page',
            ],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \Rentals\V1\Rest\User\UserEntity::class,
            'collection_class' => \Rentals\V1\Rest\User\UserCollection::class,
            'service_name' => 'User',
        ],
        'Rentals\\V1\\Rest\\Apartment\\Controller' => [
            'listener' => \Rentals\V1\Rest\Apartment\ApartmentResource::class,
            'route_name' => 'rentals.rest.apartment',
            'route_identifier_name' => 'apartment_id',
            'collection_name' => 'apartment',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [
                0 => 'size_from',
                1 => 'size_to',
                2 => 'price_from',
                3 => 'price_to',
                4 => 'room_count_from',
                5 => 'room_count_to',
                6 => 'page',
            ],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \Rentals\V1\Rest\Apartment\ApartmentEntity::class,
            'collection_class' => \Rentals\V1\Rest\Apartment\ApartmentCollection::class,
            'service_name' => 'Apartment',
        ],
    ],
    'zf-content-negotiation' => [
        'controllers' => [
            'Rentals\\V1\\Rest\\User\\Controller' => 'Json',
            'Rentals\\V1\\Rest\\Apartment\\Controller' => 'Json',
        ],
        'accept_whitelist' => [
            'Rentals\\V1\\Rest\\User\\Controller' => [
                0 => 'application/vnd.rentals.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'Rentals\\V1\\Rest\\Apartment\\Controller' => [
                0 => 'application/vnd.rentals.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
        ],
        'content_type_whitelist' => [
            'Rentals\\V1\\Rest\\User\\Controller' => [
                0 => 'application/vnd.rentals.v1+json',
                1 => 'application/json',
            ],
            'Rentals\\V1\\Rest\\Apartment\\Controller' => [
                0 => 'application/vnd.rentals.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'zf-hal' => [
        'metadata_map' => [
            \Rentals\V1\Rest\User\UserEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'rentals.rest.user',
                'route_identifier_name' => 'user_id',
                'hydrator' => \Zend\Hydrator\Reflection::class,
            ],
            \Rentals\V1\Rest\User\UserCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'rentals.rest.user',
                'route_identifier_name' => 'user_id',
                'is_collection' => true,
            ],
            \Rentals\V1\Rest\Apartment\ApartmentEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'rentals.rest.apartment',
                'route_identifier_name' => 'apartment_id',
                'hydrator' => \Zend\Hydrator\Reflection::class,
            ],
            \Rentals\V1\Rest\Apartment\ApartmentCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'rentals.rest.apartment',
                'route_identifier_name' => 'apartment_id',
                'is_collection' => true,
            ],
        ],
    ],
    'zf-mvc-auth' => [
        'authorization' => [
            'Rentals\\V1\\Rest\\User\\Controller' => [
                'collection' => [
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ],
                'entity' => [
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => true,
                    'DELETE' => true,
                ],
            ],
            'Rentals\\V1\\Rest\\Apartment\\Controller' => [
                'collection' => [
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ],
                'entity' => [
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => true,
                    'DELETE' => true,
                ],
            ],
        ],
    ],
    'zf-content-validation' => [
        'Rentals\\V1\\Rest\\User\\Controller' => [
            'input_filter' => 'Rentals\\V1\\Rest\\User\\Validator',
        ],
        'Rentals\\V1\\Rest\\Apartment\\Controller' => [
            'input_filter' => 'Rentals\\V1\\Rest\\Apartment\\Validator',
        ],
    ],
    'input_filter_specs' => [
        'Rentals\\V1\\Rest\\User\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StripTags::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Zend\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'first_name',
                'field_type' => 'text',
            ],
            1 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StripTags::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Zend\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'last_name',
            ],
            2 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\EmailAddress::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StringTrim::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Zend\Filter\StripTags::class,
                        'options' => [],
                    ],
                ],
                'name' => 'email',
            ],
            3 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [
                            'min' => 8,
                            'max' => 20,
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StripTags::class,
                        'options' => [],
                    ],
                ],
                'name' => 'password',
            ],
            4 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\Uuid::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StripTags::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Zend\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'uuid',
            ],
            5 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\InArray::class,
                        'options' => [
                            'haystack' => [
                                0 => 'realtor',
                                1 => 'client',
                            ],
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StripTags::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Zend\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'type',
            ],
            6 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\Identical::class,
                        'options' => [
                            'token' => 'password',
                            'message' => 'Password confirmation is wrong',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StripTags::class,
                        'options' => [],
                    ],
                ],
                'name' => 'password_confirm',
            ],
            7 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [
                            'min' => 8,
                            'max' => 20,
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StripTags::class,
                        'options' => [],
                    ],
                ],
                'name' => 'current_password',
            ],
        ],
        'Rentals\\V1\\Rest\\Apartment\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\Uuid::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StripTags::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Zend\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'uuid',
            ],
            1 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\Digits::class,
                    ],
                    1 => [
                        'name' => \Zend\Validator\GreaterThan::class,
                        'options' => [
                            'min' => 0,
                            'inclusive' => false,
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StripTags::class,
                        'options' => [],
                    ],
                ],
                'name' => 'realtor_id',
            ],
            2 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\InArray::class,
                        'options' => [
                            'haystack' => [
                                0 => 'rentable',
                                1 => 'rented',
                            ],
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StripTags::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Zend\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'status',
            ],
            3 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\GreaterThan::class,
                        'options' => [
                            'min' => 0,
                            'inclusive' => false,
                        ],
                    ],
                    1 => [
                        'name' => \Zend\Validator\LessThan::class,
                        'options' => [
                            'max' => 1000,
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StripTags::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Zend\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'size',
            ],
            4 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\GreaterThan::class,
                        'options' => [
                            'min' => 0,
                            'inclusive' => false,
                        ],
                    ],
                    1 => [
                        'name' => \Zend\Validator\LessThan::class,
                        'options' => [
                            'max' => 1000000,
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StripTags::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Zend\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'price',
            ],
            5 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\Digits::class,
                    ],
                    1 => [
                        'name' => \Zend\Validator\GreaterThan::class,
                        'options' => [
                            'min' => 0,
                            'inclusive' => false,
                        ],
                    ],
                    2 => [
                        'name' => \Zend\Validator\LessThan::class,
                        'options' => [
                            'max' => 10,
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StripTags::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Zend\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'room_count',
            ],
            6 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\GreaterThan::class,
                        'options' => [
                            'min' => -180,
                            'inclusive' => true,
                        ],
                    ],
                    1 => [
                        'name' => \Zend\Validator\LessThan::class,
                        'options' => [
                            'max' => 180,
                            'inclusive' => true,
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StripTags::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Zend\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'longitude',
            ],
            7 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\GreaterThan::class,
                        'options' => [
                            'min' => -85.05112878,
                            'inclusive' => true,
                        ],
                    ],
                    1 => [
                        'name' => \Zend\Validator\LessThan::class,
                        'options' => [
                            'max' => 85.05112878,
                            'inclusive' => true,
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StripTags::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Zend\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'latitude',
            ],
        ],
    ],
];
