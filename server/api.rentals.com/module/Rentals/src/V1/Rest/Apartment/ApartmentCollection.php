<?php
namespace Rentals\V1\Rest\Apartment;

use Zend\Paginator\Paginator;

class ApartmentCollection extends Paginator
{
    protected static $defaultItemCountPerPage = 3;

}
