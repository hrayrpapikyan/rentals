<?php

namespace Rentals\V1\Rest\Apartment;

use Application\Constant\Constant;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="apartment")
 */
class ApartmentEntity implements \JsonSerializable
{
    const STATUS_RENTED = 'rented';
    const STATUS_RENTABLE = 'rentable';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id")
     */
    protected $id;

    /**
     * @ORM\Column(name="created_at")
     */
    protected $created_at;


    /**
     * @ORM\Column(name="status")
     */
    protected $status;

    /**
     * @ORM\Column(name="size")
     */
    protected $size;

    /**
     * @ORM\Column(name="price")
     */
    protected $price;

    /**
     * @ORM\Column(name="room_count")
     */
    protected $room_count;

    /**
     * @ORM\Column(name="longitude")
     */
    protected $longitude;

    /**
     * @ORM\Column(name="latitude")
     */
    protected $latitude;

    /**
     * @ORM\ManyToOne(targetEntity="\Rentals\V1\Rest\User\UserEntity", inversedBy="apartment")
     * @ORM\JoinColumn(name="realtor_id", referencedColumnName="id")
     * @var \Rentals\V1\Rest\User\UserEntity
     */
    protected $realtor;


    public function getId()
    {
        return $this->id;
    }


    public function setId($id)
    {
        $this->id = $id;
    }


    public function getCreatedAt()
    {
        return $this->created_at;
    }


    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }




    public function getStatus()
    {
        return $this->status;
    }


    public function setStatus($status)
    {
        $this->status = $status;
    }


    public function getSize()
    {
        return $this->size;
    }


    public function setSize($size)
    {
        $this->size = $size;
    }


    public function getPrice()
    {
        return $this->price;
    }


    public function setPrice($price)
    {
        $this->price = $price;
    }


    public function getRoomCount()
    {
        return $this->room_count;
    }


    public function setRoomCount($room_count)
    {
        $this->room_count = $room_count;
    }


    public function getLongitude()
    {
        return $this->longitude;
    }


    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }


    public function getLatitude()
    {
        return $this->latitude;
    }


    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    public function getRealtor(): \Rentals\V1\Rest\User\UserEntity
    {
        return $this->realtor;
    }


    public function setRealtor(\Rentals\V1\Rest\User\UserEntity $realtor): void
    {
        $this->realtor = $realtor;
    }


    public function jsonSerialize()
    {
        $that = get_object_vars($this);
        $that['created_at'] = date(Constant::GLOBAL_DATE_TIME_FORMAT, strtotime($this->created_at));
        $that['realtor_id'] = $this->realtor->getId();
        $that['realtor_name'] = $this->realtor->getFirstName() . ' ' . $this->realtor->getLastName();
        unset($that['realtor']);
        return $that;
    }
}
