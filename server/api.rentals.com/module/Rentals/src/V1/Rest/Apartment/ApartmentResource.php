<?php
namespace Rentals\V1\Rest\Apartment;

use Application\BaseResource;
use Application\CompleteRequest\CompleteRequestService;
use Application\Constant\Error;
use Application\Exception\DuplicateRequestException;
use Rentals\V1\Rest\Apartment\Exception\ApartmentNotFoundException;
use Rentals\V1\Rest\Apartment\Exception\RealtorCanNotCRUDOtherRealtorsApartmentsException;
use Rentals\V1\Rest\User\Exception\UserIsNotAdminException;
use Rentals\V1\Rest\User\Exception\UserIsNotAdminOrRealtorException;
use Rentals\V1\Rest\User\Exception\UserNotFoundException;
use Rentals\V1\Rest\User\UserService;
use Zend\Log\LoggerInterface;
use ZF\ApiProblem\ApiProblem;

class ApartmentResource extends BaseResource
{
    /**
     * @var ApartmentService
     */
    protected $apartmentService;


    public function __construct(
        ApartmentService $apartmentService,
        UserService $userService,
        CompleteRequestService $completeRequestService,
        LoggerInterface $logger
    ) {
        $this->apartmentService = $apartmentService;
        parent::__construct($userService, $completeRequestService, $logger);

    }

    /**
     * @api {post} /apartments Create Apartment
     * @apiName CreateApartment
     * @apiGroup Apartment
     * @apiVersion 1.0.0
     *
     * @apiParam {String} uuid Request unique ID.
     * @apiParam {Float} price Monthly price.
     * @apiParam {Float} size Apartment size.
     * @apiParam {Integer} room_count Number of rooms.
     * @apiParam {Integer} [realtor_id] Id of attached realtor user. If Creator is realtor, he will be the attached realtor
     * @apiParam {Float} longitude Geographical Longitude.
     * @apiParam {String} latitude Geographical Latitude.
     *
     * @apiHeaderExample {json} Header
     *     {
     *       "Authorization": "Bearer 611b2f9900a34863b869b43fa33b6e6444f5dd2a",
     *       "Content-Type": "application/json",
     *     }
     *
     * @apiSuccessExample {json} Success-Response
     *     HTTP/1.1 201 Created
     *       {
     *       "id": 3,
     *       "created_at": "Mar 1, 2018 11:46:29",
     *       "status": "rentable",
     *       "size": 80.3,
     *       "price": 15.23,
     *       "room_count": 7,
     *       "longitude": "44.503490",
     *       "latitude": "40.177200",
     *       "realtor_id": 3,
     *       "realtor_name": "John Doe"
     *       }
     *
     * @apiErrorExample {json} Error-Response Duplicate Request
     *      HTTP/1.1 409 Conflict
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Conflict",
     *       "status": 409,
     *       "detail": "Duplicate request"
     *      }
     *
     * @apiErrorExample {json} Error-Response Invalid argument
     *      HTTP/1.1 400 Bad Request
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Bad Request",
     *       "status": 400,
     *       "detail": "Selected user is not a realtor"
     *      }
     *
     * @apiErrorExample {json} Error-Response User Is Not Admin or Realtor
     *      HTTP/1.1 403 Forbidden
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Forbidden",
     *       "status": 403,
     *       "detail": "Only Admin User and Realtors can perform this action"
     *      }
     *
     *
     * @apiErrorExample {json} Error-Response Validation
     *      HTTP/1.1 422 Unprocessable Entity
     *      {
     *           "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *           "title": "Bad Request",
     *           "status": 422,
     *           "detail": "Failed Validation",
     *           "validation_messages": {
     *               "uuid": {
     *               "isEmpty": "Value is required and can't be empty"
     *               },
     *               "price": {
     *               "isEmpty": "Value is required and can't be empty"
     *               },
     *               "room_count": {
     *               "isEmpty": "Value is required and can't be empty"
     *               },
     *               "longitude": {
     *               "mustBeLessThan": "The input is not less or equal than '180'"
     *               },
     *               "latitude": {
     *               "mustBeGreaterThan": "TThe input is not greater than or equal to '-85.05112878'"
     *               },
     *               "size": {
     *               "isEmpty": "The input is not greater than '0'"
     *               }
     *           },
     *      }
     *
     *
     * @apiErrorExample {json} Error-Response Unknown Error
     *      HTTP/1.1 500 Internal Server Error
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Internal Server Error",
     *       "status": 500,
     *       "detail": "Server side problem"
     *      }
     */
    public function create($data)
    {
        try {
            $requestUuid = $data->uuid;
            $this->checkIsNotClient();
            $this->getCompleteRequestService()->checkRequest($requestUuid);
            $apartmentEntity = $this->getApartmentService()->createApartment((array) $data, $this->getLoggedInUser());

            $this->getCompleteRequestService()->complete($requestUuid);

            return $apartmentEntity;
        } catch (DuplicateRequestException $e) {
            return new ApiProblem(409, $e->getMessage());
        } catch (UserIsNotAdminOrRealtorException $e) {
            return new ApiProblem(403, $e->getMessage());
        } catch (\InvalidArgumentException | UserNotFoundException $e) {
            return new ApiProblem(400, $e->getMessage());
        } catch (\Throwable $e) {
            $this->getLogger()->err($e->getMessage());
            return new ApiProblem(500, Error::SERVER_PROBLEM);
        }
    }

    /**
     * @api {delete} /apartments/:id Delete Apartment
     * @apiName DeleteApartment
     * @apiGroup Apartment
     * @apiVersion 1.0.0
     *
     * @apiHeaderExample {json} Header
     *     {
     *       "Authorization": "Bearer 611b2f9900a34863b869b43fa33b6e6444f5dd2a",
     *       "Content-Type": "application/json",
     *     }
     *
     * @apiSuccessExample {json} Success-Response
     *     HTTP/1.1 204 Deleted
     *
     * @apiErrorExample {json} Error-Response Apartment Not Found
     *      HTTP/1.1 404 Not Found
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Not Found",
     *       "status": 404,
     *       "detail": "Apartment not found"
     *      }
     *
     * @apiErrorExample {json} Error-Response User Is Not Admin or Realtor
     *      HTTP/1.1 403 Forbidden
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Forbidden",
     *       "status": 403,
     *       "detail": "Only Admin User and Realtors can perform this action"
     *      }
     *
     * @apiErrorExample {json} Error-Response Realtor can not CRUD other realtor's apartments
     *      HTTP/1.1 403 Forbidden
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Forbidden",
     *       "status": 403,
     *       "detail": "Realtor does not have access to other realtors apartments"
     *      }
     *
     * @apiErrorExample {json} Error-Response Unknown Error
     *      HTTP/1.1 500 Internal Server Error
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Internal Server Error",
     *       "status": 500,
     *       "detail": "Server side problem"
     *      }
     *
     *
     */
    public function delete($id)
    {
        try {
            $this->checkIsNotClient();
            $this->getApartmentService()->deleteApartment($id, $this->getLoggedInUser());
            return true;
        } catch (ApartmentNotFoundException $e) {
            return new ApiProblem(404, $e->getMessage());
        } catch (UserIsNotAdminOrRealtorException | RealtorCanNotCRUDOtherRealtorsApartmentsException $e) {
            return new ApiProblem(403, $e->getMessage());
        } catch (\Throwable $e) {
            $this->getLogger()->err($e->getMessage());
            return new ApiProblem(500, Error::SERVER_PROBLEM);
        }
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }


    /**
     * @api {get} /apartments/:id Get Apartment information
     * @apiName GetApartment
     * @apiGroup Apartment
     * @apiVersion 1.0.0
     *
     * @apiParam id Apartment unique ID, "me" to get current user.
     *
     * @apiHeaderExample {json} Header
     *     {
     *       "Authorization": "Bearer 611b2f9900a34863b869b43fa33b6e6444f5dd2a",
     *       "Content-Type": "application/json",
     *     }
     *
     * @apiSuccessExample {json} Success-Response
     *     HTTP/1.1 200 Ok
     *     {
     *      "id": 1,
     *      "created_at": "Mar 1, 2018 11:23:17",
     *      "status": "rentable",
     *      "size": "80.30",
     *      "price": "15.23",
     *      "room_count": "7",
     *      "longitude": "44.503490",
     *      "latitude": "40.177200",
     *      "realtor_name": "John Doe",
     *     }
     *
     * @apiErrorExample {json} Error-Response Apartment Not Found
     *      HTTP/1.1 404 Not Found
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Not Found",
     *       "status": 404,
     *       "detail": "Apartment not found"
     *      }
     *
     * @apiErrorExample {json} Error-Response User Is Not Admin or Realtor
     *      HTTP/1.1 403 Forbidden
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Forbidden",
     *       "status": 403,
     *       "detail": "Only Admin User and Realtors can perform this action"
     *      }
     *
     * @apiErrorExample {json} Error-Response Realtor can not CRUD other realtor's apartments
     *      HTTP/1.1 403 Forbidden
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Forbidden",
     *       "status": 403,
     *       "detail": "Realtor does not have access to other realtors apartments"
     *      }
     *
     * @apiErrorExample {json} Error-Response Unknown Error
     *      HTTP/1.1 500 Internal Server Error
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Internal Server Error",
     *       "status": 500,
     *       "detail": "Server side problem"
     *      }
     *
     */
    public function fetch($id)
    {
        try {

            $this->checkIsNotClient();
            return $this->getApartmentService()->getApartment($id, $this->getLoggedInUser());

        } catch (ApartmentNotFoundException $e) {
            return new ApiProblem(404, $e->getMessage());
        } catch (UserIsNotAdminOrRealtorException | RealtorCanNotCRUDOtherRealtorsApartmentsException $e) {
            return new ApiProblem(403, $e->getMessage());
        } catch (\Throwable $e) {
            $this->getLogger()->err($e->getMessage());
            return new ApiProblem(500, Error::SERVER_PROBLEM);
        }
    }

    /**
     * @api {get} /apartments Get Paginated Apartments
     * @apiName GetApartments
     * @apiGroup Apartment
     * @apiVersion 1.0.0
     *
     * @apiParam {Integer} [page] Current page number
     * @apiParam {Float} [size_from] Apartment size from
     * @apiParam {Float} [size_to] Apartment size to
     * @apiParam {Float} [price_from] Apartment monthly price from
     * @apiParam {Float} [price_to] Apartment monthly price to
     * @apiParam {Integer={1..10}} [room_count_from] Number of room in apartment from
     * @apiParam {Integer={1..10}} [room_count_to] Number of room in apartment to
     *
     *
     * @apiHeaderExample {json} Header
     *     {
     *       "Authorization": "Bearer 611b2f9900a34863b869b43fa33b6e6444f5dd2a",
     *       "Content-Type": "application/json",
     *     }
     *
     * @apiSuccessExample {json} Success-Response
     *     HTTP/1.1 200 Ok
     *   {
     *       "users": [
     *           {
     *           "id": "8",
     *           "created_at": "Mar 2, 2018 12:42:33",
     *           "status": "rentable",
     *           "size": "80.30",
     *           "price": "15.23",
     *           "room_count": "8",
     *           "longitude": "44.503490",
     *           "latitude": "40.177200",
     *           "realtor_id": "5",
     *           "realtor_name": "John Doe",
     *           "may_crud": 1
     *           }
     *       ],
     *       "total": 1,
     *       "per_page": 10,
     *       "page": 1,
     *       "page_count": 1
     *  }
     *
     *
     *
     * @apiErrorExample {json} Error-Response Unknown Error
     *      HTTP/1.1 500 Internal Server Error
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Internal Server Error",
     *       "status": 500,
     *       "detail": "Server side problem"
     *      }
     *
     */
    public function fetchAll($params = [])
    {
        try {
            return $this->getApartmentService()->getApartments((array) $params, $this->getLoggedInUser());
        } catch (\Throwable $e) {
            $this->getLogger()->err($e->getMessage());
            return new ApiProblem(500, Error::SERVER_PROBLEM);
        }
    }

    /**
     * @api {patch} /apartments/:id Update Apartment
     * @apiName UpdateApartment
     * @apiGroup Apartment
     * @apiVersion 1.0.0
     *
     * @apiParam {Integer} id Apartments unique ID.
     * @apiParam {Float} [price] Monthly price.
     * @apiParam {Float} [size] Apartment size.
     * @apiParam {Integer} [room_count] Number of rooms.
     * @apiParam {Integer} [realtor_id] Id of attached realtor user. If Creator is realtor, he will be the attached realtor
     * @apiParam {Float} [longitude] Geographical Longitude.
     * @apiParam {String} [latitude] Geographical Latitude.
     *
     * @apiHeaderExample {json} Header
     *     {
     *       "Authorization": "Bearer 611b2f9900a34863b869b43fa33b6e6444f5dd2a",
     *       "Content-Type": "application/json",
     *     }
     *
     * @apiSuccessExample {json} Success-Response
     *     HTTP/1.1 200 OK
     *       {
     *       "price": 15.23,
     *       "room_count": 8,
     *       "longitude": "44.503490",
     *       "latitude": "40.177200",
     *       "size": 80.3,
     *       "realtor_id": 5
     *       }
     *
     * @apiErrorExample {json} Error-Response User Not Found
     *      HTTP/1.1 404 Not Found
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Not Found",
     *       "status": 404,
     *       "detail": "User not found"
     *      }
     *
     *
     *
     * @apiErrorExample {json} Error-Response Apartment Not Found
     *      HTTP/1.1 404 Not Found
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Not Found",
     *       "status": 404,
     *       "detail": "Apartment not found"
     *      }
     *
     * @apiErrorExample {json} Error-Response User Is Not Admin or Realtor
     *      HTTP/1.1 403 Forbidden
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Forbidden",
     *       "status": 403,
     *       "detail": "Only Admin User and Realtors can perform this action"
     *      }
     *
     * @apiErrorExample {json} Error-Response Realtor can not CRUD other realtor's apartments
     *      HTTP/1.1 403 Forbidden
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Forbidden",
     *       "status": 403,
     *       "detail": "Realtor does not have access to other realtors apartments"
     *      }
     *
     * @apiErrorExample {json} Error-Response Validation
     *      HTTP/1.1 422 Unprocessable Entity
     *      {
     *           "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *           "title": "Bad Request",
     *           "status": 422,
     *           "detail": "Failed Validation",
     *           "validation_messages": {
     *               "uuid": {
     *               "isEmpty": "Value is required and can't be empty"
     *               },
     *               "price": {
     *               "isEmpty": "Value is required and can't be empty"
     *               },
     *               "room_count": {
     *               "isEmpty": "Value is required and can't be empty"
     *               },
     *               "longitude": {
     *               "mustBeLessThan": "The input is not less or equal than '180'"
     *               },
     *               "latitude": {
     *               "mustBeGreaterThan": "TThe input is not greater than or equal to '-85.05112878'"
     *               },
     *               "size": {
     *               "isEmpty": "The input is not greater than '0'"
     *               }
     *           },
     *      }
     *
     *
     *
     * @apiErrorExample {json} Error-Response Unknown Error
     *      HTTP/1.1 500 Internal Server Error
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Internal Server Error",
     *       "status": 500,
     *       "detail": "Server side problem"
     *      }
     *
     */
    public function patch($id, $data)
    {
        try {
            $this->checkIsNotClient();
            return $this->getApartmentService()->updateApartment($id, (array) $data, $this->getLoggedInUser());
        } catch (ApartmentNotFoundException | UserNotFoundException $e) {
            return new ApiProblem(404, $e->getMessage());
        } catch (UserIsNotAdminOrRealtorException
        | RealtorCanNotCRUDOtherRealtorsApartmentsException
        | UserIsNotAdminException $e) {
            return new ApiProblem(403, $e->getMessage());
        } catch (\Throwable $e) {
            $this->getLogger()->err($e->getMessage());
            return new ApiProblem(500, Error::SERVER_PROBLEM);
        }
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }

    protected function getApartmentService(): ApartmentService
    {
        return $this->apartmentService;
    }

}
