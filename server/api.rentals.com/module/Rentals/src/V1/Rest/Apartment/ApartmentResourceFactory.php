<?php
namespace Rentals\V1\Rest\Apartment;

use Application\CompleteRequest\CompleteRequestService;
use Rentals\V1\Rest\User\UserService;

class ApartmentResourceFactory
{
    public function __invoke($services): ApartmentResource
    {
        return new ApartmentResource(
            $services->get(ApartmentService::class),
            $services->get(UserService::class),
            $services->get(CompleteRequestService::class),
            $services->get('logger')
        );
    }
}
