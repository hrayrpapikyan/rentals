<?php

namespace Rentals\V1\Rest\Apartment;

use Application\Constant\Constant;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator;
use Rentals\V1\Rest\Apartment\Exception\ApartmentNotFoundException;
use Rentals\V1\Rest\Apartment\Exception\RealtorCanNotCRUDOtherRealtorsApartmentsException;
use Rentals\V1\Rest\User\Exception\UserIsNotAdminException;
use Rentals\V1\Rest\User\UserEntity;
use Rentals\V1\Rest\User\UserService;
use Zend\Hydrator\Reflection;
use Doctrine\ORM\Tools\Pagination\Paginator as OrmPaginator;


class ApartmentService
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var UserService
     */
    protected $userService;

    public function __construct(EntityManager $entityManager, UserService $userService)
    {
        $this->entityManager = $entityManager;
        $this->userService = $userService;
    }

    public function createApartment(array $data, UserEntity $loggedInUser) : ApartmentEntity
    {
        $apartmentEntity = $this->hydrateApartmentEntity($data);
        $apartmentEntity->setCreatedAt(date(Constant::DATABASE_DATE_TIME_FORMAT));
        $apartmentEntity->setStatus(ApartmentEntity::STATUS_RENTABLE);
        if ($loggedInUser->isAdmin()) {
            if (empty($data['realtor_id'])) {
                throw new \InvalidArgumentException('Realtor must be selected');
            }
            $realtor = $this->getUserService()->getUser($data['realtor_id']);
            if (!$realtor->isRealtor()) {
                throw new \InvalidArgumentException('Selected user is not a realtor');
            }
        } else {
            $realtor = $loggedInUser;
        }

        $apartmentEntity->setRealtor($realtor);
        $this->getEntityManager()->persist($apartmentEntity);
        $this->getEntityManager()->flush();

        return $apartmentEntity;
    }




    public function getApartment(int $id, UserEntity $loggedInUser) : ApartmentEntity
    {
        /**
         * @var ApartmentEntity $apartmentEntity
         */
        $apartmentEntity = $this->getEntityManager()->getRepository(ApartmentEntity::class)->findOneBy(
            ['id'=> $id]
        );

        if (!$apartmentEntity) {
            throw new ApartmentNotFoundException();
        }

        if ($loggedInUser->isRealtor() && $apartmentEntity->getRealtor()->getId() != $loggedInUser->getId()) {
            throw new RealtorCanNotCRUDOtherRealtorsApartmentsException();
        }

        return $apartmentEntity;
    }

    public function deleteApartment(int $id, UserEntity $loggedInUser): void
    {
        $apartmentEntity = $this->getApartment($id, $loggedInUser);
        $this->getEntityManager()->remove($apartmentEntity);
        $this->getEntityManager()->flush();

    }




    protected function hydrateApartmentEntity(array $data, ApartmentEntity $apartmentEntity = null) : ApartmentEntity
    {
        if (is_null($apartmentEntity)) {
            $apartmentEntity = new ApartmentEntity();
        }

        $hydrator = new Reflection();
        $hydrator->hydrate($data, $apartmentEntity);
        return $apartmentEntity;
    }



    protected function getApartmentPaginatedCollection(array $params, UserEntity $loggedInUser)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('a')->from(ApartmentEntity::class, 'a');

        $parameters = [];
        if ($loggedInUser->isClient()) {
            $queryBuilder
                ->where('a.status = :status');
            $parameters['status'] = ApartmentEntity::STATUS_RENTABLE;

        }

        if (!empty($params['size_from'])) {
            $queryBuilder
                ->andWhere('a.size >= :size_from');
            $parameters['size_from'] = $params['size_from'];
        }

        if (!empty($params['size_to'])) {
            $queryBuilder
                ->andWhere('a.size <= :size_to');
            $parameters['size_to'] = $params['size_to'];
        }

        if (!empty($params['price_from'])) {
            $queryBuilder
                ->andWhere('a.price >= :price_from');
            $parameters['price_from'] = $params['price_from'];
        }

        if (!empty($params['price_to'])) {
            $queryBuilder
                ->andWhere('a.price <= :price_to');
            $parameters['price_to'] = $params['price_to'];
        }

        if (!empty($params['room_count_from'])) {
            $queryBuilder
                ->andWhere('a.room_count >= :room_count_from');
            $parameters['room_count_from'] = $params['room_count_from'];
        }

        if (!empty($params['room_count_to'])) {
            $queryBuilder
                ->andWhere('a.room_count <= :room_count_to');
            $parameters['room_count_to'] = $params['room_count_to'];
        }



        if (count($parameters)) {
            $queryBuilder->setParameters($parameters);
        }

        $query = $queryBuilder->getQuery();

        $ormPaginator = new OrmPaginator($query);
        $doctrinePaginator = new DoctrinePaginator($ormPaginator);
        $collection = new ApartmentCollection($doctrinePaginator);
        return $collection;
    }

    public function updateApartment(int $id, array $data, UserEntity $loggedInUser): ApartmentEntity
    {
        $apartmentEntity = $this->getApartment($id, $loggedInUser);
        $this->hydrateApartmentEntity($data, $apartmentEntity);
        if (!empty($data['realtor_id'])) {
            if (!$loggedInUser->isAdmin()) {
                throw new UserIsNotAdminException();
            }
            if ($apartmentEntity->getRealtor()->getId() != $data['realtor_id']) {
                $realtor = $this->getUserService()->getUser($data['realtor_id']);
                $apartmentEntity->setRealtor($realtor);
            }
        }
        $this->getEntityManager()->persist($apartmentEntity);
        $this->getEntityManager()->flush();
        return $apartmentEntity;
    }

    public function getApartments(array $params, UserEntity $loggedInUser): array
    {
        $currentPageNumber = $params['page'] ?? 1;
        $collection = $this->getApartmentPaginatedCollection($params, $loggedInUser);

        $collection->setCurrentPageNumber($currentPageNumber);
        $result = [];
        $result['apartments'] = [];
        /**
         * @var ApartmentEntity $apartment
         */
        $i = 0;
        foreach ($collection->getCurrentItems() as $apartment) {
            $result['apartments'][$i] = $apartment->jsonSerialize();
            if ($loggedInUser->isAdmin()) {
                $result['apartments'][$i]['may_crud'] = 1;
            } elseif ($loggedInUser->isRealtor() && $apartment->getRealtor()->getId() == $loggedInUser->getId()) {
                $result['apartments'][$i]['may_crud'] = 1;
            } else {
                $result['apartments'][$i]['may_crud'] = 0;
            }
            $i++;
        }
        return array_merge($result, [
            'total' => $collection->getTotalItemCount(),
            "per_page" => $collection->getItemCountPerPage(),
            "page" => $collection->getCurrentPageNumber(),
            "page_count" => $collection->count()
        ]);
    }



    protected function getUserService()
    {
        return $this->userService;
    }


    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }
}