<?php

namespace Rentals\V1\Rest\Apartment;


use Interop\Container\ContainerInterface;
use Rentals\V1\Rest\User\UserService;
use Zend\ServiceManager\Factory\FactoryInterface;

class ApartmentServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): ApartmentService
    {
        return new ApartmentService(
            $container->get('doctrine.entitymanager.orm_default'),
            $container->get(UserService::class)
        );
    }
}