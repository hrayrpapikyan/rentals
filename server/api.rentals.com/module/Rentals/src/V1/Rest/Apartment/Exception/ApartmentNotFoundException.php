<?php

namespace Rentals\V1\Rest\Apartment\Exception;

class ApartmentNotFoundException extends \DomainException
{
    protected $message = "Apartment not found";
}