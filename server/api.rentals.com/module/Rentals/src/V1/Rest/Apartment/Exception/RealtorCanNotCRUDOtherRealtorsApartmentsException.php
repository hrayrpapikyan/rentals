<?php

namespace Rentals\V1\Rest\Apartment\Exception;

class RealtorCanNotCRUDOtherRealtorsApartmentsException extends \DomainException
{
    protected $message = "Realtor does not have access to other realtors apartments";
}