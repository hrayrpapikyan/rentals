<?php

namespace Rentals\V1\Rest\User\Exception;

class AdminUserCanNotBeDeletedException extends \DomainException
{
    protected $message = "Admin User can not be deleted";
}