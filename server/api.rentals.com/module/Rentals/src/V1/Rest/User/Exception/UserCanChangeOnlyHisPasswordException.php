<?php

namespace Rentals\V1\Rest\User\Exception;

class UserCanChangeOnlyHisPasswordException extends \DomainException
{
    protected $message = "User may change only his/her password";
}