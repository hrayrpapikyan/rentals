<?php

namespace Rentals\V1\Rest\User\Exception;

class UserEmailBusyException extends \DomainException
{
    protected $message = "Email is already used";
}