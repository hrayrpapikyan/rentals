<?php

namespace Rentals\V1\Rest\User\Exception;

class UserIsNotAdminException extends \DomainException
{
    protected $message = "Only Admin User can perform this action";
}