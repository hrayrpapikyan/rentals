<?php

namespace Rentals\V1\Rest\User\Exception;

class UserIsNotAdminOrRealtorException extends \DomainException
{
    protected $message = "Only Admin User and Realtors can perform this action";
}