<?php

namespace Rentals\V1\Rest\User\Exception;

class UserNotFoundException extends \DomainException
{
    protected $message = "User not found";
}