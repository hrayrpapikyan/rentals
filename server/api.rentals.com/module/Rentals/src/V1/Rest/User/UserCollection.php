<?php

namespace Rentals\V1\Rest\User;

use Zend\Paginator\Paginator;

class UserCollection extends Paginator
{
    protected static $defaultItemCountPerPage = 3;

}
