<?php

namespace Rentals\V1\Rest\User;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class UserEntity implements \JsonSerializable
{
    const TYPE_ADMIN = 'admin';
    const TYPE_REALTOR = 'realtor';
    const TYPE_CLIENT = 'client';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id")
     */
    protected $id;

    /**
     * @ORM\Column(name="first_name")
     */
    protected $first_name;

    /**
     * @ORM\Column(name="last_name")
     */
    protected $last_name;

    /**
     * @ORM\Column(name="email")
     */
    protected $email;

    /**
     * @ORM\Column(name="password")
     */
    protected $password;


    /**
     * @ORM\Column(name="type")
     */
    protected $type;

    public function getEmail()
    {
        return $this->email;
    }

    public function isAdmin()
    {
        return $this->type == self::TYPE_ADMIN;
    }

    public function isRealtor()
    {
        return $this->type == self::TYPE_REALTOR;
    }

    public function isClient()
    {
        return $this->type == self::TYPE_CLIENT;
    }


    public function getId()
    {
        return $this->id;
    }


    public function getFirstName()
    {
        return $this->first_name;
    }

    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    public function getLastName()
    {
        return $this->last_name;
    }

    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }


    public function getPassword()
    {
        return $this->password;
    }


    public function getType()
    {
        return $this->type;
    }


    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }


    public function jsonSerialize()
    {
        $that = get_object_vars($this);
        unset($that['password']);
        return $that;
    }
}
