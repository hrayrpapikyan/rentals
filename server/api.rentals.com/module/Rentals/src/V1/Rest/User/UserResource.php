<?php
namespace Rentals\V1\Rest\User;

use Application\BaseResource;
use Application\CompleteRequest\CompleteRequestService;
use Application\Constant\Error;
use Application\Exception\DuplicateRequestException;
use Rentals\V1\Rest\User\Exception\AdminUserCanNotBeDeletedException;
use Rentals\V1\Rest\User\Exception\UserCanChangeOnlyHisPasswordException;
use Rentals\V1\Rest\User\Exception\UserEmailBusyException;
use Rentals\V1\Rest\User\Exception\UserIsNotAdminException;
use Zend\Log\LoggerInterface;
use Rentals\V1\Rest\User\Exception\UserNotFoundException;
use ZF\ApiProblem\ApiProblem;

class UserResource extends BaseResource
{
    const CURRENT_USER_PARAM = 'me';


    /**
     * UserResource constructor.
     * @param UserService $userService
     * @param CompleteRequestService $completeRequestService
     * @param LoggerInterface $logger
     */
    public function __construct(
        UserService $userService,
        CompleteRequestService $completeRequestService,
        LoggerInterface $logger
    ) {
        parent::__construct($userService, $completeRequestService, $logger);
    }

    /**
     * @api {post} /users Create User
     * @apiName CreateUser
     * @apiGroup User
     * @apiVersion 1.0.0
     *
     * @apiParam {String} uuid Request unique ID.
     * @apiParam {String} email User email.
     * @apiParam {String} first_name User first name.
     * @apiParam {String} last_name User last name.
     * @apiParam {String} password User password.
     * @apiParam {String} password_confirm User password confirmation.
     * @apiParam {String} type May be "client" or "realtor".
     *
     * @apiHeaderExample {json} Header
     *     {
     *       "Accept": "application/json",
     *       "Content-Type": "application/json",
     *     }
     *
     * @apiSuccessExample {json} Success-Response
     *     HTTP/1.1 201 Created
     *     {
     *      "id": 1,
     *      "first_name": "John",
     *      "last_name": "Doe",
     *      "email": "john.doe@gmail.com",
     *      "type": "client"
     *     }
     *
     * @apiErrorExample {json} Error-Response Duplicate Request
     *      HTTP/1.1 409 Conflict
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Conflict",
     *       "status": 409,
     *       "detail": "Duplicate request"
     *      }
     *
     * @apiErrorExample {json} Error-Response User Email Busy
     *      HTTP/1.1 400 Bad Request
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Bad Request",
     *       "status": 400,
     *       "detail": "Email is already used"
     *      }
     *
     * @apiErrorExample {json} Error-Response Validation
     *      HTTP/1.1 422 Unprocessable Entity
     *      {
     *           "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *           "title": "Bad Request",
     *           "status": 422,
     *           "detail": "Failed Validation",
     *           "validation_messages": {
     *               "uuid": {
     *               "isEmpty": "Value is required and can't be empty"
     *               },
     *               "first_name": {
     *               "isEmpty": "Value is required and can't be empty"
     *               },
     *               "last_name": {
     *               "isEmpty": "Value is required and can't be empty"
     *               },
     *               "email": {
     *               "emailAddressInvalidFormat": "The input is not a valid email address. Use the basic format local-part@hostname"
     *               },
     *               "password": {
     *               "stringLengthTooShort": "The input is less than 8 characters long"
     *               },
     *               "type": {
     *               "isEmpty": "Value is required and can't be empty"
     *               },
     *               "password_confirm": {
     *               "notSame": "Password confirmation is wrong"
     *               }
     *           },
     *      }
     *
     * @apiErrorExample {json} Error-Response Unknown Error
     *      HTTP/1.1 500 Internal Server Error
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Internal Server Error",
     *       "status": 500,
     *       "detail": "Server side problem"
     *      }
     */

    public function create($data)
    {
        try {
            $requestUuid = $data->uuid;
            $this->getCompleteRequestService()->checkRequest($requestUuid);
            $userEntity = $this->getUserService()->createUser((array) $data);

            $this->getCompleteRequestService()->complete($requestUuid);

            return $userEntity;
        } catch (DuplicateRequestException $e) {
            return new ApiProblem(409, $e->getMessage());
        } catch (UserEmailBusyException $e) {
            return new ApiProblem(400, $e->getMessage());
        } catch (\Throwable $e) {
            $this->getLogger()->err($e->getMessage());
            return new ApiProblem(500, Error::SERVER_PROBLEM);
        }
    }

    /**
     * @api {delete} /users/:id Delete User
     * @apiName DeleteUser
     * @apiGroup User
     * @apiVersion 1.0.0
     *
     * @apiHeaderExample {json} Header
     *     {
     *       "Authorization": "Bearer 611b2f9900a34863b869b43fa33b6e6444f5dd2a",
     *       "Content-Type": "application/json",
     *     }
     *
     * @apiSuccessExample {json} Success-Response
     *     HTTP/1.1 204 Deleted
     *
     * @apiErrorExample {json} Error-Response User Not Found
     *      HTTP/1.1 404 Not Found
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Not Found",
     *       "status": 404,
     *       "detail": "User not found"
     *      }
     *
     * @apiErrorExample {json} Error-Response Admin User can not be deleted
     *      HTTP/1.1 403 Forbidden
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Forbidden",
     *       "status": 403,
     *       "detail": "Admin User can not be deleted"
     *      }
     *
     * @apiErrorExample {json} Error-Response User Is Not Admin
     *      HTTP/1.1 403 Forbidden
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Forbidden",
     *       "status": 403,
     *       "detail": "Only Admin User can perform this action"
     *      }
     *
     * @apiErrorExample {json} Error-Response Unknown Error
     *      HTTP/1.1 500 Internal Server Error
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Internal Server Error",
     *       "status": 500,
     *       "detail": "Server side problem"
     *      }
     *
     */
    public function delete($id)
    {
        try {
            $this->checkIsAdmin();
            $user = $this->getUserService()->getUser($id);
            if ($user->getType() == UserEntity::TYPE_ADMIN) {
                throw new AdminUserCanNotBeDeletedException();
            }
            $this->getUserService()->deleteUser($user);
            return true;
        } catch (UserNotFoundException $e) {
            return new ApiProblem(404, $e->getMessage());
        } catch (UserIsNotAdminException | AdminUserCanNotBeDeletedException $e) {
            return new ApiProblem(403, $e->getMessage());
        } catch (\Throwable $e) {
            $this->getLogger()->err($e->getMessage());
            return new ApiProblem(500, Error::SERVER_PROBLEM);
        }
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * @api {get} /users/:id Get User information
     * @apiName GetUser
     * @apiGroup User
     * @apiVersion 1.0.0
     *
     * @apiParam {Integer={1,2,3...,"me"}} id Users unique ID, "me" to get current user.
     *
     * @apiHeaderExample {json} Header
     *     {
     *       "Authorization": "Bearer 611b2f9900a34863b869b43fa33b6e6444f5dd2a",
     *       "Content-Type": "application/json",
     *     }
     *
     * @apiSuccessExample {json} Success-Response
     *     HTTP/1.1 200 Ok
     *     {
     *      "id": 1,
     *      "first_name": "John",
     *      "last_name": "Doe",
     *      "email": "john.doe@gmail.com",
     *      "type": "client"
     *
     *     }
     *
     * @apiErrorExample {json} Error-Response User Not Found
     *      HTTP/1.1 404 Not Found
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Not Found",
     *       "status": 404,
     *       "detail": "User not found"
     *      }
     *
     * @apiErrorExample {json} Error-Response User Is Not Admin
     *      HTTP/1.1 403 Forbidden
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Forbidden",
     *       "status": 403,
     *       "detail": "Only Admin User can perform this action"
     *      }
     * @apiErrorExample {json} Error-Response Validation
     *      HTTP/1.1 422 Unprocessable Entity
     *      {
     *           "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *           "title": "Bad Request",
     *           "status": 422,
     *           "detail": "Failed Validation",
     *           "validation_messages": {
     *               "first_name": {
     *               "isEmpty": "Value is required and can't be empty"
     *               },
     *               "last_name": {
     *               "isEmpty": "Value is required and can't be empty"
     *               }
     *           },
     *      }
     *
     *
     * @apiErrorExample {json} Error-Response Unknown Error
     *      HTTP/1.1 500 Internal Server Error
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Internal Server Error",
     *       "status": 500,
     *       "detail": "Server side problem"
     *      }
     *
     */
    public function fetch($id)
    {
        try {
            if ($id == self::CURRENT_USER_PARAM) {
                $id = $this->getLoggedInUserId();
            } elseif ($this->getLoggedInUserId() != $id) {
                $this->checkIsAdmin();
            }
            return $this->getUserService()->getUser($id);
        } catch (UserNotFoundException $e) {
            return new ApiProblem(404, $e->getMessage());
        } catch (UserIsNotAdminException $e) {
            return new ApiProblem(403, $e->getMessage());
        } catch (\Throwable $e) {
            $this->getLogger()->err($e->getMessage());
            return new ApiProblem(500, Error::SERVER_PROBLEM);
        }
    }

    /**
     * @api {get} /users Get Paginated Users Or All Realtors
     * @apiName GetUsers
     * @apiGroup User
     * @apiVersion 1.0.0
     *
     * @apiParam {Integer} [page] Current page number
     * @apiParam {Integer={0, 1}} [only_realtors] bit for fetching only realtors
     *
     *
     * @apiHeaderExample {json} Header
     *     {
     *       "Authorization": "Bearer 611b2f9900a34863b869b43fa33b6e6444f5dd2a",
     *       "Content-Type": "application/json",
     *     }
     *
     * @apiSuccessExample {json} Success-Response Paginated Result
     *     HTTP/1.1 200 Ok
     *   {
     *       "users": [
     *           {
     *           "id": "1",
     *           "first_name": "John",
     *           "last_name": "Doe",
     *           "email": "admin@rentals.com",
     *           "type": "admin"
     *           },
     *           {
     *           "id": "2",
     *           "first_name": "John",
     *           "last_name": "Doe",
     *           "email": "john.doe@gmail.com",
     *           "type": "realtor"
     *          }
     *       ],
     *       "total": 2,
     *       "per_page": 10,
     *       "page": 1,
     *       "page_count": 1
     *  }
     *
     * @apiSuccessExample {json} Success-Response Only realtors not paginated
     *     HTTP/1.1 200 Ok
     *     [
     *           {
     *           "id": "5",
     *           "first_name": "John",
     *           "last_name": "Doe",
     *           "email": "john.doe@gmail.com",
     *           "type": "realtor"
     *           }
     *      ]
     *
     * @apiErrorExample {json} Error-Response User Is Not Admin
     *      HTTP/1.1 403 Forbidden
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Forbidden",
     *       "status": 403,
     *       "detail": "Only Admin User can perform this action"
     *      }
     *
     * @apiErrorExample {json} Error-Response Unknown Error
     *      HTTP/1.1 500 Internal Server Error
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Internal Server Error",
     *       "status": 500,
     *       "detail": "Server side problem"
     *      }
     *
     */
    public function fetchAll($params = [])
    {
        try {
            $this->checkIsAdmin();
            if (empty($params['only_realtors'])) {
                return $this->getUserService()->getUsersPaginated((array) $params);
            } else {
                return $this->getUserService()->getRealtors();
            }
        } catch (UserIsNotAdminException $e) {
            return new ApiProblem(403, $e->getMessage());
        } catch (\Throwable $e) {
            $this->getLogger()->err($e->getMessage());
            return new ApiProblem(500, Error::SERVER_PROBLEM);
        }

    }

    /**
     * @api {patch} /users/:id Update User
     * @apiName UpdateUser
     * @apiGroup User
     * @apiVersion 1.0.0
     *
     * @apiParam {Integer={1,2,3...,"me"}} id Users unique ID, "me" to get current user.
     * @apiParam {String} first_name User first name.
     * @apiParam {String} last_name User last name.
     * @apiParam {String} [current_password] User current password.
     * @apiParam {String} [password] User new password.
     * @apiParam {String} [password_confirm] User new password confirmation.
     *
     *
     * @apiHeaderExample {json} Header-Example
     *     {
     *       "Authorization": "Bearer 611b2f9900a34863b869b43fa33b6e6444f5dd2a",
     *       "Content-Type": "application/json",
     *     }
     *
     * @apiSuccessExample {json} Success-Response
     *     HTTP/1.1 20 OK
     *     {
     *      "id": 1,
     *      "first_name": "John",
     *      "last_name": "Doe",
     *      "email": "john.doe@gmail.com",
     *      "type": "client"
     *     }
     *
     * @apiErrorExample {json} Error-Response User Not Found
     *      HTTP/1.1 404 Not Found
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Not Found",
     *       "status": 404,
     *       "detail": "User not found"
     *      }
     *
     * @apiErrorExample {json} Error-Response User Is Not Admin
     *      HTTP/1.1 403 Forbidden
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Forbidden",
     *       "status": 403,
     *       "detail": "Only Admin User can perform this action"
     *      }
     *
     * @apiErrorExample {json} Error-Response User can not change other user's password
     *      HTTP/1.1 403 Forbidden
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Forbidden",
     *       "status": 403,
     *       "detail": "User may change only his/her password"
     *      }
     *
     * @apiErrorExample {json} Error-Response Current password is wrong
     *      HTTP/1.1 400 Bad Request
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Bad Request",
     *       "status": 400,
     *       "detail": "Current password is wrong"
     *      }
     *
     *
     *
     * @apiErrorExample {json} Error-Response Unknown Error
     *      HTTP/1.1 500 Internal Server Error
     *      {
     *       "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
     *       "title": "Internal Server Error",
     *       "status": 500,
     *       "detail": "Server side problem"
     *      }
     *
     *
     */
    public function patch($id, $data)
    {
        try {

            if ($id == self::CURRENT_USER_PARAM) {
                $id = $this->getLoggedInUserId();
            } elseif ($this->getLoggedInUserId() != $id) {
                $this->checkIsAdmin();
            }

            $userEntity = $this->getUserService()->getUser($id);
            $this->getUserService()->updateUser($userEntity, $this->getLoggedInUser(), (array) $data);
            return $userEntity;
        } catch (UserNotFoundException $e) {
            return new ApiProblem(404, $e->getMessage());
        } catch (\InvalidArgumentException $e) {
            return new ApiProblem(400, $e->getMessage());
        } catch (UserIsNotAdminException | UserCanChangeOnlyHisPasswordException $e) {
            return new ApiProblem(403, $e->getMessage());
        } catch (\Throwable $e) {
            $this->getLogger()->err($e->getMessage());
            return new ApiProblem(500, Error::SERVER_PROBLEM);
        }
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }



}
