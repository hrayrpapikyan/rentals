<?php
namespace Rentals\V1\Rest\User;

use Application\CompleteRequest\CompleteRequestService;

class UserResourceFactory
{
    public function __invoke($services): UserResource
    {
        return new UserResource(
            $services->get(UserService::class),
            $services->get(CompleteRequestService::class),
            $services->get('logger')
        );
    }
}
