<?php

namespace Rentals\V1\Rest\User;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator;
use Rentals\V1\Rest\User\Exception\UserCanChangeOnlyHisPasswordException;
use Rentals\V1\Rest\User\Exception\UserEmailBusyException;
use Rentals\V1\Rest\User\Exception\UserNotFoundException;
use Zend\Hydrator\Reflection;
use Zend\Crypt\Password\Bcrypt;
use Doctrine\ORM\Tools\Pagination\Paginator as OrmPaginator;


class UserService
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    public function createUser(array $data) : UserEntity
    {
        $userEntity = $this->hydrateUserEntity($data);
        $existingUser = $this->getEntityManager()->getRepository(UserEntity::class)->findOneBy(
            ['email'=> $userEntity->getEmail()]
        );

        if ($existingUser) {
            throw new UserEmailBusyException();
        }

        $this->getEntityManager()->persist($userEntity);
        $this->getEntityManager()->flush();

        return $userEntity;
    }

    public function getUser(int $id) : UserEntity
    {
        /**
         * @var UserEntity $userEntity
         */
        $userEntity = $this->getEntityManager()->getRepository(UserEntity::class)->findOneBy(
            ['id'=> $id]
        );

        if (!$userEntity) {
            throw new UserNotFoundException();
        }

        return $userEntity;
    }

    public function getUsersPaginated(array $params): array
    {
        $currentPageNumber = $params['page'] ?? 1;

        $collection = $this->getUserPaginatedCollection();
        $collection->setCurrentPageNumber($currentPageNumber);
        $result = [];
        $result['users'] = $collection->getCurrentItems();
        return array_merge($result, [
            'total' => $collection->getTotalItemCount(),
            "per_page" => $collection->getItemCountPerPage(),
            "page" => $collection->getCurrentPageNumber(),
            "page_count" => $collection->count()
        ]);

    }

    public function getRealtors()
    {
        return $this->getEntityManager()->getRepository(UserEntity::class)->findBy([
            'type' => UserEntity::TYPE_REALTOR
        ]);
    }

    public function deleteUser(UserEntity $userEntity)
    {
        $this->getEntityManager()->remove($userEntity);
        $this->getEntityManager()->flush();
    }

    public function updateUser(UserEntity $userEntity, UserEntity $loggedInUser, array $data): void
    {
        $userEntity->setFirstName($data['first_name']);
        $userEntity->setLastName($data['last_name']);

        if (isset($data['current_password'])) {
            if ($userEntity->getId() != $loggedInUser->getId()) {
                throw new UserCanChangeOnlyHisPasswordException();
            }
            $bcrypt = $this->getBcryptObject();
            if (!$bcrypt->verify($data['current_password'], $userEntity->getPassword())) {
                throw new \InvalidArgumentException('Current password is wrong');
            }

            $userEntity->setPassword($bcrypt->create($data['password']));
        }

        $this->getEntityManager()->persist($userEntity);
        $this->getEntityManager()->flush();
    }

    protected function hydrateUserEntity(array $data) : UserEntity
    {
        $hydrator = new Reflection();
        $userEntity = new UserEntity();
        $bcrypt = $this->getBcryptObject();

        if (isset($data['password'])) {
            $data['password'] = $bcrypt->create($data['password']);
        }

        $hydrator->hydrate($data, $userEntity);
        return $userEntity;
    }

    protected function getUserPaginatedCollection()
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('u')->from(UserEntity::class, 'u');
        $query = $queryBuilder->getQuery();
        $ormPaginator = new OrmPaginator($query);
        $doctrinePaginator = new DoctrinePaginator($ormPaginator);
        $collection = new UserCollection($doctrinePaginator);
        return $collection;
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    protected function getBcryptObject(): Bcrypt
    {
        return new Bcrypt();
    }

}