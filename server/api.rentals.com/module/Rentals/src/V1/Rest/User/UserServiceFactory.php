<?php

namespace Rentals\V1\Rest\User;


use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class UserServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): UserService
    {
        return new UserService(
            $container->get('doctrine.entitymanager.orm_default')
        );
    }
}