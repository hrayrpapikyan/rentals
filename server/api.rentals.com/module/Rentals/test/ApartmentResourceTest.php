<?php

namespace RentalsTest;

use Application\Constant\Error;
use Application\Exception\DuplicateRequestException;
use ApplicationTest\ResourceBaseTest;
use PHPUnit\Runner\Exception;
use Rentals\V1\Rest\Apartment\ApartmentEntity;
use Rentals\V1\Rest\Apartment\ApartmentResource;
use Rentals\V1\Rest\Apartment\ApartmentService;
use Rentals\V1\Rest\Apartment\Exception\ApartmentNotFoundException;
use Rentals\V1\Rest\Apartment\Exception\RealtorCanNotCRUDOtherRealtorsApartmentsException;
use Rentals\V1\Rest\User\Exception\UserIsNotAdminException;
use Rentals\V1\Rest\User\Exception\UserIsNotAdminOrRealtorException;
use Rentals\V1\Rest\User\Exception\UserNotFoundException;
use Rentals\V1\Rest\User\UserEntity;
use ZF\ApiProblem\ApiProblem;


class ApartmentResourceTest extends ResourceBaseTest
{


    public function testCreateSuccess()
    {

        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'getCompleteRequestService',
                'getApartmentService',
                'getLoggedInUser',
                'checkIsNotClient',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'createApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $competeRequestServiceStub = $this->getCompleteRequestServiceStub();
        $competeRequestServiceStub->method('checkRequest')->willReturn(null);

        $apartmentResourceMock->method('checkIsNotClient')->willReturn(null);
        $apartmentResourceMock->method('getCompleteRequestService')->willReturn($competeRequestServiceStub);
        $apartmentResourceMock->method('getApartmentService')->willReturn($apartmentServiceStub);
        $apartmentResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());

        $apartmentServiceStub->method('createApartment')->willReturn(new ApartmentEntity());

        $data = new \stdClass();
        $data->uuid = '624d5b7a-3e98-4344-bbb1-e7bebcbff66a';

        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');
        $apartmentResourceMock->expects($this->once())->method('getApartmentService');
        $apartmentResourceMock->expects($this->once())->method('getLoggedInUser');
        $apartmentResourceMock->expects($this->exactly(2))->method('getCompleteRequestService');
        $competeRequestServiceStub->expects($this->once())->method('checkRequest');
        $competeRequestServiceStub->expects($this->once())->method('complete');
        $apartmentServiceStub->expects($this->once())->method('createApartment');

        $result = $apartmentResourceMock->create($data);
        $this->assertInstanceOf(ApartmentEntity::class, $result);
    }

    public function testCreateThrowsDuplicateRequestException()
    {

        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'getCompleteRequestService',
                'getApartmentService',
                'getLoggedInUser',
                'checkIsNotClient',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'createApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentResourceMock->method('checkIsNotClient')->willReturn(null);
        $competeRequestServiceStub = $this->getCompleteRequestServiceStub();
        $apartmentResourceMock->method('getCompleteRequestService')->willReturn($competeRequestServiceStub);

        $exception = new DuplicateRequestException();
        $competeRequestServiceStub->method('checkRequest')->willThrowException($exception);


        $data = new \stdClass();
        $data->uuid = '624d5b7a-3e98-4344-bbb1-e7bebcbff66a';

        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');
        $apartmentResourceMock->expects($this->never())->method('getApartmentService');
        $apartmentResourceMock->expects($this->never())->method('getLoggedInUser');
        $apartmentResourceMock->expects($this->once())->method('getCompleteRequestService');
        $competeRequestServiceStub->expects($this->once())->method('checkRequest');
        $competeRequestServiceStub->expects($this->never())->method('complete');
        $apartmentServiceStub->expects($this->never())->method('createApartment');

        $result = $apartmentResourceMock->create($data);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(409, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }

    public function testCreateThrowsInvalidArgumentException()
    {

        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'getCompleteRequestService',
                'getApartmentService',
                'getLoggedInUser',
                'checkIsNotClient',

            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'createApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $competeRequestServiceStub = $this->getCompleteRequestServiceStub();
        $competeRequestServiceStub->method('checkRequest')->willReturn(null);

        $apartmentResourceMock->method('checkIsNotClient')->willReturn(null);

        $apartmentResourceMock->method('getCompleteRequestService')->willReturn($competeRequestServiceStub);
        $apartmentResourceMock->method('getApartmentService')->willReturn($apartmentServiceStub);
        $apartmentResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());

        $exception = new \InvalidArgumentException('message');
        $apartmentServiceStub->method('createApartment')->willThrowException($exception);

        $data = new \stdClass();
        $data->uuid = '624d5b7a-3e98-4344-bbb1-e7bebcbff66a';

        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');
        $apartmentResourceMock->expects($this->once())->method('getApartmentService');
        $apartmentResourceMock->expects($this->once())->method('getLoggedInUser');
        $apartmentResourceMock->expects($this->once())->method('getCompleteRequestService');
        $competeRequestServiceStub->expects($this->once())->method('checkRequest');
        $competeRequestServiceStub->expects($this->never())->method('complete');
        $apartmentServiceStub->expects($this->once())->method('createApartment');

        $result = $apartmentResourceMock->create($data);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(400, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }

    public function testCreateThrowsUserNotFoundException()
    {

        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'getCompleteRequestService',
                'getApartmentService',
                'getLoggedInUser',
                'checkIsNotClient',

            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'createApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $competeRequestServiceStub = $this->getCompleteRequestServiceStub();
        $competeRequestServiceStub->method('checkRequest')->willReturn(null);

        $apartmentResourceMock->method('checkIsNotClient')->willReturn(null);
        $apartmentResourceMock->method('getCompleteRequestService')->willReturn($competeRequestServiceStub);
        $apartmentResourceMock->method('getApartmentService')->willReturn($apartmentServiceStub);
        $apartmentResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());

        $exception = new UserNotFoundException();
        $apartmentServiceStub->method('createApartment')->willThrowException($exception);

        $data = new \stdClass();
        $data->uuid = '624d5b7a-3e98-4344-bbb1-e7bebcbff66a';


        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');
        $apartmentResourceMock->expects($this->once())->method('getApartmentService');
        $apartmentResourceMock->expects($this->once())->method('getLoggedInUser');
        $apartmentResourceMock->expects($this->once())->method('getCompleteRequestService');
        $competeRequestServiceStub->expects($this->once())->method('checkRequest');
        $competeRequestServiceStub->expects($this->never())->method('complete');
        $apartmentServiceStub->expects($this->once())->method('createApartment');

        $result = $apartmentResourceMock->create($data);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(400, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }

    public function testCreateThrowsUserIsNotAdminOrRealtorException()
    {

        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'getCompleteRequestService',
                'getApartmentService',
                'getLoggedInUser',
                'checkIsNotClient',

            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'createApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();


        $exception = new UserIsNotAdminOrRealtorException();
        $apartmentResourceMock->method('checkIsNotClient')->willThrowException($exception);

        $data = new \stdClass();
        $data->uuid = '624d5b7a-3e98-4344-bbb1-e7bebcbff66a';


        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');
        $apartmentResourceMock->expects($this->never())->method('getApartmentService');
        $apartmentResourceMock->expects($this->never())->method('getLoggedInUser');
        $apartmentResourceMock->expects($this->never())->method('getCompleteRequestService');
        $apartmentServiceStub->expects($this->never())->method('createApartment');

        $result = $apartmentResourceMock->create($data);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(403, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }

    public function testCreateThrowsUnknownException()
    {

        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'getCompleteRequestService',
                'getApartmentService',
                'getLoggedInUser',
                'getLogger',
                'checkIsNotClient',

            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'createApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $competeRequestServiceStub = $this->getCompleteRequestServiceStub();
        $competeRequestServiceStub->method('checkRequest')->willReturn(null);

        $loggerStub = $this->getLoggerStub();
        $apartmentResourceMock->method('getLogger')->willReturn($loggerStub);

        $apartmentResourceMock->method('checkIsNotClient')->willReturn(null);
        $apartmentResourceMock->method('getCompleteRequestService')->willReturn($competeRequestServiceStub);
        $apartmentResourceMock->method('getApartmentService')->willReturn($apartmentServiceStub);
        $apartmentResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());

        $exception = new Exception();
        $apartmentServiceStub->method('createApartment')->willThrowException($exception);

        $data = new \stdClass();
        $data->uuid = '624d5b7a-3e98-4344-bbb1-e7bebcbff66a';

        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');
        $apartmentResourceMock->expects($this->once())->method('getApartmentService');
        $apartmentResourceMock->expects($this->once())->method('getLoggedInUser');
        $apartmentResourceMock->expects($this->once())->method('getCompleteRequestService');
        $competeRequestServiceStub->expects($this->once())->method('checkRequest');
        $competeRequestServiceStub->expects($this->never())->method('complete');
        $apartmentServiceStub->expects($this->once())->method('createApartment');
        $loggerStub->expects($this->once())->method('err');

        $result = $apartmentResourceMock->create($data);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(500, $result->toArray()['status']);
        $this->assertEquals(Error::SERVER_PROBLEM, $result->toArray()['detail']);
    }


    public function testDeleteSuccess()
    {
        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'checkIsNotClient',
                'getApartmentService',
                'getLoggedInUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'deleteApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentResourceMock->method('checkIsNotClient')->willReturn(null);
        $apartmentResourceMock->method('getApartmentService')->willReturn($apartmentServiceStub);
        $apartmentResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());

        $apartmentServiceStub->method('deleteApartment')->willReturn(null);

        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');
        $apartmentResourceMock->expects($this->once())->method('getApartmentService');
        $apartmentResourceMock->expects($this->once())->method('getLoggedInUser');
        $apartmentServiceStub->expects($this->once())->method('deleteApartment');

        $result = $apartmentResourceMock->delete(1);
        $this->assertTrue($result);

    }

    public function testDeleteThrowsUserIsNotAdminOrRealtorException()
    {
        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'checkIsNotClient',
                'getApartmentService',
                'getLoggedInUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'deleteApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $exception = new UserIsNotAdminOrRealtorException();
        $apartmentResourceMock->method('checkIsNotClient')->willThrowException($exception);

        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');
        $apartmentResourceMock->expects($this->never())->method('getApartmentService');
        $apartmentResourceMock->expects($this->never())->method('getLoggedInUser');
        $apartmentServiceStub->expects($this->never())->method('deleteApartment');

        $result = $apartmentResourceMock->delete(1);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(403, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }

    public function testDeleteThrowsRealtorCanNotCRUDOtherRealtorsApartmentsException()
    {
        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'checkIsNotClient',
                'getApartmentService',
                'getLoggedInUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'deleteApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentResourceMock->method('checkIsNotClient')->willReturn(null);
        $apartmentResourceMock->method('getApartmentService')->willReturn($apartmentServiceStub);
        $apartmentResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());

        $exception = new RealtorCanNotCRUDOtherRealtorsApartmentsException();
        $apartmentServiceStub->method('deleteApartment')->willThrowException($exception);

        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');
        $apartmentResourceMock->expects($this->once())->method('getApartmentService');
        $apartmentResourceMock->expects($this->once())->method('getLoggedInUser');
        $apartmentServiceStub->expects($this->once())->method('deleteApartment');

        $result = $apartmentResourceMock->delete(1);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(403, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }

    public function testDeleteThrowsApartmentNotFoundException()
    {
        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'checkIsNotClient',
                'getApartmentService',
                'getLoggedInUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'deleteApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentResourceMock->method('checkIsNotClient')->willReturn(null);
        $apartmentResourceMock->method('getApartmentService')->willReturn($apartmentServiceStub);
        $apartmentResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());

        $exception = new ApartmentNotFoundException();
        $apartmentServiceStub->method('deleteApartment')->willThrowException($exception);

        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');
        $apartmentResourceMock->expects($this->once())->method('getApartmentService');
        $apartmentResourceMock->expects($this->once())->method('getLoggedInUser');
        $apartmentServiceStub->expects($this->once())->method('deleteApartment');

        $result = $apartmentResourceMock->delete(1);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(404, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);

    }

    public function testDeleteThrowsUnknownException()
    {
        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'checkIsNotClient',
                'getApartmentService',
                'getLoggedInUser',
                'getLogger',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'deleteApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentResourceMock->method('checkIsNotClient')->willReturn(null);
        $apartmentResourceMock->method('getApartmentService')->willReturn($apartmentServiceStub);
        $apartmentResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());

        $loggerStub = $this->getLoggerStub();
        $apartmentResourceMock->method('getLogger')->willReturn($loggerStub);

        $exception = new \Exception();
        $apartmentServiceStub->method('deleteApartment')->willThrowException($exception);

        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');
        $apartmentResourceMock->expects($this->once())->method('getApartmentService');
        $apartmentResourceMock->expects($this->once())->method('getLoggedInUser');
        $apartmentServiceStub->expects($this->once())->method('deleteApartment');
        $loggerStub->expects($this->once())->method('err');

        $result = $apartmentResourceMock->delete(1);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(500, $result->toArray()['status']);
        $this->assertEquals(Error::SERVER_PROBLEM, $result->toArray()['detail']);

    }

    public function testFetchSuccess()
    {
        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'checkIsNotClient',
                'getApartmentService',
                'getLoggedInUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'getApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentResourceMock->method('checkIsNotClient')->willReturn(null);
        $apartmentResourceMock->method('getApartmentService')->willReturn($apartmentServiceStub);
        $apartmentResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());
        $apartmentServiceStub->method('getApartment')->willReturn(new ApartmentEntity());

        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');

        $apartmentResourceMock->expects($this->once())->method('getApartmentService');
        $apartmentResourceMock->expects($this->once())->method('getLoggedInUser');
        $apartmentServiceStub->expects($this->once())->method('getApartment');

        $result = $apartmentResourceMock->fetch(10);
        $this->assertInstanceOf(ApartmentEntity::class, $result);
    }

    public function testFetchThrowsUserIsNotAdminOrRealtorException()
    {
        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'checkIsNotClient',
                'getApartmentService',
                'getLoggedInUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'getApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $exception = new UserIsNotAdminOrRealtorException();
        $apartmentResourceMock->method('checkIsNotClient')->willThrowException($exception);


        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');
        $apartmentResourceMock->expects($this->never())->method('getApartmentService');
        $apartmentResourceMock->expects($this->never())->method('getLoggedInUser');
        $apartmentServiceStub->expects($this->never())->method('getApartment');

        $result = $apartmentResourceMock->fetch(10);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(403, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }

    public function testFetchThrowsApartmentNotFoundException()
    {
        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'checkIsNotClient',
                'getApartmentService',
                'getLoggedInUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'getApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentResourceMock->method('checkIsNotClient')->willReturn(null);
        $apartmentResourceMock->method('getApartmentService')->willReturn($apartmentServiceStub);
        $apartmentResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());

        $exception = new ApartmentNotFoundException();
        $apartmentServiceStub->method('getApartment')->willThrowException($exception);

        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');

        $apartmentResourceMock->expects($this->once())->method('getApartmentService');
        $apartmentResourceMock->expects($this->once())->method('getLoggedInUser');
        $apartmentServiceStub->expects($this->once())->method('getApartment');

        $result = $apartmentResourceMock->fetch(10);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(404, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }

    public function testFetchThrowsRealtorCanNotCRUDOtherRealtorsApartmentsException()
    {
        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'checkIsNotClient',
                'getApartmentService',
                'getLoggedInUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'getApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentResourceMock->method('checkIsNotClient')->willReturn(null);
        $apartmentResourceMock->method('getApartmentService')->willReturn($apartmentServiceStub);
        $apartmentResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());

        $exception = new RealtorCanNotCRUDOtherRealtorsApartmentsException();
        $apartmentServiceStub->method('getApartment')->willThrowException($exception);

        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');

        $apartmentResourceMock->expects($this->once())->method('getApartmentService');
        $apartmentResourceMock->expects($this->once())->method('getLoggedInUser');
        $apartmentServiceStub->expects($this->once())->method('getApartment');

        $result = $apartmentResourceMock->fetch(10);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(403, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }

    public function testFetchThrowsUnknownExceptionException()
    {
        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'checkIsNotClient',
                'getApartmentService',
                'getLoggedInUser',
                'getLogger',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'getApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentResourceMock->method('checkIsNotClient')->willReturn(null);
        $apartmentResourceMock->method('getApartmentService')->willReturn($apartmentServiceStub);
        $apartmentResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());
        $loggerStub = $this->getLoggerStub();
        $apartmentResourceMock->method('getLogger')->willReturn($loggerStub);
        $exception = new \Exception();
        $apartmentServiceStub->method('getApartment')->willThrowException($exception);

        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');

        $apartmentResourceMock->expects($this->once())->method('getApartmentService');
        $apartmentResourceMock->expects($this->once())->method('getLoggedInUser');
        $apartmentServiceStub->expects($this->once())->method('getApartment');
        $loggerStub->expects($this->once())->method('err');


        $result = $apartmentResourceMock->fetch(10);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(500, $result->toArray()['status']);
        $this->assertEquals(Error::SERVER_PROBLEM, $result->toArray()['detail']);
    }

    public function testFetchAllSuccess()
    {
        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'getApartmentService',
                'getLoggedInUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'getApartments',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentResourceMock->method('getApartmentService')->willReturn($apartmentServiceStub);
        $apartmentResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());

        $apartmentServiceStub->method('getApartments')->willReturn([
            'apartments' => [[], []],
            'total' => 20,
            "per_page" => 10,
            "page" => 1,
            "page_count" => 2
        ]);

        $apartmentResourceMock->expects($this->once())->method('getApartmentService');
        $apartmentResourceMock->expects($this->once())->method('getLoggedInUser');
        $apartmentServiceStub->expects($this->once())->method('getApartments');

        $result = $apartmentResourceMock->fetchAll(10);
        $this->assertArrayHasKey('apartments', $result);
        $this->assertCount(2, $result['apartments']);
        $this->assertArrayHasKey('total', $result);
        $this->assertArrayHasKey('per_page', $result);
        $this->assertArrayHasKey('page', $result);
        $this->assertArrayHasKey('page_count', $result);
    }

    public function testFetchAllThrowsUnknownException()
    {
        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'getApartmentService',
                'getLoggedInUser',
                'getLogger',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'getApartments',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $loggerStub = $this->getLoggerStub();
        $apartmentResourceMock->method('getLogger')->willReturn($loggerStub);

        $apartmentResourceMock->method('getApartmentService')->willReturn($apartmentServiceStub);
        $apartmentResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());

        $apartmentServiceStub->method('getApartments')->willThrowException(new \Exception());

        $apartmentResourceMock->expects($this->once())->method('getApartmentService');
        $apartmentResourceMock->expects($this->once())->method('getLoggedInUser');
        $apartmentServiceStub->expects($this->once())->method('getApartments');
        $loggerStub->expects($this->once())->method('err');

        $result = $apartmentResourceMock->fetchAll(10);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(500, $result->toArray()['status']);
        $this->assertEquals(Error::SERVER_PROBLEM, $result->toArray()['detail']);
    }

    public function testPatchSuccess()
    {
        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'checkIsNotClient',
                'getApartmentService',
                'getLoggedInUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'updateApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentResourceMock->method('checkIsNotClient')->willReturn(null);
        $apartmentResourceMock->method('getApartmentService')->willReturn($apartmentServiceStub);
        $apartmentResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());
        $apartmentServiceStub->method('updateApartment')->willReturn(new ApartmentEntity());

        $apartmentResourceMock->expects($this->once())->method('getApartmentService');
        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');
        $apartmentResourceMock->expects($this->once())->method('getLoggedInUser');
        $apartmentServiceStub->expects($this->once())->method('updateApartment');

        $result = $apartmentResourceMock->patch(10, new \StdClass());
        $this->assertInstanceOf(ApartmentEntity::class, $result);

    }

    public function testPatchThrowsUserIsNotAdminOrRealtorException()
    {
        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'checkIsNotClient',
                'getApartmentService',
                'getLoggedInUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'updateApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $exception = new UserIsNotAdminOrRealtorException();
        $apartmentResourceMock->method('checkIsNotClient')->willThrowException($exception);

        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');
        $apartmentResourceMock->expects($this->never())->method('getApartmentService');
        $apartmentResourceMock->expects($this->never())->method('getLoggedInUser');
        $apartmentServiceStub->expects($this->never())->method('updateApartment');

        $result = $apartmentResourceMock->patch(10, new \StdClass());
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(403, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);

    }

    public function testPatchThrowsApartmentNotFoundException()
    {
        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'checkIsNotClient',
                'getApartmentService',
                'getLoggedInUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'updateApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentResourceMock->method('checkIsNotClient')->willReturn(null);
        $apartmentResourceMock->method('getApartmentService')->willReturn($apartmentServiceStub);
        $apartmentResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());

        $exception = new ApartmentNotFoundException();
        $apartmentServiceStub->method('updateApartment')->willThrowException($exception);

        $apartmentResourceMock->expects($this->once())->method('getApartmentService');
        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');
        $apartmentResourceMock->expects($this->once())->method('getLoggedInUser');
        $apartmentServiceStub->expects($this->once())->method('updateApartment');

        $result = $apartmentResourceMock->patch(10, new \StdClass());
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(404, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }

    public function testPatchThrowsUserNotFoundException()
    {
        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'checkIsNotClient',
                'getApartmentService',
                'getLoggedInUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'updateApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentResourceMock->method('checkIsNotClient')->willReturn(null);
        $apartmentResourceMock->method('getApartmentService')->willReturn($apartmentServiceStub);
        $apartmentResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());

        $exception = new UserNotFoundException();
        $apartmentServiceStub->method('updateApartment')->willThrowException($exception);

        $apartmentResourceMock->expects($this->once())->method('getApartmentService');
        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');
        $apartmentResourceMock->expects($this->once())->method('getLoggedInUser');
        $apartmentServiceStub->expects($this->once())->method('updateApartment');

        $result = $apartmentResourceMock->patch(10, new \StdClass());
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(404, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }

    public function testPatchThrowsRealtorCanNotCRUDOtherRealtorsApartmentsException()
    {
        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'checkIsNotClient',
                'getApartmentService',
                'getLoggedInUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'updateApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentResourceMock->method('checkIsNotClient')->willReturn(null);
        $apartmentResourceMock->method('getApartmentService')->willReturn($apartmentServiceStub);
        $apartmentResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());

        $exception = new RealtorCanNotCRUDOtherRealtorsApartmentsException();
        $apartmentServiceStub->method('updateApartment')->willThrowException($exception);

        $apartmentResourceMock->expects($this->once())->method('getApartmentService');
        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');
        $apartmentResourceMock->expects($this->once())->method('getLoggedInUser');
        $apartmentServiceStub->expects($this->once())->method('updateApartment');

        $result = $apartmentResourceMock->patch(10, new \StdClass());
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(403, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }

    public function testPatchThrowsUserIsNotAdminException()
    {
        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'checkIsNotClient',
                'getApartmentService',
                'getLoggedInUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'updateApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentResourceMock->method('checkIsNotClient')->willReturn(null);
        $apartmentResourceMock->method('getApartmentService')->willReturn($apartmentServiceStub);
        $apartmentResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());

        $exception = new UserIsNotAdminException();
        $apartmentServiceStub->method('updateApartment')->willThrowException($exception);

        $apartmentResourceMock->expects($this->once())->method('getApartmentService');
        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');
        $apartmentResourceMock->expects($this->once())->method('getLoggedInUser');
        $apartmentServiceStub->expects($this->once())->method('updateApartment');

        $result = $apartmentResourceMock->patch(10, new \StdClass());
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(403, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }

    public function testPatchThrowsUnknownException()
    {
        $apartmentResourceMock = $this
            ->getMockBuilder(ApartmentResource::class)
            ->setMethods([
                'checkIsNotClient',
                'getApartmentService',
                'getLoggedInUser',
                'getLogger',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceStub = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'updateApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentResourceMock->method('checkIsNotClient')->willReturn(null);
        $apartmentResourceMock->method('getApartmentService')->willReturn($apartmentServiceStub);
        $apartmentResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());
        $loggerStub = $this->getLoggerStub();
        $apartmentResourceMock->method('getLogger')->willReturn($loggerStub);

        $exception = new \Exception();
        $apartmentServiceStub->method('updateApartment')->willThrowException($exception);

        $apartmentResourceMock->expects($this->once())->method('getApartmentService');
        $apartmentResourceMock->expects($this->once())->method('checkIsNotClient');
        $apartmentResourceMock->expects($this->once())->method('getLoggedInUser');
        $apartmentServiceStub->expects($this->once())->method('updateApartment');
        $loggerStub->expects($this->once())->method('err');

        $result = $apartmentResourceMock->patch(10, new \StdClass());
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(500, $result->toArray()['status']);
        $this->assertEquals(Error::SERVER_PROBLEM, $result->toArray()['detail']);
    }


}
