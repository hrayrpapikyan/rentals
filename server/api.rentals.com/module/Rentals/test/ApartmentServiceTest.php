<?php

namespace RentalsTest;

use ApplicationTest\ServiceBaseTest;
use Rentals\V1\Rest\Apartment\ApartmentEntity;
use Rentals\V1\Rest\Apartment\ApartmentService;
use Rentals\V1\Rest\Apartment\Exception\ApartmentNotFoundException;
use Rentals\V1\Rest\Apartment\Exception\RealtorCanNotCRUDOtherRealtorsApartmentsException;
use Rentals\V1\Rest\User\Exception\UserIsNotAdminException;
use Rentals\V1\Rest\User\UserService;
use Rentals\V1\Rest\User\UserEntity;


class ApartmentServiceTest extends ServiceBaseTest
{
    public function testCreateApartmentSuccessLoggedInUserIsRealtor()
    {
        $apartmentServiceMock = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'getEntityManager',
                'hydrateApartmentEntity',
                'getUserService'
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceMock->method('hydrateApartmentEntity')->willReturn(new ApartmentEntity());
        $apartmentServiceMock->method('getUserService')->willReturn($userServiceStub);

        $entityManagerStub = $this->getEntityManagerStub();
        $apartmentServiceMock->method('getEntityManager')->willReturn($entityManagerStub);


        $entityManagerStub->expects($this->once())->method('persist');
        $entityManagerStub->expects($this->once())->method('flush');
        $apartmentServiceMock->expects($this->never())->method('getUserService');
        $apartmentServiceMock->expects($this->once())->method('hydrateApartmentEntity');

        $loggedInUser = new UserEntity();
        $loggedInUser->setType(UserEntity::TYPE_REALTOR);
        $loggedInUser->setId(18);

        /**
         * @var ApartmentEntity $result
         */
        $result = $apartmentServiceMock->createApartment([], $loggedInUser);
        $this->assertInstanceOf(ApartmentEntity::class, $result);
        $this->assertEquals(18, $result->getRealtor()->getId());
    }


    public function testCreateApartmentSuccessLoggedInUserIsAdmin()
    {
        $apartmentServiceMock = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'getEntityManager',
                'hydrateApartmentEntity',
                'getUserService'
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceMock->method('hydrateApartmentEntity')->willReturn(new ApartmentEntity());
        $apartmentServiceMock->method('getUserService')->willReturn($userServiceStub);
        $realtor = new UserEntity();
        $realtor->setType(UserEntity::TYPE_REALTOR);
        $userServiceStub->method('getUser')->willReturn($realtor);

        $entityManagerStub = $this->getEntityManagerStub();
        $apartmentServiceMock->method('getEntityManager')->willReturn($entityManagerStub);


        $entityManagerStub->expects($this->once())->method('persist');
        $entityManagerStub->expects($this->once())->method('flush');
        $apartmentServiceMock->expects($this->once())->method('getUserService');
        $apartmentServiceMock->expects($this->once())->method('hydrateApartmentEntity');

        $loggedInUser = new UserEntity();
        $loggedInUser->setType(UserEntity::TYPE_ADMIN);

        $result = $apartmentServiceMock->createApartment(['realtor_id' => 1], $loggedInUser);
        $this->assertInstanceOf(ApartmentEntity::class, $result);
    }

    public function testCreateApartmentEmptyRealtorIdThrowsInvalidArgumentException()
    {
        $apartmentServiceMock = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'getEntityManager',
                'hydrateApartmentEntity',
                'getUserService'
            ])
            ->disableOriginalConstructor()
            ->getMock();


        $apartmentServiceMock->method('hydrateApartmentEntity')->willReturn(new ApartmentEntity());
        $entityManagerStub = $this->getEntityManagerStub();

        $entityManagerStub->expects($this->never())->method('persist');
        $entityManagerStub->expects($this->never())->method('flush');
        $apartmentServiceMock->expects($this->never())->method('getUserService');
        $apartmentServiceMock->expects($this->once())->method('hydrateApartmentEntity');

        $loggedInUser = new UserEntity();
        $loggedInUser->setType(UserEntity::TYPE_ADMIN);

        $this->expectException(\InvalidArgumentException::class);
        $apartmentServiceMock->createApartment([], $loggedInUser);
    }

    public function testCreateApartmentUserWithRealtorIdIsNotRealtor()
    {
        $apartmentServiceMock = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'getEntityManager',
                'hydrateApartmentEntity',
                'getUserService'
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceMock->method('hydrateApartmentEntity')->willReturn(new ApartmentEntity());
        $apartmentServiceMock->method('getUserService')->willReturn($userServiceStub);
        $admin = new UserEntity();
        $admin->setType(UserEntity::TYPE_ADMIN);
        $userServiceStub->method('getUser')->willReturn($admin);

        $entityManagerStub = $this->getEntityManagerStub();

        $entityManagerStub->expects($this->never())->method('persist');
        $entityManagerStub->expects($this->never())->method('flush');
        $apartmentServiceMock->expects($this->once())->method('getUserService');
        $apartmentServiceMock->expects($this->once())->method('hydrateApartmentEntity');

        $loggedInUser = new UserEntity();
        $loggedInUser->setType(UserEntity::TYPE_ADMIN);

        $this->expectException(\InvalidArgumentException::class);
        $apartmentServiceMock->createApartment(['realtor_id' => 1], $loggedInUser);
    }

    public function testGetApartmentSuccess()
    {
        $apartmentServiceMock = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'getEntityManager',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $entityManagerStub = $this->getEntityManagerStub();
        $apartmentServiceMock->method('getEntityManager')->willReturn($entityManagerStub);

        $repositoryStub = $this->getRepositoryStub();
        $entityManagerStub->method('getRepository')->willReturn($repositoryStub);

        $repositoryStub->method('findOneBy')->willReturn(new ApartmentEntity());

        $loggedInUser = new UserEntity();
        $loggedInUser->setType(UserEntity::TYPE_ADMIN);

        $result = $apartmentServiceMock->getApartment(10, $loggedInUser);
        $this->assertInstanceOf(ApartmentEntity::class, $result);

    }

    public function testGetApartmentThrowsApartmentNotFoundException()
    {
        $apartmentServiceMock = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'getEntityManager',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $entityManagerStub = $this->getEntityManagerStub();
        $apartmentServiceMock->method('getEntityManager')->willReturn($entityManagerStub);

        $repositoryStub = $this->getRepositoryStub();
        $entityManagerStub->method('getRepository')->willReturn($repositoryStub);

        $repositoryStub->method('findOneBy')->willReturn(null);

        $loggedInUser = new UserEntity();

        $this->expectException(ApartmentNotFoundException::class);
        $apartmentServiceMock->getApartment(10, $loggedInUser);

    }

    public function testGetApartmentThrowsRealtorCanNotCRUDOtherRealtorsApartmentsException()
    {
        $apartmentServiceMock = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'getEntityManager',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $entityManagerStub = $this->getEntityManagerStub();
        $apartmentServiceMock->method('getEntityManager')->willReturn($entityManagerStub);

        $repositoryStub = $this->getRepositoryStub();
        $entityManagerStub->method('getRepository')->willReturn($repositoryStub);

        $realtor = new UserEntity();
        $realtor->setType(UserEntity::TYPE_REALTOR);
        $realtor->setId(1);
        $apartment = new ApartmentEntity();
        $apartment->setRealtor($realtor);

        $repositoryStub->method('findOneBy')->willReturn($apartment);

        $loggedInUser = new UserEntity();
        $loggedInUser->setType(UserEntity::TYPE_REALTOR);
        $loggedInUser->setId(20);

        $this->expectException(RealtorCanNotCRUDOtherRealtorsApartmentsException::class);
        $apartmentServiceMock->getApartment(10, $loggedInUser);

    }

    public function testDeleteApartmentSuccess()
    {
        $apartmentServiceMock = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'getEntityManager',
                'getApartment',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $entityManagerStub = $this->getEntityManagerStub();
        $apartmentServiceMock->method('getEntityManager')->willReturn($entityManagerStub);

        $apartmentServiceMock->method('getApartment')->willReturn(new ApartmentEntity());

        $entityManagerStub->expects($this->once())->method('remove');
        $entityManagerStub->expects($this->once())->method('flush');
        $apartmentServiceMock->expects($this->once())->method('getApartment');

        $apartmentServiceMock->deleteApartment(10, new UserEntity());

    }

    public function testUpdateApartmentSuccessRealtorIdIsEmpty()
    {
        $apartmentServiceMock = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'getEntityManager',
                'getApartment',
                'hydrateApartmentEntity',
                'getUserService',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $entityManagerStub = $this->getEntityManagerStub();
        $apartmentServiceMock->method('getEntityManager')->willReturn($entityManagerStub);
        $apartmentServiceMock->method('getApartment')->willReturn(new ApartmentEntity());
        $apartmentServiceMock->method('hydrateApartmentEntity')->willReturn(new ApartmentEntity());

        $entityManagerStub->expects($this->once())->method('persist');
        $entityManagerStub->expects($this->once())->method('flush');
        $apartmentServiceMock->expects($this->once())->method('getApartment');
        $apartmentServiceMock->expects($this->once())->method('hydrateApartmentEntity');
        $apartmentServiceMock->expects($this->never())->method('getUserService');


        $result = $apartmentServiceMock->updateApartment(10, [], new UserEntity());
        $this->assertInstanceOf(ApartmentEntity::class, $result);
    }

    public function testUpdateApartmentSuccessRealtorIdIsNotEmpty()
    {
        $apartmentServiceMock = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'getUserService',
                'getEntityManager',
                'getApartment',
                'hydrateApartmentEntity',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $entityManagerStub = $this->getEntityManagerStub();
        $apartmentServiceMock->method('getEntityManager')->willReturn($entityManagerStub);

        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceMock->method('getUserService')->willReturn($userServiceStub);
        $userServiceStub->method('getUser')->willReturn(new UserEntity());

        $realtor = new UserEntity();
        $realtor->setType(UserEntity::TYPE_REALTOR);
        $realtor->setId(1);
        $apartment = new ApartmentEntity();
        $apartment->setRealtor($realtor);
        $apartmentServiceMock->method('getApartment')->willReturn($apartment);
        $apartmentServiceMock->method('hydrateApartmentEntity')->willReturn(new ApartmentEntity());


        $entityManagerStub->expects($this->once())->method('persist');
        $entityManagerStub->expects($this->once())->method('flush');
        $apartmentServiceMock->expects($this->once())->method('getApartment');
        $apartmentServiceMock->expects($this->once())->method('hydrateApartmentEntity');
        $apartmentServiceMock->expects($this->once())->method('getUserService');
        $userServiceStub->expects($this->once())->method('getUser');

        $loggedInUser = new UserEntity();
        $loggedInUser->setType(UserEntity::TYPE_ADMIN);
        $result = $apartmentServiceMock->updateApartment(10, ['realtor_id' => 20], $loggedInUser);
        $this->assertInstanceOf(ApartmentEntity::class, $result);
    }

    public function testUpdateThrowsUserIsNotAdminException()
    {
        $apartmentServiceMock = $this
            ->getMockBuilder(ApartmentService::class)
            ->setMethods([
                'getUserService',
                'getEntityManager',
                'getApartment',
                'hydrateApartmentEntity',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $entityManagerStub = $this->getEntityManagerStub();
        $apartmentServiceMock->method('getEntityManager')->willReturn($entityManagerStub);

        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $apartmentServiceMock->method('getUserService')->willReturn($userServiceStub);
        $userServiceStub->method('getUser')->willReturn(new UserEntity());


        $apartmentServiceMock->method('getApartment')->willReturn(new ApartmentEntity());
        $apartmentServiceMock->method('hydrateApartmentEntity')->willReturn(new ApartmentEntity());

        $entityManagerStub->expects($this->never())->method('persist');
        $entityManagerStub->expects($this->never())->method('flush');
        $apartmentServiceMock->expects($this->once())->method('getApartment');
        $apartmentServiceMock->expects($this->once())->method('hydrateApartmentEntity');
        $apartmentServiceMock->expects($this->never())->method('getUserService');
        $userServiceStub->expects($this->never())->method('getUser');

        $loggedInUser = new UserEntity();
        $loggedInUser->setType(UserEntity::TYPE_REALTOR);

        $this->expectException(UserIsNotAdminException::class);
        $apartmentServiceMock->updateApartment(10, ['realtor_id' => 20], $loggedInUser);
    }

}
