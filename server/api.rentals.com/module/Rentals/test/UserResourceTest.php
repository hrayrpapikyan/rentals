<?php

namespace RentalsTest;

use Application\Constant\Error;
use Application\Exception\DuplicateRequestException;
use ApplicationTest\ResourceBaseTest;
use Rentals\V1\Rest\User\Exception\AdminUserCanNotBeDeletedException;
use Rentals\V1\Rest\User\Exception\UserCanChangeOnlyHisPasswordException;
use Rentals\V1\Rest\User\Exception\UserEmailBusyException;
use Rentals\V1\Rest\User\Exception\UserIsNotAdminException;
use Rentals\V1\Rest\User\Exception\UserNotFoundException;
use Rentals\V1\Rest\User\UserResource;
use Rentals\V1\Rest\User\UserService;
use Rentals\V1\Rest\User\UserEntity;
use ZF\ApiProblem\ApiProblem;


class UserResourceTest extends ResourceBaseTest
{


    public function testCreateSuccess()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getCompleteRequestService',
                'getUserService',
            ])
            ->disableOriginalConstructor()
            ->getMock();


        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'createUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $competeRequestServiceStub = $this->getCompleteRequestServiceStub();
        $competeRequestServiceStub->method('checkRequest')->willReturn(null);

        $userResourceMock->method('getCompleteRequestService')->willReturn($competeRequestServiceStub);
        $userResourceMock->method('getUserService')->willReturn($userServiceStub);

        $userServiceStub->method('createUser')->willReturn(new UserEntity());

        $data = new \stdClass();
        $data->uuid = '624d5b7a-3e98-4344-bbb1-e7bebcbff66a';

        $userResourceMock->expects($this->once())->method('getUserService');
        $userResourceMock->expects($this->exactly(2))->method('getCompleteRequestService');
        $competeRequestServiceStub->expects($this->once())->method('checkRequest');
        $competeRequestServiceStub->expects($this->once())->method('complete');
        $userServiceStub->expects($this->once())->method('createUser');
        $result = $userResourceMock->create($data);
        $this->assertInstanceOf(UserEntity::class, $result);

    }

    public function testCreateThrowsDuplicateRequestException()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getCompleteRequestService',
                'getUserService',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $competeRequestServiceStub = $this->getCompleteRequestServiceStub();
        $exception = new DuplicateRequestException();
        $competeRequestServiceStub->method('checkRequest')->willThrowException($exception);

        $userResourceMock->method('getCompleteRequestService')->willReturn($competeRequestServiceStub);

        $data = new \stdClass();
        $data->uuid = '624d5b7a-3e98-4344-bbb1-e7bebcbff66a';

        $userResourceMock->expects($this->once())->method('getCompleteRequestService');
        $competeRequestServiceStub->expects($this->once())->method('checkRequest');
        $userResourceMock->expects($this->never())->method('getUserService');

        /**
         * @var ApiProblem $result
         */
        $result = $userResourceMock->create($data);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(409, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }

    public function testCreateThrowsUserEmailBusyException()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getCompleteRequestService',
                'getUserService',
            ])
            ->disableOriginalConstructor()
            ->getMock();


        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'createUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $competeRequestServiceStub = $this->getCompleteRequestServiceStub();
        $competeRequestServiceStub->method('checkRequest')->willReturn(null);

        $userResourceMock->method('getCompleteRequestService')->willReturn($competeRequestServiceStub);
        $userResourceMock->method('getUserService')->willReturn($userServiceStub);

        $exception = new UserEmailBusyException();
        $userServiceStub->method('createUser')->willThrowException($exception);

        $data = new \stdClass();
        $data->uuid = '624d5b7a-3e98-4344-bbb1-e7bebcbff66a';

        $userResourceMock->expects($this->once())->method('getUserService');
        $userResourceMock->expects($this->once())->method('getCompleteRequestService');
        $competeRequestServiceStub->expects($this->once())->method('checkRequest');
        $competeRequestServiceStub->expects($this->never())->method('complete');
        $userServiceStub->expects($this->once())->method('createUser');

        /**
         * @var ApiProblem $result
         */
        $result = $userResourceMock->create($data);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(400, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);

    }

    public function testCreateThrowsUnknownException()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getCompleteRequestService',
                'getUserService',
                'getLogger',
            ])
            ->disableOriginalConstructor()
            ->getMock();


        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'createUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $competeRequestServiceStub = $this->getCompleteRequestServiceStub();
        $competeRequestServiceStub->method('checkRequest')->willReturn(null);

        $userResourceMock->method('getCompleteRequestService')->willReturn($competeRequestServiceStub);
        $userResourceMock->method('getUserService')->willReturn($userServiceStub);
        $loggerStub = $this->getLoggerStub();
        $userResourceMock->method('getLogger')->willReturn($loggerStub);

        $exception = new \Exception();
        $userServiceStub->method('createUser')->willThrowException($exception);

        $data = new \stdClass();
        $data->uuid = '624d5b7a-3e98-4344-bbb1-e7bebcbff66a';

        $userResourceMock->expects($this->once())->method('getUserService');
        $userResourceMock->expects($this->once())->method('getCompleteRequestService');
        $competeRequestServiceStub->expects($this->once())->method('checkRequest');
        $competeRequestServiceStub->expects($this->never())->method('complete');
        $userServiceStub->expects($this->once())->method('createUser');
        $loggerStub->expects($this->once())->method('err');

        /**
         * @var ApiProblem $result
         */
        $result = $userResourceMock->create($data);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(500, $result->toArray()['status']);
        $this->assertEquals(Error::SERVER_PROBLEM, $result->toArray()['detail']);

    }



    public function testDeleteSuccess()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'checkIsAdmin',
                'getUserService',
            ])
            ->disableOriginalConstructor()
            ->getMock();


        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
                'deleteUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();



        $userResourceMock->method('checkIsAdmin')->willReturn(null);
        $userResourceMock->method('getUserService')->willReturn($userServiceStub);

        $userEntity = new UserEntity();
        $userEntity->setType(UserEntity::TYPE_CLIENT);

        $userServiceStub->method('getUser')->willReturn($userEntity);
        $userServiceStub->method('deleteUser')->willReturn(null);


        $userResourceMock->expects($this->once())->method('checkIsAdmin');
        $userResourceMock->expects($this->exactly(2))->method('getUserService');
        $userServiceStub->expects($this->once())->method('getUser');
        $result = $userResourceMock->delete(1);
        $this->assertTrue($result);

    }


    public function testDeleteThrowsUserIsNotAdminException()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'checkIsAdmin',
                'getUserService',

            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'deleteUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();


        $exception = new UserIsNotAdminException();
        $userResourceMock->method('checkIsAdmin')->willThrowException($exception);


        $userServiceStub->expects($this->never())->method('deleteUser');

        /**
         * @var ApiProblem $result
         */
        $result = $userResourceMock->delete(1);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(403, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }

    public function testDeleteThrowsUserNotFoundException()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'checkIsAdmin',
                'getUserService',

            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
                'deleteUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();


        $userResourceMock->method('checkIsAdmin')->willReturn(null);
        $userResourceMock->method('getUserService')->willReturn($userServiceStub);

        $exception = new UserNotFoundException();
        $userServiceStub->method('getUser')->willThrowException($exception);

        $userServiceStub->expects($this->never())->method('deleteUser');

        /**
         * @var ApiProblem $result
         */
        $result = $userResourceMock->delete(1);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(404, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }

    public function testDeleteThrowsAdminUserCanNotBeDeletedException()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'checkIsAdmin',
                'getUserService',

            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
                'deleteUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();


        $userResourceMock->method('checkIsAdmin')->willReturn(null);
        $userResourceMock->method('getUserService')->willReturn($userServiceStub);

        $userEntity = new UserEntity();
        $userEntity->setType(UserEntity::TYPE_ADMIN);

        $userServiceStub->method('getUser')->willReturn($userEntity);

        $userServiceStub->expects($this->never())->method('deleteUser');

        /**
         * @var ApiProblem $result
         */
        $exception = new AdminUserCanNotBeDeletedException();
        $result = $userResourceMock->delete(1);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(403, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }


    public function testDeleteThrowsUnknownException()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'checkIsAdmin',
                'getUserService',
                'getLogger',
            ])
            ->disableOriginalConstructor()
            ->getMock();


        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
                'deleteUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();



        $userResourceMock->method('checkIsAdmin')->willReturn(null);
        $userResourceMock->method('getUserService')->willReturn($userServiceStub);

        $userEntity = new UserEntity();
        $userEntity->setType(UserEntity::TYPE_CLIENT);

        $userServiceStub->method('getUser')->willReturn($userEntity);
        $userServiceStub->method('deleteUser')->willThrowException(new \Exception());

        $loggerStub = $this->getLoggerStub();
        $userResourceMock->method('getLogger')->willReturn($loggerStub);


        $userResourceMock->expects($this->once())->method('checkIsAdmin');
        $userResourceMock->expects($this->exactly(2))->method('getUserService');
        $userServiceStub->expects($this->once())->method('getUser');
        $userResourceMock->expects($this->once())->method('getLogger');
        $loggerStub->expects($this->once())->method('err');

        $result = $userResourceMock->delete(1);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(500, $result->toArray()['status']);
        $this->assertEquals(Error::SERVER_PROBLEM, $result->toArray()['detail']);
    }

    public function testFetchSuccessIdEqualsMe()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getLoggedInUserId',
                'getUserService',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userResourceMock->method('getLoggedInUserId')->willReturn(1);
        $userResourceMock->method('getUserService')->willReturn($userServiceStub);
        $userServiceStub->method('getUser')->willReturn(new UserEntity());

        $userResourceMock->expects($this->once())->method('getLoggedInUserId');
        $userResourceMock->expects($this->once())->method('getUserService');
        $userServiceStub->expects($this->once())->method('getUser');

        $result = $userResourceMock->fetch(UserResource::CURRENT_USER_PARAM);

        $this->assertInstanceOf(UserEntity::class, $result);

    }

    public function testFetchSuccessIdEqualsMyId()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getLoggedInUserId',
                'getUserService',
                'checkIsAdmin',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userResourceMock->method('getLoggedInUserId')->willReturn(1);
        $userResourceMock->method('getUserService')->willReturn($userServiceStub);
        $userServiceStub->method('getUser')->willReturn(new UserEntity());

        $userResourceMock->expects($this->once())->method('getLoggedInUserId');
        $userResourceMock->expects($this->once())->method('getUserService');
        $userResourceMock->expects($this->never())->method('checkIsAdmin');
        $userServiceStub->expects($this->once())->method('getUser');

        $result = $userResourceMock->fetch(1);

        $this->assertInstanceOf(UserEntity::class, $result);

    }

    public function testFetchSuccessIdNotEqualsMyIdButIsAdmin()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getLoggedInUserId',
                'getUserService',
                'checkIsAdmin',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userResourceMock->method('getLoggedInUserId')->willReturn(1);
        $userResourceMock->method('checkIsAdmin')->willReturn(null);
        $userResourceMock->method('getUserService')->willReturn($userServiceStub);
        $userServiceStub->method('getUser')->willReturn(new UserEntity());

        $userResourceMock->expects($this->once())->method('getLoggedInUserId');
        $userResourceMock->expects($this->once())->method('getUserService');
        $userResourceMock->expects($this->once())->method('checkIsAdmin');
        $userServiceStub->expects($this->once())->method('getUser');

        $result = $userResourceMock->fetch(2);

        $this->assertInstanceOf(UserEntity::class, $result);

    }

    public function testFetchThrowsUserIsNotAdminException()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getLoggedInUserId',
                'getUserService',
                'checkIsAdmin',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userResourceMock->method('getLoggedInUserId')->willReturn(1);
        $exception = new UserIsNotAdminException();
        $userResourceMock->method('checkIsAdmin')->willThrowException($exception);

        $userResourceMock->expects($this->once())->method('getLoggedInUserId');
        $userResourceMock->expects($this->never())->method('getUserService');
        $userResourceMock->expects($this->once())->method('checkIsAdmin');
        $userServiceStub->expects($this->never())->method('getUser');

        $result = $userResourceMock->fetch(2);
        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(403, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }

    public function testFetchThrowsUserNotFoundException()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getLoggedInUserId',
                'getUserService',
                'checkIsAdmin',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userResourceMock->method('getLoggedInUserId')->willReturn(1);
        $userResourceMock->method('checkIsAdmin')->willReturn(null);
        $userResourceMock->method('getUserService')->willReturn($userServiceStub);
        $exception = new UserNotFoundException();
        $userServiceStub->method('getUser')->willThrowException($exception);

        $userResourceMock->expects($this->once())->method('getLoggedInUserId');
        $userResourceMock->expects($this->once())->method('getUserService');
        $userResourceMock->expects($this->once())->method('checkIsAdmin');
        $userServiceStub->expects($this->once())->method('getUser');

        $result = $userResourceMock->fetch(2);

        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(404, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }

    public function testFetchThrowsUnknownException()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getLoggedInUserId',
                'getUserService',
                'checkIsAdmin',
                'getLogger',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userResourceMock->method('getLoggedInUserId')->willReturn(1);
        $userResourceMock->method('checkIsAdmin')->willReturn(null);
        $userResourceMock->method('getUserService')->willReturn($userServiceStub);
        $exception = new \Exception();
        $userServiceStub->method('getUser')->willThrowException($exception);

        $loggerStub = $this->getLoggerStub();
        $userResourceMock->method('getLogger')->willReturn($loggerStub);

        $userResourceMock->expects($this->once())->method('getLoggedInUserId');
        $userResourceMock->expects($this->once())->method('getUserService');
        $userResourceMock->expects($this->once())->method('checkIsAdmin');
        $userServiceStub->expects($this->once())->method('getUser');
        $userResourceMock->expects($this->once())->method('getLogger');
        $loggerStub->expects($this->once())->method('err');

        $result = $userResourceMock->fetch(2);

        $this->assertInstanceOf(ApiProblem::class, $result);
        $this->assertEquals(500, $result->toArray()['status']);
        $this->assertEquals(Error::SERVER_PROBLEM, $result->toArray()['detail']);
    }

    public function testFetchAllPaginatedResultSuccess()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getUserService',
                'checkIsAdmin'
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUsersPaginated',
                'getRealtors',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userResourceMock->method('checkIsAdmin')->willReturn(null);
        $userResourceMock->method('getUserService')->willReturn($userServiceStub);

        $userServiceStub->method('getUsersPaginated')->willReturn([
            'users' => [new UserEntity(), new UserEntity()],
            'total' => 20,
            "per_page" => 10,
            "page" => 1,
            "page_count" => 2
        ]);

        $userResourceMock->expects($this->once())->method('checkIsAdmin');
        $userResourceMock->expects($this->once())->method('getUserService');
        $userServiceStub->expects($this->once())->method('getUsersPaginated')->with([]);
        $userServiceStub->expects($this->never())->method('getRealtors');

        $result = $userResourceMock->fetchAll();
        $this->assertArrayHasKey('users', $result);
        $this->assertCount(2, $result['users']);
        $this->assertInstanceOf(UserEntity::class, $result['users'][0]);
        $this->assertInstanceOf(UserEntity::class, $result['users'][1]);
        $this->assertArrayHasKey('total', $result);
        $this->assertArrayHasKey('per_page', $result);
        $this->assertArrayHasKey('page', $result);
        $this->assertArrayHasKey('page_count', $result);

    }

    public function testFetchAllRealtorsSuccess()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getUserService',
                'checkIsAdmin'
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUsersPaginated',
                'getRealtors',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userResourceMock->method('checkIsAdmin')->willReturn(null);
        $userResourceMock->method('getUserService')->willReturn($userServiceStub);

        $userServiceStub->method('getRealtors')->willReturn([new UserEntity(), new UserEntity()]);

        $userResourceMock->expects($this->once())->method('checkIsAdmin');
        $userResourceMock->expects($this->once())->method('getUserService');
        $userServiceStub->expects($this->never())->method('getUsersPaginated');
        $userServiceStub->expects($this->once())->method('getRealtors');

        $result = $userResourceMock->fetchAll(['only_realtors' => 1]);
        $this->assertCount(2, $result);
        $this->assertInstanceOf(UserEntity::class, $result[0]);
        $this->assertInstanceOf(UserEntity::class, $result[1]);

    }

    public function testFetchAllThrowsUserIsNotAdminException()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getUserService',
                'checkIsAdmin'
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUsersPaginated',
                'getRealtors',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $exception = new UserIsNotAdminException();
        $userResourceMock->method('checkIsAdmin')->willThrowException($exception);

        $userResourceMock->expects($this->once())->method('checkIsAdmin');
        $userResourceMock->expects($this->never())->method('getUserService');
        $userServiceStub->expects($this->never())->method('getUsersPaginated');
        $userServiceStub->expects($this->never())->method('getRealtors');

        $result = $userResourceMock->fetchAll();
        $this->assertEquals(403, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);

    }

    public function testFetchAllThrowsUnknownException()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getUserService',
                'checkIsAdmin',
                'getLogger',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUsersPaginated',
                'getRealtors',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userResourceMock->method('checkIsAdmin')->willReturn(null);
        $userResourceMock->method('getUserService')->willReturn($userServiceStub);

        $loggerStub = $this->getLoggerStub();
        $userResourceMock->method('getLogger')->willReturn($loggerStub);

        $userServiceStub->method('getUsersPaginated')->willThrowException(new \Exception());

        $userResourceMock->expects($this->once())->method('checkIsAdmin');
        $userResourceMock->expects($this->once())->method('getUserService');
        $userResourceMock->expects($this->once())->method('getLogger');
        $userServiceStub->expects($this->once())->method('getUsersPaginated')->with([]);
        $userServiceStub->expects($this->never())->method('getRealtors');
        $loggerStub->expects($this->once())->method('err');

        $result = $userResourceMock->fetchAll();
        $this->assertEquals(500, $result->toArray()['status']);
        $this->assertEquals(Error::SERVER_PROBLEM, $result->toArray()['detail']);

    }

    public function testPatchIdEqualsMe()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getLoggedInUserId',
                'getUserService',
                'checkIsAdmin',
                'getLoggedInUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();


        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
                'updateUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userResourceMock->method('getUserService')->willReturn($userServiceStub);
        $userResourceMock->method('getLoggedInUserId')->willReturn(1);
        $userServiceStub->method('getUser')->willReturn(new UserEntity());
        $userResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());
        $userServiceStub->method('updateUser')->willReturn(null);

        $userResourceMock->expects($this->exactly(2))->method('getUserService');
        $userResourceMock->expects($this->never())->method('checkIsAdmin');
        $userResourceMock->expects($this->once())->method('getLoggedInUserId');
        $userResourceMock->expects($this->once())->method('getLoggedInUser');

        $result = $userResourceMock->patch(UserResource::CURRENT_USER_PARAM, []);
        $this->assertInstanceOf(UserEntity::class, $result);

    }

    public function testPatchIdEqualsMyId()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getLoggedInUserId',
                'getUserService',
                'checkIsAdmin',
                'getLoggedInUser',

            ])
            ->disableOriginalConstructor()
            ->getMock();


        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
                'updateUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userResourceMock->method('getUserService')->willReturn($userServiceStub);
        $userServiceStub->method('getUser')->willReturn(new UserEntity());
        $userServiceStub->method('updateUser')->willReturn(null);
        $userResourceMock->method('getLoggedInUserId')->willReturn(1);
        $userResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());


        $userResourceMock->expects($this->exactly(2))->method('getUserService');
        $userResourceMock->expects($this->never())->method('checkIsAdmin');
        $userResourceMock->expects($this->once())->method('getLoggedInUserId');
        $userResourceMock->expects($this->once())->method('getLoggedInUser');


        $result = $userResourceMock->patch(1, []);
        $this->assertInstanceOf(UserEntity::class, $result);

    }

    public function testPatchIdNotEqualMyIdButLoggedInUserIsAdmin()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getLoggedInUserId',
                'getUserService',
                'checkIsAdmin',
                'getLoggedInUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();


        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
                'updateUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userResourceMock->method('getUserService')->willReturn($userServiceStub);
        $userServiceStub->method('getUser')->willReturn(new UserEntity());
        $userServiceStub->method('updateUser')->willReturn(null);
        $userResourceMock->method('getLoggedInUserId')->willReturn(1);
        $userResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());


        $userResourceMock->expects($this->exactly(2))->method('getUserService');
        $userResourceMock->expects($this->once())->method('checkIsAdmin');
        $userResourceMock->expects($this->once())->method('getLoggedInUserId');
        $userResourceMock->expects($this->once())->method('getLoggedInUser');


        $result = $userResourceMock->patch(2, []);
        $this->assertInstanceOf(UserEntity::class, $result);

    }

    public function testPatchThrowsUserNotFoundException()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getLoggedInUserId',
                'getUserService',
                'checkIsAdmin',
            ])
            ->disableOriginalConstructor()
            ->getMock();


        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
                'updateUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userResourceMock->method('getUserService')->willReturn($userServiceStub);

        $exception = new UserNotFoundException();
        $userServiceStub->method('getUser')->willThrowException($exception);
        $userResourceMock->method('getLoggedInUserId')->willReturn(1);

        $userResourceMock->expects($this->once())->method('getUserService');
        $userServiceStub->expects($this->never())->method('updateUser');
        $userResourceMock->expects($this->once())->method('checkIsAdmin');
        $userResourceMock->expects($this->once())->method('getLoggedInUserId');

        $result = $userResourceMock->patch(2, []);
        $this->assertEquals(404, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);

    }

    public function testPatchThrowsUserIsNotAdminException()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getLoggedInUserId',
                'getUserService',
                'checkIsAdmin',
            ])
            ->disableOriginalConstructor()
            ->getMock();


        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
                'updateUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userResourceMock->method('getUserService')->willReturn($userServiceStub);
        $userResourceMock->method('getLoggedInUserId')->willReturn(1);
        $exception = new UserIsNotAdminException();
        $userResourceMock->method('checkIsAdmin')->willThrowException($exception);


        $userResourceMock->expects($this->never())->method('getUserService');
        $userResourceMock->expects($this->once())->method('checkIsAdmin');
        $userServiceStub->expects($this->never())->method('updateUser');
        $userResourceMock->expects($this->once())->method('getLoggedInUserId');

        $result = $userResourceMock->patch(2, []);
        $this->assertEquals(403, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }

    public function testPatchThrowsInvalidArgumentException()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getLoggedInUserId',
                'getUserService',
                'checkIsAdmin',
                'getLoggedInUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();


        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
                'updateUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userResourceMock->method('getUserService')->willReturn($userServiceStub);
        $userServiceStub->method('getUser')->willReturn(new UserEntity());

        $exception = new \InvalidArgumentException('message');
        $userServiceStub->method('updateUser')->willThrowException($exception);
        $userResourceMock->method('getLoggedInUserId')->willReturn(1);
        $userResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());

        $userResourceMock->expects($this->exactly(2))->method('getUserService');
        $userResourceMock->expects($this->once())->method('checkIsAdmin');
        $userServiceStub->expects($this->once())->method('updateUser');
        $userResourceMock->expects($this->once())->method('getLoggedInUserId');
        $userResourceMock->expects($this->once())->method('getLoggedInUser');

        $result = $userResourceMock->patch(2, []);
        $this->assertEquals(400, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }

    public function testPatchThrowsUserCanChangeOnlyHisPasswordException()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getLoggedInUserId',
                'getUserService',
                'checkIsAdmin',
                'getLoggedInUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();


        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
                'updateUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userResourceMock->method('getUserService')->willReturn($userServiceStub);
        $userServiceStub->method('getUser')->willReturn(new UserEntity());

        $exception = new UserCanChangeOnlyHisPasswordException();
        $userServiceStub->method('updateUser')->willThrowException($exception);
        $userResourceMock->method('getLoggedInUserId')->willReturn(1);
        $userResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());


        $userResourceMock->expects($this->exactly(2))->method('getUserService');
        $userResourceMock->expects($this->once())->method('checkIsAdmin');
        $userServiceStub->expects($this->once())->method('updateUser');
        $userResourceMock->expects($this->once())->method('getLoggedInUserId');
        $userResourceMock->expects($this->once())->method('getLoggedInUser');


        $result = $userResourceMock->patch(2, []);
        $this->assertEquals(403, $result->toArray()['status']);
        $this->assertEquals($exception->getMessage(), $result->toArray()['detail']);
    }

    public function testPatchThrowsUnknownException()
    {
        $userResourceMock = $this
            ->getMockBuilder(UserResource::class)
            ->setMethods([
                'getLoggedInUserId',
                'getUserService',
                'checkIsAdmin',
                'getLoggedInUser',
                'getLogger',
            ])
            ->disableOriginalConstructor()
            ->getMock();


        $userServiceStub = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUser',
                'updateUser',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userResourceMock->method('getUserService')->willReturn($userServiceStub);
        $userServiceStub->method('getUser')->willReturn(new UserEntity());
        $userServiceStub->method('updateUser')->willThrowException(new \Exception());
        $userResourceMock->method('getLoggedInUserId')->willReturn(1);
        $userResourceMock->method('getLoggedInUser')->willReturn(new UserEntity());

        $loggerStub = $this->getLoggerStub();
        $userResourceMock->method('getLogger')->willReturn($loggerStub);

        $userResourceMock->expects($this->exactly(2))->method('getUserService');
        $userResourceMock->expects($this->once())->method('checkIsAdmin');
        $userServiceStub->expects($this->once())->method('updateUser');
        $userResourceMock->expects($this->once())->method('getLoggedInUserId');
        $userResourceMock->expects($this->once())->method('getLoggedInUser');
        $userResourceMock->expects($this->once())->method('getLogger');
        $loggerStub->expects($this->once())->method('err');

        $result = $userResourceMock->patch(2, []);
        $this->assertEquals(500, $result->toArray()['status']);
        $this->assertEquals(Error::SERVER_PROBLEM, $result->toArray()['detail']);
    }


}
