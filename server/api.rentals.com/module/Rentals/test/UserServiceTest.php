<?php

namespace RentalsTest;

use ApplicationTest\ServiceBaseTest;
use Rentals\V1\Rest\User\Exception\UserCanChangeOnlyHisPasswordException;
use Rentals\V1\Rest\User\Exception\UserEmailBusyException;
use Rentals\V1\Rest\User\Exception\UserNotFoundException;
use Rentals\V1\Rest\User\UserCollection;
use Rentals\V1\Rest\User\UserService;
use Rentals\V1\Rest\User\UserEntity;
use Zend\Crypt\Password\Bcrypt;


class UserServiceTest extends ServiceBaseTest
{
    public function testCreateUserSuccess()
    {

        $userServiceMock = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getEntityManager',
                'hydrateUserEntity',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userServiceMock->method('hydrateUserEntity')->willReturn(new UserEntity());

        $entityManagerStub = $this->getEntityManagerStub();
        $userServiceMock->method('getEntityManager')->willReturn($entityManagerStub);

        $repositoryStub = $this->getRepositoryStub();
        $entityManagerStub->method('getRepository')->willReturn($repositoryStub);

        $repositoryStub->method('findOneBy')->willReturn(null);

        $entityManagerStub->expects($this->once())->method('persist');
        $entityManagerStub->expects($this->once())->method('flush');
        $entityManagerStub->expects($this->once())->method('getRepository');
        $repositoryStub->expects($this->once())->method('findOneBy');

        $result = $userServiceMock->createUser([]);
        $this->assertInstanceOf(UserEntity::class, $result);

    }

    public function testCreateUserThrowsDomainException()
    {

        $userServiceMock = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getEntityManager',
                'hydrateUserEntity',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $userServiceMock->method('hydrateUserEntity')->willReturn(new UserEntity());

        $entityManagerStub = $this->getEntityManagerStub();
        $userServiceMock->method('getEntityManager')->willReturn($entityManagerStub);

        $repositoryStub = $this->getRepositoryStub();
        $entityManagerStub->method('getRepository')->willReturn($repositoryStub);

        $repositoryStub->method('findOneBy')->willReturn(new UserEntity());

        $entityManagerStub->expects($this->once())->method('getRepository');
        $repositoryStub->expects($this->once())->method('findOneBy');

        $this->expectException(UserEmailBusyException::class);
        $userServiceMock->createUser([]);

    }


    public function testGetUserSuccess()
    {
        $userServiceMock = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getEntityManager',
            ])
            ->disableOriginalConstructor()
            ->getMock();


        $entityManagerStub = $this->getEntityManagerStub();
        $userServiceMock->method('getEntityManager')->willReturn($entityManagerStub);

        $repositoryStub = $this->getRepositoryStub();
        $entityManagerStub->method('getRepository')->willReturn($repositoryStub);

        $repositoryStub->method('findOneBy')->willReturn(new UserEntity());

        $entityManagerStub->expects($this->once())->method('getRepository');
        $repositoryStub->expects($this->once())->method('findOneBy');

        $result = $userServiceMock->getUser(1);
        $this->assertInstanceOf(UserEntity::class, $result);
    }

    public function testGetUserThrowsUserNotFoundException()
    {
        $userServiceMock = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getEntityManager',
            ])
            ->disableOriginalConstructor()
            ->getMock();


        $entityManagerStub = $this->getEntityManagerStub();
        $userServiceMock->method('getEntityManager')->willReturn($entityManagerStub);

        $repositoryStub = $this->getRepositoryStub();
        $entityManagerStub->method('getRepository')->willReturn($repositoryStub);

        $repositoryStub->method('findOneBy')->willReturn(null);

        $entityManagerStub->expects($this->once())->method('getRepository');
        $repositoryStub->expects($this->once())->method('findOneBy');

        $this->expectException(UserNotFoundException::class);
        $userServiceMock->getUser(1);
    }

    public function testGetRealtors()
    {
        $userServiceMock = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getEntityManager',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $entityManagerStub = $this->getEntityManagerStub();
        $userServiceMock->method('getEntityManager')->willReturn($entityManagerStub);

        $repositoryStub = $this->getRepositoryStub();
        $entityManagerStub->method('getRepository')->willReturn($repositoryStub);

        $repositoryStub->method('findBy')->willReturn([new UserEntity(), new UserEntity()]);

        $entityManagerStub->expects($this->once())->method('getRepository');
        $repositoryStub->expects($this->once())->method('findBy');

        $result = $userServiceMock->getRealtors();
        $this->assertCount(2, $result);
        $this->assertInstanceOf(UserEntity::class, $result[0]);
    }

    public function testHydrateUserEntity()
    {
        $userServiceMock = $this
            ->getMockBuilder(UserService::class)
            ->disableOriginalConstructor()
            ->getMock();
        /**
         * @var UserEntity $result
         */
        $result = $this->callProtectedMethod($userServiceMock, 'hydrateUserEntity', [
            [
                'uuid' => '624d5b7a-3e98-4344-bbb1-e7bebcbff66a',
                'first_name' => 'John',
                'last_name'  => 'Doe',
                'email' => 'john.doe@rentals.com',
                'password' => 'john.doe',
                'password_confirm' => 'john.doe',
                'type' => 'client'
            ]
        ]);

        $this->assertInstanceOf(UserEntity::class, $result);
        $this->assertEquals('John', $result->getFirstName());
        $this->assertEquals('Doe', $result->getLastName());
        $this->assertEquals('john.doe@rentals.com', $result->getEmail());
        $this->assertEquals('client', $result->getType());
    }

    public function testGetUsersPaginated()
    {
        $userServiceMock = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getUserPaginatedCollection',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        /**
         * @var UserCollection $userCollectionStub
         */
        $userCollectionStub = $this
            ->getMockBuilder(UserCollection::class)
            ->disableOriginalConstructor()
            ->getMock();


        $userServiceMock->method('getUserPaginatedCollection')->willReturn($userCollectionStub);

        $result = $userServiceMock->getUsersPaginated(['page' => 2]);
        $this->assertArrayHasKey('total', $result);
        $this->assertArrayHasKey('per_page', $result);
        $this->assertArrayHasKey('page', $result);
        $this->assertArrayHasKey('page_count', $result);
        $this->assertArrayHasKey('users', $result);

    }

    public function testDeleteUser()
    {
        $userServiceMock = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getEntityManager',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $entityManagerStub = $this->getEntityManagerStub();
        $userServiceMock->method('getEntityManager')->willReturn($entityManagerStub);

        $entityManagerStub->method('remove')->willReturn(null);
        $entityManagerStub->method('flush')->willReturn(null);


        $entityManagerStub->expects($this->once())->method('remove');
        $entityManagerStub->expects($this->once())->method('flush');
        $userServiceMock->expects($this->exactly(2))->method('getEntityManager');

        $userServiceMock->deleteUser(new UserEntity());

    }

    public function testUpdateUserWithoutPasswordChangeSuccess()
    {
        $userServiceMock = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getEntityManager',
                'getBcryptObject',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $entityManagerStub = $this->getEntityManagerStub();
        $userServiceMock->method('getEntityManager')->willReturn($entityManagerStub);

        $entityManagerStub->method('persist')->willReturn(null);
        $entityManagerStub->method('flush')->willReturn(null);

        $entityManagerStub->expects($this->once())->method('persist');
        $entityManagerStub->expects($this->once())->method('flush');
        $userServiceMock->expects($this->never())->method('getBcryptObject');

        $userEntity = new UserEntity();
        $userServiceMock->updateUser($userEntity, new UserEntity(), ['first_name' => 'john', 'last_name' => 'doe']);

        $this->assertEquals('john', $userEntity->getFirstName());
        $this->assertEquals('doe', $userEntity->getLastName());
        $this->assertNull($userEntity->getPassword());
    }

    public function testUpdateUserWithPasswordChangeSuccess()
    {
        $userServiceMock = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getEntityManager',
                'getBcryptObject',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $bcryptStub = $this
            ->getMockBuilder(Bcrypt::class)
            ->setMethods([
                'verify',
                'create',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $entityManagerStub = $this->getEntityManagerStub();
        $userServiceMock->method('getEntityManager')->willReturn($entityManagerStub);
        $userServiceMock->method('getBcryptObject')->willReturn($bcryptStub);
        $bcryptStub->method('verify')->willReturn(true);
        $bcryptStub->method('create')->willReturn('password_hash');

        $entityManagerStub->method('persist')->willReturn(null);
        $entityManagerStub->method('flush')->willReturn(null);

        $entityManagerStub->expects($this->once())->method('persist');
        $entityManagerStub->expects($this->once())->method('flush');
        $userServiceMock->expects($this->once())->method('getBcryptObject');
        $bcryptStub->expects($this->once())->method('verify');
        $bcryptStub->expects($this->once())->method('create');

        $userEntity = new UserEntity();
        $userEntity->setId(1);

        $loggedInUser = new UserEntity();
        $loggedInUser->setId(1);
        $userServiceMock->updateUser($userEntity, $loggedInUser,
            [
                'first_name' => 'john',
                'last_name' => 'doe',
                'current_password' => 'john.doe',
                'password' => 'doe.john'
            ]
        );

        $this->assertEquals('john', $userEntity->getFirstName());
        $this->assertEquals('doe', $userEntity->getLastName());
        $this->assertEquals('password_hash', $userEntity->getPassword());
    }

    public function testUpdateUserThrowsUserCanChangeOnlyHisPasswordException()
    {
        $userServiceMock = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getEntityManager',
                'getBcryptObject',
            ])
            ->disableOriginalConstructor()
            ->getMock();


        $entityManagerStub = $this->getEntityManagerStub();
        $userServiceMock->method('getEntityManager')->willReturn($entityManagerStub);


        $entityManagerStub->expects($this->never())->method('persist');
        $entityManagerStub->expects($this->never())->method('flush');
        $userServiceMock->expects($this->never())->method('getBcryptObject');

        $userEntity = new UserEntity();
        $userEntity->setId(1);

        $loggedInUser = new UserEntity();
        $loggedInUser->setId(2);

        $this->expectException(UserCanChangeOnlyHisPasswordException::class);
        $userServiceMock->updateUser($userEntity, $loggedInUser,
            [
                'first_name' => 'john',
                'last_name' => 'doe',
                'current_password' => 'john.doe',
                'password' => 'doe.john'
            ]
        );


    }

    public function testUpdateThrowsInvalidArgumentException()
    {
        $userServiceMock = $this
            ->getMockBuilder(UserService::class)
            ->setMethods([
                'getEntityManager',
                'getBcryptObject',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $bcryptStub = $this
            ->getMockBuilder(Bcrypt::class)
            ->setMethods([
                'verify',
                'create',
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $entityManagerStub = $this->getEntityManagerStub();
        $userServiceMock->method('getEntityManager')->willReturn($entityManagerStub);
        $userServiceMock->method('getBcryptObject')->willReturn($bcryptStub);
        $bcryptStub->method('verify')->willReturn(false);


        $entityManagerStub->expects($this->never())->method('persist');
        $entityManagerStub->expects($this->never())->method('flush');
        $userServiceMock->expects($this->once())->method('getBcryptObject');
        $bcryptStub->expects($this->once())->method('verify');
        $bcryptStub->expects($this->never())->method('create');

        $userEntity = new UserEntity();
        $userEntity->setId(1);

        $loggedInUser = new UserEntity();
        $loggedInUser->setId(1);

        $this->expectException(\InvalidArgumentException::class);
        $userServiceMock->updateUser($userEntity, $loggedInUser,
            [
                'first_name' => 'john',
                'last_name' => 'doe',
                'current_password' => 'john.doe',
                'password' => 'doe.john'
            ]
        );


    }


}
