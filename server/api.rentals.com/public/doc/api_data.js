define({ "api": [
  {
    "type": "post",
    "url": "/apartments",
    "title": "Create Apartment",
    "name": "CreateApartment",
    "group": "Apartment",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "uuid",
            "description": "<p>Request unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "price",
            "description": "<p>Monthly price.</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "size",
            "description": "<p>Apartment size.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "room_count",
            "description": "<p>Number of rooms.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "realtor_id",
            "description": "<p>Id of attached realtor user. If Creator is realtor, he will be the attached realtor</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "longitude",
            "description": "<p>Geographical Longitude.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "latitude",
            "description": "<p>Geographical Latitude.</p>"
          }
        ]
      }
    },
    "header": {
      "examples": [
        {
          "title": "Header",
          "content": "{\n  \"Authorization\": \"Bearer 611b2f9900a34863b869b43fa33b6e6444f5dd2a\",\n  \"Content-Type\": \"application/json\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 201 Created\n  {\n  \"id\": 3,\n  \"created_at\": \"Mar 1, 2018 11:46:29\",\n  \"status\": \"rentable\",\n  \"size\": 80.3,\n  \"price\": 15.23,\n  \"room_count\": 7,\n  \"longitude\": \"44.503490\",\n  \"latitude\": \"40.177200\",\n  \"realtor_id\": 3,\n  \"realtor_name\": \"John Doe\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response Duplicate Request",
          "content": "HTTP/1.1 409 Conflict\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Conflict\",\n \"status\": 409,\n \"detail\": \"Duplicate request\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response Invalid argument",
          "content": "HTTP/1.1 400 Bad Request\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Bad Request\",\n \"status\": 400,\n \"detail\": \"Selected user is not a realtor\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response User Is Not Admin or Realtor",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Forbidden\",\n \"status\": 403,\n \"detail\": \"Only Admin User and Realtors can perform this action\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response Validation",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n     \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n     \"title\": \"Bad Request\",\n     \"status\": 422,\n     \"detail\": \"Failed Validation\",\n     \"validation_messages\": {\n         \"uuid\": {\n         \"isEmpty\": \"Value is required and can't be empty\"\n         },\n         \"price\": {\n         \"isEmpty\": \"Value is required and can't be empty\"\n         },\n         \"room_count\": {\n         \"isEmpty\": \"Value is required and can't be empty\"\n         },\n         \"longitude\": {\n         \"mustBeLessThan\": \"The input is not less or equal than '180'\"\n         },\n         \"latitude\": {\n         \"mustBeGreaterThan\": \"TThe input is not greater than or equal to '-85.05112878'\"\n         },\n         \"size\": {\n         \"isEmpty\": \"The input is not greater than '0'\"\n         }\n     },\n}",
          "type": "json"
        },
        {
          "title": "Error-Response Unknown Error",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Internal Server Error\",\n \"status\": 500,\n \"detail\": \"Server side problem\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "module/Rentals/src/V1/Rest/Apartment/ApartmentResource.php",
    "groupTitle": "Apartment"
  },
  {
    "type": "delete",
    "url": "/apartments/:id",
    "title": "Delete Apartment",
    "name": "DeleteApartment",
    "group": "Apartment",
    "version": "1.0.0",
    "header": {
      "examples": [
        {
          "title": "Header",
          "content": "{\n  \"Authorization\": \"Bearer 611b2f9900a34863b869b43fa33b6e6444f5dd2a\",\n  \"Content-Type\": \"application/json\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 204 Deleted",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response Apartment Not Found",
          "content": "HTTP/1.1 404 Not Found\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Not Found\",\n \"status\": 404,\n \"detail\": \"Apartment not found\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response User Is Not Admin or Realtor",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Forbidden\",\n \"status\": 403,\n \"detail\": \"Only Admin User and Realtors can perform this action\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response Realtor can not CRUD other realtor's apartments",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Forbidden\",\n \"status\": 403,\n \"detail\": \"Realtor does not have access to other realtors apartments\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response Unknown Error",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Internal Server Error\",\n \"status\": 500,\n \"detail\": \"Server side problem\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "module/Rentals/src/V1/Rest/Apartment/ApartmentResource.php",
    "groupTitle": "Apartment"
  },
  {
    "type": "get",
    "url": "/apartments/:id",
    "title": "Get Apartment information",
    "name": "GetApartment",
    "group": "Apartment",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "id",
            "description": "<p>Apartment unique ID, &quot;me&quot; to get current user.</p>"
          }
        ]
      }
    },
    "header": {
      "examples": [
        {
          "title": "Header",
          "content": "{\n  \"Authorization\": \"Bearer 611b2f9900a34863b869b43fa33b6e6444f5dd2a\",\n  \"Content-Type\": \"application/json\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 Ok\n{\n \"id\": 1,\n \"created_at\": \"Mar 1, 2018 11:23:17\",\n \"status\": \"rentable\",\n \"size\": \"80.30\",\n \"price\": \"15.23\",\n \"room_count\": \"7\",\n \"longitude\": \"44.503490\",\n \"latitude\": \"40.177200\",\n \"realtor_name\": \"John Doe\",\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response Apartment Not Found",
          "content": "HTTP/1.1 404 Not Found\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Not Found\",\n \"status\": 404,\n \"detail\": \"Apartment not found\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response User Is Not Admin or Realtor",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Forbidden\",\n \"status\": 403,\n \"detail\": \"Only Admin User and Realtors can perform this action\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response Realtor can not CRUD other realtor's apartments",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Forbidden\",\n \"status\": 403,\n \"detail\": \"Realtor does not have access to other realtors apartments\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response Unknown Error",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Internal Server Error\",\n \"status\": 500,\n \"detail\": \"Server side problem\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "module/Rentals/src/V1/Rest/Apartment/ApartmentResource.php",
    "groupTitle": "Apartment"
  },
  {
    "type": "get",
    "url": "/apartments",
    "title": "Get Paginated Apartments",
    "name": "GetApartments",
    "group": "Apartment",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "page",
            "description": "<p>Current page number</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": true,
            "field": "size_from",
            "description": "<p>Apartment size from</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": true,
            "field": "size_to",
            "description": "<p>Apartment size to</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": true,
            "field": "price_from",
            "description": "<p>Apartment monthly price from</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": true,
            "field": "price_to",
            "description": "<p>Apartment monthly price to</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "allowedValues": [
              "{1..10}"
            ],
            "optional": true,
            "field": "room_count_from",
            "description": "<p>Number of room in apartment from</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "allowedValues": [
              "{1..10}"
            ],
            "optional": true,
            "field": "room_count_to",
            "description": "<p>Number of room in apartment to</p>"
          }
        ]
      }
    },
    "header": {
      "examples": [
        {
          "title": "Header",
          "content": "{\n  \"Authorization\": \"Bearer 611b2f9900a34863b869b43fa33b6e6444f5dd2a\",\n  \"Content-Type\": \"application/json\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "   HTTP/1.1 200 Ok\n {\n     \"users\": [\n         {\n         \"id\": \"8\",\n         \"created_at\": \"Mar 2, 2018 12:42:33\",\n         \"status\": \"rentable\",\n         \"size\": \"80.30\",\n         \"price\": \"15.23\",\n         \"room_count\": \"8\",\n         \"longitude\": \"44.503490\",\n         \"latitude\": \"40.177200\",\n         \"realtor_id\": \"5\",\n         \"realtor_name\": \"John Doe\",\n         \"may_crud\": 1\n         }\n     ],\n     \"total\": 1,\n     \"per_page\": 10,\n     \"page\": 1,\n     \"page_count\": 1\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response Unknown Error",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Internal Server Error\",\n \"status\": 500,\n \"detail\": \"Server side problem\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "module/Rentals/src/V1/Rest/Apartment/ApartmentResource.php",
    "groupTitle": "Apartment"
  },
  {
    "type": "patch",
    "url": "/apartments/:id",
    "title": "Update Apartment",
    "name": "UpdateApartment",
    "group": "Apartment",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Apartments unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": true,
            "field": "price",
            "description": "<p>Monthly price.</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": true,
            "field": "size",
            "description": "<p>Apartment size.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "room_count",
            "description": "<p>Number of rooms.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "realtor_id",
            "description": "<p>Id of attached realtor user. If Creator is realtor, he will be the attached realtor</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": true,
            "field": "longitude",
            "description": "<p>Geographical Longitude.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "latitude",
            "description": "<p>Geographical Latitude.</p>"
          }
        ]
      }
    },
    "header": {
      "examples": [
        {
          "title": "Header",
          "content": "{\n  \"Authorization\": \"Bearer 611b2f9900a34863b869b43fa33b6e6444f5dd2a\",\n  \"Content-Type\": \"application/json\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n  {\n  \"price\": 15.23,\n  \"room_count\": 8,\n  \"longitude\": \"44.503490\",\n  \"latitude\": \"40.177200\",\n  \"size\": 80.3,\n  \"realtor_id\": 5\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response User Not Found",
          "content": "HTTP/1.1 404 Not Found\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Not Found\",\n \"status\": 404,\n \"detail\": \"User not found\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response Apartment Not Found",
          "content": "HTTP/1.1 404 Not Found\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Not Found\",\n \"status\": 404,\n \"detail\": \"Apartment not found\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response User Is Not Admin or Realtor",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Forbidden\",\n \"status\": 403,\n \"detail\": \"Only Admin User and Realtors can perform this action\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response Realtor can not CRUD other realtor's apartments",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Forbidden\",\n \"status\": 403,\n \"detail\": \"Realtor does not have access to other realtors apartments\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response Validation",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n     \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n     \"title\": \"Bad Request\",\n     \"status\": 422,\n     \"detail\": \"Failed Validation\",\n     \"validation_messages\": {\n         \"uuid\": {\n         \"isEmpty\": \"Value is required and can't be empty\"\n         },\n         \"price\": {\n         \"isEmpty\": \"Value is required and can't be empty\"\n         },\n         \"room_count\": {\n         \"isEmpty\": \"Value is required and can't be empty\"\n         },\n         \"longitude\": {\n         \"mustBeLessThan\": \"The input is not less or equal than '180'\"\n         },\n         \"latitude\": {\n         \"mustBeGreaterThan\": \"TThe input is not greater than or equal to '-85.05112878'\"\n         },\n         \"size\": {\n         \"isEmpty\": \"The input is not greater than '0'\"\n         }\n     },\n}",
          "type": "json"
        },
        {
          "title": "Error-Response Unknown Error",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Internal Server Error\",\n \"status\": 500,\n \"detail\": \"Server side problem\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "module/Rentals/src/V1/Rest/Apartment/ApartmentResource.php",
    "groupTitle": "Apartment"
  },
  {
    "type": "post",
    "url": "/oauth",
    "title": "Get Access Token By password",
    "name": "GetAccessTokenByPassword",
    "group": "Oauth",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "grant_type",
            "description": "<p>Grand Type, shall be &quot;password&quot;</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>User email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User password.</p>"
          }
        ]
      }
    },
    "header": {
      "examples": [
        {
          "title": "Header",
          "content": "{\n  \"Accept\": \"application/json\",\n  \"Content-Type\": \"application/json\",\n  \"Authorization\": \"Basic d3d3LnJlbnRhbHMuY29tOnJlbnRhbHMud2Vic2l0ZQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": " HTTP/1.1 200 OK\n{\n\"access_token\": \"e2051ad8fea7c201b76b262ad2f9fb748e522ce5\",\n\"expires_in\": 3600,\n\"token_type\": \"Bearer\",\n\"scope\": null,\n\"refresh_token\": \"d5d3ec288188d6832c5f1ef3083227fa62ee66f4\"*\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response Invalid grand",
          "content": "HTTP/1.1 401 Invalid grand\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"invalid_grant\",\n \"status\": 401,\n \"detail\": \"Invalid username and password combination\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "module/Application/view/application/index/ouath-docs.php",
    "groupTitle": "Oauth"
  },
  {
    "type": "post",
    "url": "/oauth",
    "title": "Get Access Token By Refresh token",
    "name": "GetAccessTokenByRefreshToken",
    "group": "Oauth",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "grant_type",
            "description": "<p>Grand Type, shall be &quot;refresh_token&quot;</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "refresh_token",
            "description": "<p>Refresh token.</p>"
          }
        ]
      }
    },
    "header": {
      "examples": [
        {
          "title": "Header",
          "content": "{\n  \"Accept\": \"application/json\",\n  \"Content-Type\": \"application/json\",\n  \"Authorization\": \"Basic d3d3LnJlbnRhbHMuY29tOnJlbnRhbHMud2Vic2l0ZQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": " HTTP/1.1 200 OK\n{\n\"access_token\": \"e2051ad8fea7c201b76b262ad2f9fb748e522ce5\",\n\"expires_in\": 3600,\n\"token_type\": \"Bearer\",\n\"scope\": null\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response Invalid grand",
          "content": "HTTP/1.1 400 Invalid grand\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"invalid_grant\",\n \"status\": 400,\n \"detail\": \"Invalid refresh token\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "module/Application/view/application/index/ouath-docs.php",
    "groupTitle": "Oauth"
  },
  {
    "type": "post",
    "url": "/users",
    "title": "Create User",
    "name": "CreateUser",
    "group": "User",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "uuid",
            "description": "<p>Request unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>User first name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>User last name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User password.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password_confirm",
            "description": "<p>User password confirmation.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>May be &quot;client&quot; or &quot;realtor&quot;.</p>"
          }
        ]
      }
    },
    "header": {
      "examples": [
        {
          "title": "Header",
          "content": "{\n  \"Accept\": \"application/json\",\n  \"Content-Type\": \"application/json\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 201 Created\n{\n \"id\": 1,\n \"first_name\": \"John\",\n \"last_name\": \"Doe\",\n \"email\": \"john.doe@gmail.com\",\n \"type\": \"client\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response Duplicate Request",
          "content": "HTTP/1.1 409 Conflict\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Conflict\",\n \"status\": 409,\n \"detail\": \"Duplicate request\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response User Email Busy",
          "content": "HTTP/1.1 400 Bad Request\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Bad Request\",\n \"status\": 400,\n \"detail\": \"Email is already used\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response Validation",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n     \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n     \"title\": \"Bad Request\",\n     \"status\": 422,\n     \"detail\": \"Failed Validation\",\n     \"validation_messages\": {\n         \"uuid\": {\n         \"isEmpty\": \"Value is required and can't be empty\"\n         },\n         \"first_name\": {\n         \"isEmpty\": \"Value is required and can't be empty\"\n         },\n         \"last_name\": {\n         \"isEmpty\": \"Value is required and can't be empty\"\n         },\n         \"email\": {\n         \"emailAddressInvalidFormat\": \"The input is not a valid email address. Use the basic format local-part@hostname\"\n         },\n         \"password\": {\n         \"stringLengthTooShort\": \"The input is less than 8 characters long\"\n         },\n         \"type\": {\n         \"isEmpty\": \"Value is required and can't be empty\"\n         },\n         \"password_confirm\": {\n         \"notSame\": \"Password confirmation is wrong\"\n         }\n     },\n}",
          "type": "json"
        },
        {
          "title": "Error-Response Unknown Error",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Internal Server Error\",\n \"status\": 500,\n \"detail\": \"Server side problem\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "module/Rentals/src/V1/Rest/User/UserResource.php",
    "groupTitle": "User"
  },
  {
    "type": "delete",
    "url": "/users/:id",
    "title": "Delete User",
    "name": "DeleteUser",
    "group": "User",
    "version": "1.0.0",
    "header": {
      "examples": [
        {
          "title": "Header",
          "content": "{\n  \"Authorization\": \"Bearer 611b2f9900a34863b869b43fa33b6e6444f5dd2a\",\n  \"Content-Type\": \"application/json\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 204 Deleted",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response User Not Found",
          "content": "HTTP/1.1 404 Not Found\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Not Found\",\n \"status\": 404,\n \"detail\": \"User not found\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response Admin User can not be deleted",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Forbidden\",\n \"status\": 403,\n \"detail\": \"Admin User can not be deleted\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response User Is Not Admin",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Forbidden\",\n \"status\": 403,\n \"detail\": \"Only Admin User can perform this action\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response Unknown Error",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Internal Server Error\",\n \"status\": 500,\n \"detail\": \"Server side problem\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "module/Rentals/src/V1/Rest/User/UserResource.php",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/users/:id",
    "title": "Get User information",
    "name": "GetUser",
    "group": "User",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "allowedValues": [
              "{1",
              "2",
              "3...",
              "\"me\"}"
            ],
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID, &quot;me&quot; to get current user.</p>"
          }
        ]
      }
    },
    "header": {
      "examples": [
        {
          "title": "Header",
          "content": "{\n  \"Authorization\": \"Bearer 611b2f9900a34863b869b43fa33b6e6444f5dd2a\",\n  \"Content-Type\": \"application/json\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 Ok\n{\n \"id\": 1,\n \"first_name\": \"John\",\n \"last_name\": \"Doe\",\n \"email\": \"john.doe@gmail.com\",\n \"type\": \"client\"\n\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response User Not Found",
          "content": "HTTP/1.1 404 Not Found\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Not Found\",\n \"status\": 404,\n \"detail\": \"User not found\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response User Is Not Admin",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Forbidden\",\n \"status\": 403,\n \"detail\": \"Only Admin User can perform this action\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response Validation",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n     \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n     \"title\": \"Bad Request\",\n     \"status\": 422,\n     \"detail\": \"Failed Validation\",\n     \"validation_messages\": {\n         \"first_name\": {\n         \"isEmpty\": \"Value is required and can't be empty\"\n         },\n         \"last_name\": {\n         \"isEmpty\": \"Value is required and can't be empty\"\n         }\n     },\n}",
          "type": "json"
        },
        {
          "title": "Error-Response Unknown Error",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Internal Server Error\",\n \"status\": 500,\n \"detail\": \"Server side problem\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "module/Rentals/src/V1/Rest/User/UserResource.php",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/users",
    "title": "Get Paginated Users Or All Realtors",
    "name": "GetUsers",
    "group": "User",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "page",
            "description": "<p>Current page number</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "allowedValues": [
              "{0",
              "1}"
            ],
            "optional": true,
            "field": "only_realtors",
            "description": "<p>bit for fetching only realtors</p>"
          }
        ]
      }
    },
    "header": {
      "examples": [
        {
          "title": "Header",
          "content": "{\n  \"Authorization\": \"Bearer 611b2f9900a34863b869b43fa33b6e6444f5dd2a\",\n  \"Content-Type\": \"application/json\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response Paginated Result",
          "content": "   HTTP/1.1 200 Ok\n {\n     \"users\": [\n         {\n         \"id\": \"1\",\n         \"first_name\": \"John\",\n         \"last_name\": \"Doe\",\n         \"email\": \"admin@rentals.com\",\n         \"type\": \"admin\"\n         },\n         {\n         \"id\": \"2\",\n         \"first_name\": \"John\",\n         \"last_name\": \"Doe\",\n         \"email\": \"john.doe@gmail.com\",\n         \"type\": \"realtor\"\n        }\n     ],\n     \"total\": 2,\n     \"per_page\": 10,\n     \"page\": 1,\n     \"page_count\": 1\n}",
          "type": "json"
        },
        {
          "title": "Success-Response Only realtors not paginated",
          "content": "HTTP/1.1 200 Ok\n[\n      {\n      \"id\": \"5\",\n      \"first_name\": \"John\",\n      \"last_name\": \"Doe\",\n      \"email\": \"john.doe@gmail.com\",\n      \"type\": \"realtor\"\n      }\n ]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response User Is Not Admin",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Forbidden\",\n \"status\": 403,\n \"detail\": \"Only Admin User can perform this action\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response Unknown Error",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Internal Server Error\",\n \"status\": 500,\n \"detail\": \"Server side problem\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "module/Rentals/src/V1/Rest/User/UserResource.php",
    "groupTitle": "User"
  },
  {
    "type": "patch",
    "url": "/users/:id",
    "title": "Update User",
    "name": "UpdateUser",
    "group": "User",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "allowedValues": [
              "{1",
              "2",
              "3...",
              "\"me\"}"
            ],
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID, &quot;me&quot; to get current user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>User first name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>User last name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "current_password",
            "description": "<p>User current password.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "password",
            "description": "<p>User new password.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "password_confirm",
            "description": "<p>User new password confirmation.</p>"
          }
        ]
      }
    },
    "header": {
      "examples": [
        {
          "title": "Header-Example",
          "content": "{\n  \"Authorization\": \"Bearer 611b2f9900a34863b869b43fa33b6e6444f5dd2a\",\n  \"Content-Type\": \"application/json\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 20 OK\n{\n \"id\": 1,\n \"first_name\": \"John\",\n \"last_name\": \"Doe\",\n \"email\": \"john.doe@gmail.com\",\n \"type\": \"client\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response User Not Found",
          "content": "HTTP/1.1 404 Not Found\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Not Found\",\n \"status\": 404,\n \"detail\": \"User not found\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response User Is Not Admin",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Forbidden\",\n \"status\": 403,\n \"detail\": \"Only Admin User can perform this action\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response User can not change other user's password",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Forbidden\",\n \"status\": 403,\n \"detail\": \"User may change only his/her password\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response Current password is wrong",
          "content": "HTTP/1.1 400 Bad Request\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Bad Request\",\n \"status\": 400,\n \"detail\": \"Current password is wrong\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response Unknown Error",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n \"type\": \"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\n \"title\": \"Internal Server Error\",\n \"status\": 500,\n \"detail\": \"Server side problem\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "module/Rentals/src/V1/Rest/User/UserResource.php",
    "groupTitle": "User"
  }
] });
